<?php
    include "connection.php";
    
    if(isset($_POST['contact']) && $_POST['contact'] == 1) {
        parse_str($_POST['data'], $data);
        
        $stmt = $mysqli->prepare("INSERT INTO db_contact_us (name, email, phone, subject, description) VALUES (?, ?, ?, ?, ?)");
        $stmt->bind_param("sssss", $data['username'], $data['email'], $data['phone'], $data['subject'], $data['message']);
        $stmt->execute();
        if($mysqli->insert_id > 0) {
            echo json_encode(array("status" => 1));
        }else{
            echo json_encode(array("status" => 0));
        }
        $stmt->close();
        die();
    }

    if(isset($_POST["footer_newsletter"]) && $_POST["footer_newsletter"] ==1) {
        $stmt = $mysqli->prepare("SELECT * FROM db_newsletter WHERE email = ?");
        $stmt->bind_param("s", $_POST['email']);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows > 0) {
            echo json_encode(array("status" => 1, "msg" => "Email already exist."));
        }else{
            $stmt = $mysqli->prepare("INSERT INTO db_newsletter (email) VALUES (?)");
            $stmt->bind_param("s", $_POST['email']);
            $stmt->execute();
            if($mysqli->insert_id > 0) {
                echo json_encode(array("status" => 1, "msg" => "Email has been submitted successfully."));
            }else{
                echo json_encode(array("status" => 0, "msg" => "Email has not been submitted."));
            }
        }
        $stmt->close();
        die();
    }

    if(isset($_FILES["resume"]) && !empty($_FILES["resume"])) {
        $file_extension = pathinfo($_FILES["resume"]["name"], PATHINFO_EXTENSION);
        $allowed_image_extension = array("pdf","doc","docx");
        $file_name = uniqid()."_".basename($_FILES["resume"]["name"]);
        $target = "images/career/" . $file_name;
        if (in_array($file_extension, $allowed_image_extension)) {
            if (move_uploaded_file($_FILES["resume"]["tmp_name"], $target)) {
                $stmt = $mysqli->prepare("INSERT INTO db_career (username,email,phone,job,file_name) VALUES (?,?,?,?,?)");
                $stmt->bind_param("sssss", $_POST['username'], $_POST['email'], $_POST['phone'], $_POST['job'], $file_name);
                $stmt->execute();
                if($mysqli->insert_id > 0) {
                    echo json_encode(array("status" => 1, "msg" => "Job profile has been submitted successfully."));
                }else{
                    echo json_encode(array("status" => 0, "msg" => "Job profile has not been submitted."));
                }
                $stmt->close();
            }else{
                echo json_encode(array("status" => 0, "msg" => "File has not uploaded, Something went wrong!!!"));
            }
        }else{
            echo json_encode(array("status" => 0, "msg" => "File should be PDF,DOC or DOCX"));
        }
        
        die();
    }
?>