<?php
include "header.php";
?>
 <!-- Inner Banner Section -->
 <section class="inner-banner alternate">        
     <div class="banner-curve"></div>
		<div class="auto-container">
            <div class="inner">
                <div class="theme-icon"></div>
    			<div class="title-box">
                    <h1>Aurum RealTech Services Private Limited</h1>
                    
                </div>
            </div>
		</div>
    </section>
    <!--End Banner Section -->

    <div class="sidebar-page-container services-page">
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                    <div class="services-content">
                        <div class="service-details">
                            <!--content-->
                            <div class="content">
                                <h3>Aurum RealTech Services Private Limited</h3>
                                <div class="text">
                                    <p>A full stack integrated fulfillment center providing technology enabled Real Estate Services to developers, purchasers, and occupiers of Real Estate under its module Aurum CREX – Customer Real Estate Experts. It provides a range of services from real estate broking, pre-sales, sales, post sales, direct selling agents services to banks, property funding services for retail home loan applicants, property search and listing services, investment advisory services for fractional ownership. The services team operates out of its Delivery Centre at Aurum Millennium Business Park, Navi Mumbai.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
                    <aside class="sidebar services-sidebar">
                        <!--Services Widget-->
                        <div class="sidebar-widget services-widget">
                            <div class="widget-inner">
                                <ul>
                                    <li><a href="aurum-software-solutions-private-limited">Aurum Software Solutions Private Limited </a></li>
                                    <li class="active"><a href="aurum-realtech-services-private-limited">Aurum RealTech Services Private Limited </a></li>
                                    <li><a href="k2v2-technologies">K2V2 Technologies</a></li>
                                    <li><a href="monktech-labs">MonkTech Labs Pte. Ltd.</a></li>
                                    <li><a href="integrow-asset-management">Integrow Asset Management Pvt. Ltd.</a></li>
                                </ul>
                            </div>
                        </div>
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
<?php
include "footer.php";
?>