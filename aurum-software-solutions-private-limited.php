<?php
include "header.php";
?>
 <!-- Inner Banner Section -->
 <section class="inner-banner alternate">
        <div class="banner-curve"></div>
		<div class="auto-container">
            <div class="inner">
                <div class="theme-icon"></div>
    			<div class="title-box">
                    <h1>Aurum Softwares and Solutions Private Limited</h1>
                    
                </div>
            </div>
		</div>
    </section>
    <!--End Banner Section -->

    <div class="sidebar-page-container services-page">
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                    <div class="services-content">
                        <div class="service-details">
                            <!--content-->
                            <div class="content">
                                <h3>Aurum Softwares and Solutions Private Limited</h3>
                                <div class="text">
                                    <p>The Technology arm of Aurum PropTech focused on developing Software products, solutions, and platforms for the Real Estate value chain. It is working on various new-age technologies like Artificial Intelligence, Machine learning, Cloud Solutions, Block chain, Augmented Reality and Virtual Reality.</p>
                                    <p>The team is young and diverse group with a combination of Technology and Real Estate Domain Experts.</p>
                                    <p>The development work happens out of a state-of-the-art Technology studio at Aurum Q Parć, Navi Mumbai.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
                    <aside class="sidebar services-sidebar">
                        <!--Services Widget-->
                        <div class="sidebar-widget services-widget">
                            <div class="widget-inner">
                                <ul>
                                    <li class="active"><a href="aurum-software-solutions-private-limited.php">Aurum Software Solutions Private Limited </a></li>
                                    <li><a href="aurum-realtech-services-private-limited.php">Aurum RealTech Services Private Limited </a></li>
                                    <li><a href="k2v2-technologies.php">K2V2 Technologies</a></li>
                                    <li><a href="monktech-labs.php">MonkTech Labs Pte. Ltd.</a></li>
                                    <li><a href="integrow-asset-management.php">Integrow Asset Management Pvt. Ltd.</a></li>
                                </ul>
                            </div>
                        </div>
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
<?php
include "footer.php";
?>