<?php
include "header.php";
?>

 <!-- Inner Banner Section -->
 <section class="inner-banner alternate">
        <div class="banner-curve"></div>
		<div class="auto-container">
            <div class="inner">
                <div class="theme-icon"></div>
    			<div class="title-box">
                    <h1>Board of Directors</h1>
                </div>
            </div>
		</div>
    </section>
    <!--End Banner Section -->

	<!--Team Section-->
    
<section style="margin-top: 70px;">

<div id="rs-team" class="rs-team fullwidth-team pt-100 pb-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="team-item">
                    <div class="team-img">
                        <img src="images/board-member/vasant-gujarathi.jpg" alt="team Image">
                        <div class="normal-text">
                            <h4 class="team-name"><strong>Mr. Vasant Gujarathi</strong></h4>
                            <span class="subtitle">Independent Director</span>
                        </div>
                    </div>
                    <div class="team-content">
                        <div class="display-table">
                            <div class="display-table-cell">
                                <div class="share-icons">
                                    <ul class="team-social icons-1">
                                        <li><a target="_blank" href="https://www.linkedin.com/in/vasant-gujarathi-3a02a98/" class="social-icon"><i class="fab fa-linkedin"></i></a>
                                        </li>
                                        <!--<li><a href="#" class="social-icon"><i class="fab fa-twitter"></i></a>
                                        </li>-->
                                    </ul>
                                </div>
                                <div class="team-details" data-toggle="modal" data-target="#myModal1">
                                    <h4 class="team-name">
                                        <a href="" data-toggle="modal" data-target="#myModal1"><strong>Mr. Vasant Gujarathi</strong></a>
                                    </h4>
                                    <span class="postion">Independent Director</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6">
                <div class="team-item">
                    <div class="team-img">
                        <img src="images/board-member/Ajit-Joshi.jpg" alt="team Image">
                        <div class="normal-text">
                            <h4 class="team-name"><strong>Mr. Ajit Joshi</strong></h4>
                            <span class="subtitle">Independent Director</span>
                        </div>
                    </div>
                    <div class="team-content">
                        <div class="display-table">
                            <div class="display-table-cell">
                                <div class="share-icons">
                                    
                                    <ul class="team-social icons-1">
                                        <li><a target="_blank" href="https://www.linkedin.com/in/ajit-joshi-5a545496/" class="social-icon"><i class="fab fa-linkedin"></i></a>
                                        </li>
                                        <li><a target="_blank" href="https://twitter.com/ajitjoshi_21" class="social-icon"><i class="fab fa-twitter"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="team-details" data-toggle="modal" data-target="#myModal4">
                                    <h4 class="team-name">
                                        <a href="" data-toggle="modal" data-target="#myModal4"><strong>Mr. Ajit Joshi</strong></a>
                                    </h4>
                                    <span class="postion">Independent Director</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6">
                <div class="team-item">
                    <div class="team-img">
                        <img src="images/board-member/srirang-athalye.png" alt="team Image" style="height:404px;">
                        <div class="normal-text">
                            <h4 class="team-name"><strong>Mr. Srirang Athalye</strong></h4>
                            <span class="subtitle">Non-Executive Director</span>
                        </div>
                    </div>
                    <div class="team-content">
                        <div class="display-table">
                            <div class="display-table-cell">
                                <div class="share-icons">
                                    
                                    <ul class="team-social icons-1">
                                        <li><a target="_blank" href="https://www.linkedin.com/in/srirang-athalye-b314974/" class="social-icon"><i class="fab fa-linkedin"></i></a>
                                        </li>
                                        <li><a target="_blank" href="https://twitter.com/ATHALYESHREE" class="social-icon"><i class="fab fa-twitter"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="team-details" data-toggle="modal" data-target="#myModal2">
                                    <h4 class="team-name">
                                        <a href="" data-toggle="modal" data-target="#myModal2"><strong>Mr. Srirang Athalye</a></strong>
                                    </h4>
                                    <span class="postion">Non-Executive Director</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-4 col-md-6">
                <div class="team-item">
                    <div class="team-img">
                        <img src="images/board-member/Ram-Yadav.jpg" alt="team Image">
                        <div class="normal-text">
                            <h4 class="team-name"><strong>Mr. Ramashrya Yadav</strong></h4>
                            <span class="subtitle">Non-Executive Director</span>
                        </div>
                    </div>
                    <div class="team-content">
                        <div class="display-table">
                            <div class="display-table-cell">
                                <div class="share-icons">
                                    
                                    <ul class="team-social icons-1">
                                        <li><a target="_blank" href="https://www.linkedin.com/in/yadav-ramashrya-a8766921/" class="social-icon"><i class="fab fa-linkedin"></i></a>
                                        </li>
                                        <li><a target="_blank" href="https://twitter.com/Ram112_impact?s=08" class="social-icon"><i class="fab fa-twitter"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="team-details" data-toggle="modal" data-target="#myModal5">
                                    <h4 class="team-name">
                                        <a href="" data-toggle="modal" data-target="#myModal5"><strong>Mr. Ramashrya Yadav</a></strong>
                                    </h4>
                                    <span class="postion">Non-Executive Director</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="team-item">
                    <div class="team-img">
                        <img src="images/board-member/Dr-Padma.jpeg" alt="team Image">
                        <div class="normal-text">
                            <h4 class="team-name"><strong>Dr. Padma Deosthali</strong></h4>
                            <span class="subtitle">Independent Director</span>
                        </div>
                    </div>
                    <div class="team-content">
                        <div class="display-table">
                            <div class="display-table-cell">
                                <div class="share-icons">
                                    
                                    <ul class="team-social icons-1">
                                        <li><a target="_blank" href="https://www.linkedin.com/in/padma-bhate-deosthali-280294152/?originalSubdomain=in" class="social-icon"><i class="fab fa-linkedin"></i></a>
                                        </li>
                                        <!-- <li><a target="_blank" href="#" class="social-icon"><i class="fab fa-twitter"></i></a>
                                        </li> -->
                                    </ul>
                                </div>
                                <div class="team-details" data-toggle="modal" data-target="#myModal3">
                                    <h4 class="team-name">
                                        <a href="" data-toggle="modal" data-target="#myModal3"><strong>Dr. Padma Deosthali</strong></a>
                                    </h4>
                                    <span class="postion">Independent Director</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="team-item">
                    <div class="team-img">
                        <img src="images/key-managerial-personnel/Onkar-Shetye.jpg" alt="team Image">
                        <div class="normal-text">
                            <h4 class="team-name"><strong>Mr. Onkar Shetye</strong></h4>
                            <span class="subtitle">Executive Director</span>
                        </div>
                    </div>
                    <div class="team-content">
                        <div class="display-table">
                            <div class="display-table-cell">
                                <div class="share-icons">
                                    
                                    <ul class="team-social icons-1">
                                        <li><a target="_blank" href="https://www.linkedin.com/in/onkarshetye/" class="social-icon"><i class="fab fa-linkedin"></i></a>
                                        </li>
                                        <li><a target="_blank" href="https://twitter.com/onkar_shetye" class="social-icon"><i class="fab fa-twitter"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="team-details" data-toggle="modal" data-target="#myModal6">
                                    <h4 class="team-name">
                                        <a href="" data-toggle="modal" data-target="#myModal6"><strong>Mr.Onkar Shetye</strong></a>
                                    </h4>
                                    <span class="postion">Executive Director</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .container-fullwidth -->
</div>
</section>
<?php
include "footer.php";
?>
<!----Modal Popup---->
<div class="container">
        <!-- Modal1 -->
        <div class="modal fade" id="myModal1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="bio-head">
                            <h3><strong>Mr. Vasant Gujarathi</strong></h3>
                            <p>Independent Director</p>
                            <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                        </div>    
                        <div class="bio-content">
                        <img src="images/board-member/vasant-gujarathi.jpg" class="member-img"></img>
                        <div class="bio-text">  
                            <p>A fellow member of the ICAI, Mr. Vasant, is an Independent Director at Aurum PropTech. He’s an experienced CA with over three decades of experience in the IT and service industry. During his illustrious and decorated career, Vasant has served at leading MNCs, like PwC and Yes Bank.</p>
                        </div></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal2 -->
        <div class="modal fade" id="myModal2" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="bio-head">
                            <h3><strong>Mr. Srirang Athalye</strong></h3>
                            <p>Non-Executive Director</p>
                            <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                        </div>    
                        <div class="bio-content">
                        <img src="images/board-member/srirang-athalye.png" class="member-img"></img>
                        <div class="bio-text">    
                        <p>Mr. Srirang Athalye is an alumnus of Somaiya Institute of Management Studies and Research, Mumbai for Masters in Management Studies specialising in Finance from University of Mumbai, and has been associated with the Aurum Ventures Group since February 2001.</p>
                            <p>He has over 32 years of Entrepreneurial, Industry & Consulting experience in various Corporates, start-ups and M&A activities. </p>

                            <p>He was also a key member for commercial launch of first GSM operations outside Metros after which he took the entrepreneurial plunge as a founding partner of an ISP Company. He also has been a CFO of listed Co on BSE in the past. He has proven track record in managing new technologies and raising capital.
                            He was a Chief Strategy Officer and Founder Director of ReNew Power during its incubation by Aurum Ventures. He is passionate about Innovation and believes in contrarian approach of thinking “inside the box” utilizing Systematic Inventive Thinking (SIT) Techniques and is also in trained for Creative Problem Solving (CPS). 
                            </p></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal3 -->
        <div class="modal fade" id="myModal3" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="bio-head">
                            <h3><strong>Dr. Padma Deosthali</strong></h3>
                            <p>Independent Director</p>
                            <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                        </div>    
                        <div class="bio-content">
                            <img src="images/board-member/Dr-Padma.jpeg" style="padding:20px 20px; float:left;max-width:23%;"></img>
                            <div class="bio-text">
                                <p>Dr.  Padma has Master is Social Work MSW and additionally holds a PhD from the Tata Institute of Social Sciences (2017). In her career spanning over 20 years she has worked closely with the UNFPA, UNDP and WHO on various assignments in addition to her active role in India.</p>
                                <p>She led the Centre for health and allied themes (CEHAT) a non profit working on health and human rights, as its Director for 11 years in producing significant body of research and impacting policy and practice.</p>
                                <p>She has been engaged in research, training, and policy advocacy in the areas of gender based violence, gender in medical education, health policy research, regulation of the private health sector and women and work with a focus on health and human rights for more than 20 years.</p>
                                <ul>
                                    <li>She has led the setting up of health systems models for responding to Violence Against Women (VAW), contributed to development of WHO Clinical Guidelines for responding to VAW, 2013 as member of Steering Group of the WHO GDG.</li>

                                    <li>She also contributed towards guidelines for responding to VAW during Covid 19 for the government of Bihar, through Care India.</li>

                                    <li>Steered the integration gender integrated modules for communicable and non-communicable diseases in addition to sexual and reproductive health in medical curriculum.</li>

                                    <li>She conducted a rapid assessment of One Stop Centres in India and a situational analysis of health systems response to VAW in South East Asia Region for tbe WHO South East Asia Regional Office (SEARO).</li>

                                    <li>She has investigated the impact of rape on survivors and their families (2019).</li>

                                    <li>Co-authored a study on medico-legal context of custodial deaths and development of guidelines for examination of persons in custody and conducting post mortems (which led to the eventual WHO guidance document) in 2018.</li>

                                    <li>In additions she has multiple research papers published to her credit
                                        She has been designated to work towards advancing gender, sexuality and human rights in the WASH sector for a global feminist organisation, CREA www.creaworld.org  from 1st August 2021.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal4 -->
        <div class="modal fade" id="myModal4" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="bio-head">
                            <h3><strong>Mr. Ajit Joshi</strong></h3>
                            <p>Independent Director</p>
                            <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                        </div>    
                        <div class="bio-content">
                            <img src="images/board-member/Ajit-Joshi.jpg" class="member-img"></img>
                            <div class="bio-text">
                                <p>Mr. Joshi is a Global business leader with more than 34 years of experience in Indian and International companies. He has a unique advantage of having worked in Agriculture, Technology, Media, Renewable energy, Manufacturing, Healthcare, Chemicals, and Textiles. Ajit has also served as Director on the Board of multiple companies. He successfully ran multiple revenue models and executed many M & A deals. He helped an Austrian company to build a business in India in the smart card sector.</p>
                                <p>Post his corporate career of 20 years, he built, grew, and created two successful start-ups over a period of 10 years. Ajit raised multiple rounds of funding from the likes of Sequoia, Intel, Norwest, etc. His last start-up was in the domain of technology and media. He has built a start-up business in Dubai, Jordan, Indonesia. He created a successful exit for his investors, promotors with huge valuations.</p>
                                <p>Mr. Joshi is currently working as a consultant with many businesses and start-ups in India and New Zealand. He is also mentoring various companies in different parts of the world.  Some of his current assignments are
                                </p>
                                <ul>
                                    <li>Mentor in Residence with University of Auckland, New Zealand.</li>

                                    <li>Business Mentor with Business Mentor New Zealand.</li>

                                    <li>An advisor with Loyal VC, venture capital fund from Canada.</li>

                                    <li>A strategic advisor with Energy Bank, New Zealand.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>

        <!-- Modal5 -->
        <div class="modal fade" id="myModal5" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="bio-head">
                            <h3><strong>Mr. Ramashrya Yadav</strong></h3>
                            <p>Non-Executive Director</p>
                            <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                        </div>    
                        <div class="bio-content">
                            <img src="images/board-member/Ram-Yadav.jpg" class="member-img"></img>
                            <div class="bio-text">
                                <p>Mr. Ramashray Yadav, an alumnus of Harvard Business School, is a leading proponent of India’s real estate industry with a deep interest in bringing transformational change to the sector. Mr Yadav has over 20 years of experience in Construction, Real Estate, Banking & Investment.</p>
                                <p>Mr Yadav has built multiple businesses up from scratch. His thought philosophy is firmly rooted in multiple small steps aggregating to quantum leaps and exponential impact. Before starting Integrow Asset Management, India’s first real-estate only focused asset management company he worked as CEO- Real Estate Advisory Practice, Edelweiss Financial Services Ltd.

                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>

        <!-- Modal6 -->
        <div class="modal fade" id="myModal6" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="bio-head">
                            <h3><strong>Mr. Onkar Shetye</strong></h3>
                            <p>Executive Director</p>
                            <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                        </div>    
                        <div class="bio-content">
                            <img src="images/key-managerial-personnel/Onkar-Shetye.jpg" class="member-img"></img>
                            <div class="bio-text">
                                <p>Onkar has 15 years of multisectoral experience and has driven strategic and transformational initiatives at multiple organizations across industries like Energy, Real Estate, Mineral Exploration, and Information Technology. He has worked with diverse teams across India, Europe, and Africa.</p>

                                <p>Onkar brings a large toolbox to the table and works with teams to validate, catalyze, and scale new ventures by refining competitive dynamics, honing their business plans and refining go to market strategies.</p>
                                <p>He has successfully managed multifunctional teams reporting to him and done Project Management for On Time in Time implementation.</p>

                                <p>Onkar has a Bachelor’s degree in Science from University of Mumbai and a Master’s degree from the prestigious Russel Group Universities, UK.</p>


                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>