<?php
include "header.php";
?>
<style>
    .swal2-container {
        z-index: 9999 !important;
    }
    .btn-style-one .btn-title {
        background: #daa40b;
    }
</style>
 <!-- Inner Banner Section -->
 <section class="inner-banner alternate">
        <div class="banner-curve"></div>
		<div class="auto-container">
            <div class="inner">
                <div class="theme-icon"></div>
    			<div class="title-box">
                    <h1>Careers</h1>
                    
                </div>
            </div>
		</div>
    </section>
    <!--End Banner Section -->

	<!--Contact Section-->
    <section class="contact-section-two career-section">
        <div class="auto-container">
            <div class="upper-row">
                <div class="row clearfix">

                    <div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
                        <aside class="sidebar services-sidebar">
                            <!--Services Widget-->
                            <div class="sidebar-widget services-widget">
                                <div class="widget-inner">
                                    <ul>
                                        <li data-id ="pdm" class="job-family active"><a>Product & Delivery Management</a></li>
                                        <li data-id ="tm" class="job-family"><a>Technical Management</a></li>
                                        <li data-id ="ucdt" class="job-family"><a>User Centered Design Team</a></li>
                                        <li data-id ="dev" class="job-family"><a>DevOps</a></li>
                                    </ul>
                                </div>
                            </div>
                        </aside>
                    </div>
                    <div class="content-side col-lg-8 col-md-12 col-sm-12">
                        <div class="service-details product-details">
                            <div class="accordion-box">
                                <!--Block-->
                                <div class="accordion block pdm">
                                    <div class="acc-btn">Program Manager<div class="icon flaticon-down-arrow"></div></div>
                                    <div class="acc-content">
                                        <div class="content row" style="display: flex;">
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal2"><div class="btn-title">Job Description</div></a>
                                            </div>
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion block pdm">
                                    <div class="acc-btn">Product Innovation Lead<div class="icon flaticon-down-arrow"></div></div>
                                    <div class="acc-content">
                                        <div class="content row" style="display: flex;">
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal3"><div class="btn-title">Job Description</div></a>
                                            </div>
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion block pdm">
                                    <div class="acc-btn">Business Analyst : Real Estate Domain<div class="icon flaticon-down-arrow"></div></div>
                                    <div class="acc-content">
                                        <div class="content row" style="display: flex;">
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal4"><div class="btn-title">Job Description</div></a>
                                            </div>
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion block pdm">
                                    <div class="acc-btn">Agile Product Owner<div class="icon flaticon-down-arrow"></div></div>
                                    <div class="acc-content">
                                        <div class="content row" style="display: flex;">
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal5"><div class="btn-title">Job Description</div></a>
                                            </div>
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion block tm">
                                    <div class="acc-btn">Senior Digital Enterprise Architect<div class="icon flaticon-down-arrow"></div></div>
                                    <div class="acc-content">
                                        <div class="content row" style="display: flex;">
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal6"><div class="btn-title">Job Description</div></a>
                                            </div>
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion block tm">
                                    <div class="acc-btn">Senior Solution Architect<div class="icon flaticon-down-arrow"></div></div>
                                    <div class="acc-content">
                                        <div class="content row" style="display: flex;">
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal7"><div class="btn-title">Job Description</div></a>
                                            </div>
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion block tm">
                                    <div class="acc-btn">System Administrator- IT Infrastructure<div class="icon flaticon-down-arrow"></div></div>
                                    <div class="acc-content">
                                        <div class="content row" style="display: flex;">
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal8"><div class="btn-title">Job Description</div></a>
                                            </div>
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion block tm">
                                    <div class="acc-btn">Development Team Lead<div class="icon flaticon-down-arrow"></div></div>
                                    <div class="acc-content">
                                        <div class="content row" style="display: flex;">
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal9"><div class="btn-title">Job Description</div></a>
                                            </div>
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion block tm">
                                    <div class="acc-btn">iOS Developer<div class="icon flaticon-down-arrow"></div></div>
                                    <div class="acc-content">
                                        <div class="content row" style="display: flex;">
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal10"><div class="btn-title">Job Description</div></a>
                                            </div>
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion block tm">
                                    <div class="acc-btn">Software Developer-Android<div class="icon flaticon-down-arrow"></div></div>
                                    <div class="acc-content">
                                        <div class="content row" style="display: flex;">
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal11"><div class="btn-title">Job Description</div></a>
                                            </div>
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion block tm">
                                    <div class="acc-btn">Full Stack developer<div class="icon flaticon-down-arrow"></div></div>
                                    <div class="acc-content">
                                        <div class="content row" style="display: flex;">
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal12"><div class="btn-title">Job Description</div></a>
                                            </div>
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion block ucdt">
                                    <div class="acc-btn">Graphic Designer<div class="icon flaticon-down-arrow"></div></div>
                                    <div class="acc-content">
                                        <div class="content row" style="display: flex;">
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal13"><div class="btn-title">Job Description</div></a>
                                            </div>
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion block ucdt">
                                    <div class="acc-btn">UI_UX Designer<div class="icon flaticon-down-arrow"></div></div>
                                    <div class="acc-content">
                                        <div class="content row" style="display: flex;">
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal14"><div class="btn-title">Job Description</div></a>
                                            </div>
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion block dev">
                                    <div class="acc-btn">AWS Cloud Engineer<div class="icon flaticon-down-arrow"></div></div>
                                    <div class="acc-content">
                                        <div class="content row" style="display: flex;">
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal15"><div class="btn-title">Job Description</div></a>
                                            </div>
                                            <div class="show-modal-popup col-md-4">
                                                <a href="" class="theme-btn btn-style-one" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
include "footer.php";
?>
<!----Modal Popup---->
<div class="container">
    <!-- Modal1 -->
    <div class="modal fade" id="myModal1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bio-head" style="padding:10px 20px;">
                        <h3 class="text-center"><strong>Apply For Job</strong></h3>
                        <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                    </div>    
                    <div class="bio-content">
                        <!--Form Column-->
                        <div class="form-column col-lg-12 col-md-12 col-sm-12" style="overflow-y: hidden;">
                            <div class="inner">
                                <!--Form Box-->
                                <div class="form-box">
                                    <div class="default-form career-form">
                                        <form method="post" action="" id="career-form" enctype="multipart/form-data">
                                            <div class="row clearfix">                                    
                                                <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                                    <input type="text" name="username" id="username" placeholder="Your Name"  onkeyup="Validate('username')" value="">
                                                </div>
                                                
                                                <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                                    <input type="email" name="email" id="email" placeholder="Email"  value="">
                                                </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                                    <input type="number" name="phone" id="phone" placeholder="Phone"  onkeyup="inpNum(event)" value="">
                                                </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                                    <input type="text" name="job" id="job" value="" readonly>
                                                </div>

                                                <!-- <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                                    <select class="job-select" name="job" id="job">
                                                        <option value="" >Select Available JD *</option>
                                                        <option value="Senior Digital Enterprise Architect">Senior Digital Enterprise Architect</option>
                                                        <option value="Senior Solution Architect">Senior Solution Architect</option>
                                                        <option value="Agile Product Owner: ERP/CRM Experience">Agile Product Owner: ERP/CRM Experience</option>
                                                        <option value="System Administrator- IT Infrastructure" >System Administrator- IT Infrastructure</option>
                                                        <option value="Program Manager" >Program Manager</option>
                                                        <option value="Graphic Designer" >Graphic Designer</option>
                                                        <option value="UI_UX Designer" >UI_UX Designer</option>
                                                        <option value="Development Team Lead" >Development Team Lead</option>
                                                        <option value="iOS Developer" >iOS Developer</option>
                                                        <option value="Software Developer-Android" >Software Developer-Android</option>
                                                        <option value="Product Innovation Lead" >Product Innovation Lead</option>
                                                        <option value="Full Stack developer" >Full Stack developer</option>
                                                        <option value="AWS Cloud Engineer" >AWS Cloud Engineer</option>
                                                        <option value="Business Analyst : Real Estate Domain" >Business Analyst : Real Estate Domain</option>
                                                    </select>
                                                </div> -->

                                                <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                                    <input type="file" name="resume" id="resume"
                                                    placeholder="Resume"  value="">
                                                </div>
                        
                                                <div class="form-group col-md-12 col-sm-12 text-center">
                                                    <button class="theme-btn btn-style-one" id="submit-career"><span class="btn-title">Apply Now</span></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal1 -->
    <div class="modal fade" id="myModal2" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bio-head" style="padding:10px 20px;">
                        <h3 class="text-center" style="margin-top: 0px;"><strong>Program Manager</strong></h3>
                        <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                    </div>    
                    <div class="bio-content">
                        <!--Form Column-->
                        <div class="form-column col-lg-12 col-md-12 col-sm-12">
                            <div class="inner">
                                <!--Form Box-->
                                <div class="form-box">
                                    <div class="default-form career-form">
                                        <h3>Job Description:</h3>
                                        <div class="text">
                                            <p>Leading Aurum PropTech Digital team, candidate will be tasked with developing programs to support the organization's strategic direction, as well as creating and managing long term goals, as we define our digital journey into the PropTech market.&nbsp;&nbsp;</p>
                                            <p>Join us to be a key part of a team that is changing the future of PropTech market, with the freedom to make your mark. Together, we will push the boundaries of what is possible; the role is for Program Manager, with the ability to act as Agile Delivery Manager.&nbsp; Dynamic, capable, and pro-active, you will own the Application Development Teams, who will create an Enterprise Level platform for Real Estate Industry to serve all aspects from Construction to Sales.</p>
                                            <p>You will be Agile coach for Aurum PropTech, advocating agile as a methodology. You will be responsible for assembling the project team, assigning responsibilities, and managing schedules and resources to ensure timely completion of assigned projects.</p>
                                            <p>&emsp;</p>
                                        </div>
                                        <h3>Key Responsibilities:</h3>
                                        <div class="text">
                                            <ul>
                                            <li>Planning out the blueprints for software projects, Analysing and documenting requirements with BA.</li>
                                            <li>Develop the project schedule using Agile methodology and plan to ensure timely completion of the Minimum Viable Products (MVP).&nbsp;</li>
                                            <li>Sprint planning and identification of iterative deliverables and arrive at overall schedule and effort.</li>
                                            <li>Create Agile plan of development, Testing, Validation and Production Deployment</li>
                                            <li>Create Project Budget and Adherence</li>
                                            <li>Defining project Governance and controls</li>
                                            <li>Managing risks and issues and taking corrective measurements</li>
                                            <li>Obtain management and client agreement with the project plan and timeline and advise of any obstacles or resource needs that may affect completion of the project as planned.&nbsp;</li>
                                            <li>Assemble the project team, identify competencies, and assign resources to development tasks appropriate to everyone’s knowledge, skill, and abilities.</li>
                                            <li>Communicate project Short-Team and Long-Term goals, monitor project progress, and adjust resources as necessary to keep the project on track.</li>
                                            <li>Implementing and Managing Changes and interventions to ensure project goals achieved. Tracking milestones, deliverables, and change requests.</li>
                                            <li>Analysing program risks and create Mitigation strategies.</li>
                                            <li>Apply project management tools and tracking systems to manage all aspects of project progress.</li>
                                            <li>Reporting on program performance to executive/Management Team</li>
                                            <li>Working with the HR team to manage staff and resources for programs.</li>
                                            <li>Evaluate the performance of team members and determine training needs.</li>
                                            <li>Pre-sale activities support</li>
                                            </ul>
                                        </div>
                                        <h3>What we are looking for:&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</h3>
                                        <div class="text">
                                            <p><strong>Essential Skills&nbsp;</strong></p>
                                            <ul>
                                            <li>12+ years’ experience in Application Development and roll-out.</li>
                                            <li>Bachelor’s Degree in Technology or Business Administration or related discipline required (MBA preferred)</li>
                                            <li>Demonstratable experience in Agile methodologies, like, Kanban, Scrum, XP, and SAFe is essential.</li>
                                            <li>Agile Certification is Essential.</li>
                                            <li>Experience to manage team of 20+</li>
                                            <li>Experience working on JIRA Software.</li>
                                            <li>Staffing, planning, and people management</li>
                                            <li>Managing performance and profitability</li>
                                            <li>Financial planning and Strategic Planning skills</li>
                                            <li>PMP / Prince2 Certification required</li>
                                            <li>Ability to run several projects in parallel.</li>
                                            <li>Ability to build effective teams, motivate and direct staff.</li>
                                            <li>Monitoring the sprint's burndown chart and remove any roadblocks/obstacles from the path of the team.</li>
                                            </ul>
                                        </div>
                                        <h3>&nbsp;Who we are looking for:</h3>
                                        <div class="text">
                                            <ul>
                                            <li>Looking for a Program Manager, with the proven background of either leading a major transformation program or heading the start-up environment as a program manager.</li>
                                            <li>Good knowledge and handling of project and program management methodology and techniques.</li>
                                            <li>Ability to work positively with the wide range of individuals involved in program management.</li>
                                            <li>Good knowledge of budgeting and resource allocation procedures</li>
                                            <li>Strong leadership and management skills</li>
                                            <li>Strong interpersonal and communication skills</li>
                                            </ul>
                                        </div>
                                        <h3>Qualifications</h3>
                                        <div class="text">
                                            <ul>
                                            <li>A bachelor’s degree in information technology,&nbsp;&nbsp;</li>
                                            <li>Advance/master’s degree in management (Desire)</li>
                                            <li>Agile certification (Essential)</li>
                                            <li>Strong written and spoken communication skills.</li>
                                            <li>Proven experience of leading and managing IT programs.</li>
                                            <li>Good working knowledge of IT project estimations</li>
                                            <li>Strong technical background</li>
                                            </ul>
                                        </div>
                                        <h3>Tools Knowledge</h3>
                                        <div class="text">
                                            <p>MS Suite like, Word, Excel, PowerPoint, MS TEAMS, SharePoint, MS Project, JIRA Tool, Zoho etc.</p>
                                                                    
                                        </div>
                                        <h3>No of Positions</h3>
                                        <div class="text">
                                            <p>1</p>
                                        </div>
                                                                        
                                            <div class="text show-modal-popup">
                                            <a href="" class="theme-btn btn-style-one apply_career_form" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal1 -->
    <div class="modal fade" id="myModal3" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bio-head" style="padding:10px 20px;">
                        <h3 class="text-center" style="margin-top: 0px;"><strong>Product Innovation Lead</strong></h3>
                        <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                    </div>    
                    <div class="bio-content">
                        <!--Form Column-->
                        <div class="form-column col-lg-12 col-md-12 col-sm-12">
                            <div class="inner">
                                <!--Form Box-->
                                <div class="form-box">
                                    <div class="default-form career-form">
                                        <h3>Job Description:</h3><div class="text">
                                        <p>As part of the Digital Team for Aurum PropTech, our role will be Innovator and Changemaker. You collaborate with creative people to solve problems and launch The Next Big Thing.&nbsp; &nbsp; As a change agent, you can work towards driving new innovative product development and will own the creation, definition and design for an MVP innovating new module/products.</p>
                                        <p>You will lead the innovation product development steam and will be responsible for the development and implementation of business transformation, information technology, data science and services throughout our Aurum PropTech value proposition.&nbsp; You will be setting the vision, working on innovative solutions and drawing stakeholders together on this journey.</p>
                                        <p>Your innovative thinking process starts with Ideation, Scribble on enough whiteboard and eventually once you find the Eureka movement, needs to be documented, explain and coordinate with teams for best fit in RE Process. You will do continual exploration in Product, Process or Services focusing towards improved customer experience in Real Estate from B2B, B2B2C and B2C.</p>
                                        <p>You will have to demonstrate the Innovative side of yours for an effective user experience and there is no limit to explore at Aurum. Limit to explore at Aurum.</p>
                                        </div><h3>Key Responsibilities:</h3><div class="text">
                                        <ul>
                                        <li>Participate in new business opportunities as required, develop proposals and scopes of work as required.</li>
                                        <li>Develop and guide the innovation and new product strategies for the organization.</li>
                                        <li>Identify new innovation opportunities to drive revenue, margin and profit growth.</li>
                                        <li>Create an innovation approach with the appropriate processes, tools and metrics to support new growth opportunities.</li>
                                        <li>Create multi-disciplinary technical innovation and solutions to support growth strategies.</li>
                                        <li>Build strong relationships with technology vendors and develop first-to-market digital innovation that aligns with Aurum PropTech initiatives.</li>
                                        <li>Develop new avenues to further Aurum PropTech to support both our businesses growth.</li>
                                        <li>You will be responsible for creating a reliable, repeatable process to generate new ideas, test their viability and then either come-up with proposal to kill or invest in them.</li>
                                        <li>Work outside the realm of the rest of the company, but you will be tasked with aligning your innovation goals with those of the core business.</li>
                                        <li>Spearhead the strategic mapping of core competency for the key product areas, or the results of these efforts, and advocate R&amp;D opportunities.</li>
                                        <li>Lead all technology and innovation initiatives for the project. This may include working with the technology community to develop solutions in data collection systems, mobile applications, social media and knowledge management platforms, monitoring, and improved data sharing.</li>
                                        <li>Work with technology innovators to bring proposed solutions to scale.</li>
                                        </ul>
                                        </div><h3>What we are looking for:</h3><div class="text">
                                        <p><strong>Essential Skills&nbsp;</strong></p>
                                        <ul>
                                        <li>Master’s degree in a Technology discipline, or comparable experience.</li>
                                        <li>Fluency in English</li>
                                        <li>Demonstrable experience of contributing towards digital product strategy either on a major transformation program or start-up environment.</li>
                                        <li>At least 4+ years of experience with progressively increasing responsibility in managing application/systems development projects or in the use of technology in Real Estate or Construction</li>
                                        <li>Effective written and oral communication, presentation, and representational skills.</li>
                                        <li>Strong innovation and/or networking skills.</li>
                                        <li>Excellent organizational and follow up skills with strong attention to detail.</li>
                                        <li>Data Drive – Highly numerate and analytical</li>
                                        </ul>
                                        </div><h3>Who we are looking for:</h3><div class="text">
                                        <ul>
                                        <li>Advanced degree, in a Technology discipline</li>
                                        <li>Must demonstrate strong commercial Acumen.</li>
                                        <li>Experience in contributing towards digital product strategy and road map.</li>
                                        <li>Pragmatic and result focussed – a “Self-Starter” who can take a “blank sheet of paper” and rapidly deliver an approach and tangible outputs.</li>
                                        <li>Experience developing and refining, Tools / Applications that can work across platforms.</li>
                                        <li>Enabler, who can create structure and processes so that innovation can happen in organization.</li>
                                        <li>Highly motivated with the ability to work independently.</li>
                                        <li>Exhibits a curious mind.</li>
                                        <li>Demonstrated ability to get to the essence of a new concept quickly.</li>
                                        </ul>
                                        </div><h3>Qualifications:</h3><div class="text">
                                        <p>IIT,&nbsp; MBA Systems / Technology or relevant stream.</p>
                                        </div><h3>Tools Knowledge</h3><div class="text">
                                        <p>Microsoft Products like, Excel, Word, Power Points, Access, Visio. BA Tools like, MS Modern Requirements. Or Jira Software/agile</p>
                                                                    
                                        </div><h3>No of Positions</h3><div class="text">
                                        <p>1</p></div>
                                        <div class="text show-modal-popup">
                                            <a href="" class="theme-btn btn-style-one apply_career_form" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal1 -->
    <div class="modal fade" id="myModal4" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bio-head" style="padding:10px 20px;">
                        <h3 class="text-center" style="margin-top: 0px;"><strong>Business Analyst : Real Estate Domain</strong></h3>
                        <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                    </div>    
                    <div class="bio-content">
                        <!--Form Column-->
                        <div class="form-column col-lg-12 col-md-12 col-sm-12">
                            <div class="inner">
                                <!--Form Box-->
                                <div class="form-box">
                                    <div class="default-form career-form">
                                        <h3>Job Description:</h3><div class="text">
                                        <p>Working as part of Aurum Digital Team, Business Analyst (BA) will have to do detail process mapping of Real Estate Complete ecosystem from construction, Sales, Supply Relationship to ‘Handover and Facility Management’. You should be able to visualize complete product suite and create module wise approach.&nbsp;</p>
                                        <p>You will be focused on evaluating business processes, anticipating requirements, uncovering areas for improvement, Perform detail requirement analysis and articulate functional document for Solution Architect. These specifications will need to be very precise and very clear for Development team. You must work closely with Business teams and Solution Architect to provide clear understanding between Functional and Technical needs.&nbsp;</p>
                                        </div><h3>Business Analyst Responsibilities:</h3><div class="text">
                                        <ul>
                                        <li>Works effectively and individually with Technical as well as Non-Technical team members and Business Teams</li>
                                        <li>Devised and prepared concise and effective User Stories.</li>
                                        <li>Determined precise and accurate User Story acceptance criteria used by developers.</li>
                                        <li>Construct workflow charts and diagrams; studying system capabilities; writing specifications for each module, which is part of overall product.</li>
                                        <li>Effectively communicating your insights and plans to cross-functional team members and management.</li>
                                        <li>Understand and communicate the financial and operational impact of any changes.</li>
                                        <li>Timely escalates deviations and Changes to Project Manager to eliminate or reduce Impact on project timeline or Cost.</li>
                                        <li>Conduct System Integration Testing and User Acceptance Testing.</li>
                                        <li>Conduct Training, Coaching and Guidance to Business Users as and when require.</li>
                                        </ul>
                                        </div><h3>&nbsp;What we are looking for:</h3><div class="text">
                                        <p>&nbsp;<strong>Essential Skills&nbsp;</strong></p>
                                        <ul>
                                        <li>A minimum of 4+ years of experience as business analysis or consulting experience in Real Estate&nbsp;</li>
                                        <li>A degree in management from a reputed Business school preferably Tier I and Tier II schools.&nbsp;</li>
                                        <li>Excellent Real Estate Business Process knowledge and Able to discuss experience with cloud software solution Architect.</li>
                                        <li>Exceptional analytical and conceptual thinking skills.</li>
                                        <li>Excellent documentation skills.</li>
                                        <li>Experience creating detailed Documents, reports and giving presentations.</li>
                                        </ul>
                                        </div><h3>Who we are looking for:</h3><div class="text">
                                        <ul>
                                        <li>Experience in full software life-cycle implementation, upgrades, production support, rollout &amp; enhancement from business analysis, testing, migration, go-live assistance till post implementation support.</li>
                                        <li>Knowledge of implementation and configuration of enterprise systems.</li>
                                        <li>Business analysts should be familiar with database design and programming methodology so they can determine the best ways to organize and present information at scale.</li>
                                        <li>Placing information on graphs and charts through data visualization, to see trends and patterns to help identify which strategies are working effectively.</li>
                                        <li>Ability to work independently and with others.</li>
                                        </ul>
                                        </div><h3>Qualifications</h3><div class="text">
                                        <ul>
                                        <li>Master’s Degree in Business Management</li>
                                        <li>Experience in Jira Software</li>
                                        </ul>
                                        </div><h3>Tools Knowledge</h3><div class="text">
                                        <p>Microsoft Products like, Excel, Word, Power Points, Access, Visio. BA Tools like, MS Modern Requirements. Or Jira Software, Viima.</p>
                                        </div><h3>Development Values</h3><div class="text">
                                        <p>New technology is exciting, but at Aurum PropTech we recognize that people are the key to our successful digital transformation.&nbsp; &nbsp; We are creating a workplace where they can focus on high-value, strategic work, high-rewarding career path and be inspired to enhance our client’s digital services - with an Agile mindset.&nbsp;</p>
                                        <p>This is a fantastic opportunity to join a fluid and fast-paced organization offering an attractive working environment both in terms of work type, technologies and projects.&nbsp; &nbsp;It’s somewhere you can thrive, grow and be challenged.&nbsp; &nbsp;The program is currently at its nascent stage and will entail owning challenging and fulfilling role using cutting-edge technology.&nbsp; &nbsp;We will be willing to invest on relevant training for the right candidate.&nbsp;</p>
                                        <p><em><strong>COME JOIN US as we empower our teams with digital skills.</strong></em></p>
                                                                    
                                        </div><h3>No of Positions</h3><div class="text">
                                        <p>2</p></div>
                                        <div class="text show-modal-popup">
                                            <a href="" class="theme-btn btn-style-one apply_career_form" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal1 -->
    <div class="modal fade" id="myModal5" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bio-head" style="padding:10px 20px;">
                        <h3 class="text-center" style="margin-top: 0px;"><strong>Agile Product Owner</strong></h3>
                        <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                    </div>    
                    <div class="bio-content">
                        <!--Form Column-->
                        <div class="form-column col-lg-12 col-md-12 col-sm-12">
                            <div class="inner">
                                <!--Form Box-->
                                <div class="form-box">
                                    <div class="default-form career-form">
                                        <h3>Job Description:</h3>
                                        <div class="text">
                                            <p>Working as part of Aurum Digital Team, Business Analyst (BA) will have to do detail process mapping of Real Estate Complete ecosystem from construction, Sales, Supply Relationship to ‘Handover and Facility Management’. You should able to visualize enterprise architecture as per Real Estate Process.</p>
                                            <p>You will be focused on evaluating business processes, anticipating requirements, uncovering areas for improvement, Perform detail requirement analysis and articulate functional documents for Solution Architect. These specifications will need to be very precise and very clear for the Development team. You must work closely with Business teams and Solution Architect to provide clear understanding between Functional and Technical needs.&nbsp;</p>
                                            <p>You will be prioritizing initiatives based on business needs, requirements and create detail Roadmap for Enterprise product delivery.&nbsp;&nbsp;</p>
                                            <p>You will Own and Deliver Minimum Viable Product (MVP), and Agile module delivery to deliver final product.&nbsp;</p>
                                            <p>&nbsp;</p>
                                        </div>
                                        <h3>Agile Product Owner (BA) Responsibilities:</h3>
                                        <div class="text">
                                            <ul>
                                                <li>As part of Enterprise Digital platform team, play an integral role in the collecting, Analysing and creating Functional Solution Document.</li>
                                                <li>Devised and prepared concise and effective User Stories.</li>
                                                <li>Create and maintain product backlog and prioritize the features in backlog before sprint&nbsp;</li>
                                                <li>Attend sprint planning meetings and clearly communicate business requirement to the team.</li>
                                                <li>Validate UI/UX for every feature/story.</li>
                                                <li>Conduct stories/functionality demo to required stakeholders.&nbsp;</li>
                                                <li>Partner with Project Manager to assess value and develop business cases of a given product feature to ensure work is aligned with the product strategy and focuses on maximum value</li>
                                                <li>Determined precise and accurate User Story acceptance criteria used by developers.</li>
                                                <li>Construct workflow charts and diagrams; studying system capabilities; writing specifications for each module, which is part of overall product.</li>
                                                <li>Assist users in all aspect of ERP system use.&nbsp;</li>
                                                <li>Identifying and prioritizing technical and functional requirements as per Agile Project Plan</li>
                                                <li>Based on Agile methodology, deliver MVPs, Add-ons and final versions to deliver</li>
                                                <li>Test and Validate the configuration of ERP System, provide technical guidance for ERP.</li>
                                                <li>Timely escalates deviations and Changes to Project Manager to eliminate or reduce Impact on project timeline or Cost.</li>
                                                <li>Conduct System Integration Testing and User Acceptance Testing.</li>
                                                <li>Conduct Training, Coaching and Guidance to Business Users as and when require.</li>
                                                <li>Monitor project progress and performance by tracking activity, resolving problems, publishing progress reports, recommending actions.</li>
                                                <li>Collaborate on implement and upgrade systems and processes as required for enhanced functionality and security issue resolution and manage infrastructure to include firewalls, databases, malware protection software and other processes as they relate to ERP software.</li>
                                            </ul>
                                        </div>
                                        <h3>What we are looking for:</h3>
                                        <div class="text">
                                            <p><strong>Essential Skills&nbsp;</strong></p>
                                            <ul>
                                                <li>A minimum of 4+ years of experience as a product owner, business analyst or Experience in one Major ERP Implementation</li>
                                                <li>A degree in management from a reputed Business school preferably Tier I and Tier II schools.&nbsp;</li>
                                                <li>Excellent Real Estate Business Process knowledge and Able to discuss experience with cloud software solution Architect.</li>
                                                <li>Strong work experience in agile business analysis</li>
                                                <li>Knowledge of DevOps Practice is preferred.&nbsp;</li>
                                                <li>Exceptional analytical and conceptual thinking skills.</li>
                                                <li>Excellent documentation skills.</li>
                                                <li>Experience creating detailed Documents, reports and giving presentations.</li>
                                                <li>Excellent planning and time management skills.</li>
                                                <li>Able to discuss the benefits of a Software as a Service Model.</li>
                                                <li>identify problems within a business, including through using data modelling techniques.</li>
                                                <li>The ability to influence stakeholders and work closely with them to determine acceptable solutions.</li>
                                            </ul>
                                        </div>
                                        <h3>Who we are looking for:</h3>
                                        <div class="text">
                                            <ul>
                                            <li>Proven experience on working as Agile Product Owner for ERP Implementation.</li>
                                            <li>Experience in ERP implementation, upgrades, production support, rollout &amp; enhancement from business analysis, testing, migration, go-live assistance till post implementation support.</li>
                                            <li>Knowledge of ERP\MRP applications and how they work for functional areas such as Finance, Order Management, Engineer to Order, Warehouse/Inventory and Reporting.</li>
                                            <li>Knowledge of implementation and configuration of enterprise systems, in Agile way.</li>
                                            <li>Demonstrated ability to work with functional leaders and team members to discover, document, design and implement business processes in an ERP application platform.</li>
                                            <li>Placing information on graphs and charts through data visualization, to see trends and patterns to help identify which strategies are working effectively.</li>
                                            <li>Certification in Business Analysis or Agile Analysis</li>
                                            <li>A history of leading and supporting successful projects.</li>
                                            <li>Ability to work independently and with others.</li>
                                            </ul>
                                        </div>
                                        <h3>Qualifications</h3>
                                        <div class="text">
                                            <ul>
                                            <li>Master’s Degree in Business Management</li>
                                            <li>Agile Certifications</li>
                                            <li>Experience in Business Intelligence and Big Data</li>
                                            <li>Experience in Jira Software</li>
                                            </ul>
                                        </div>
                                        <h3>Tools Knowledge</h3>
                                        <div class="text">
                                            <p>Microsoft Products like, Excel, Word, Power Points, Access, Visio. BA Tools like, MS Modern Requirements. Or Jira Software/agile. Viima.</p>
                                        </div>
                                                                    
                                        <h3>No of Positions</h3>
                                        <div class="text">
                                            <p>1</p>
                                        </div>
                                        <div class="text show-modal-popup">
                                            <a href="" class="theme-btn btn-style-one apply_career_form" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal1 -->
    <div class="modal fade" id="myModal6" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bio-head" style="padding:10px 20px;">
                        <h3 class="text-center" style="margin-top: 0px;"><strong>Senior Digital Enterprise Architect</strong></h3>
                        <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                    </div>    
                    <div class="bio-content">
                        <!--Form Column-->
                        <div class="form-column col-lg-12 col-md-12 col-sm-12">
                            <div class="inner">
                                <!--Form Box-->
                                <div class="form-box">
                                    <div class="default-form career-form">
                                        <h3>Job Description:</h3>
                                        <div class="text">
                                            <p>Working as part of Aurum digital team, the right candidate will provide a great deal of technical oversight and technical leadership across the product evolution as we define our digital journey into the PropTech market.</p>    

                                            <p>As an Enterprise Architect you will be responsible for ensuring that business strategy is supported by an appropriate technology systems architecture and you will have an in-depth appreciation and high-level understanding of Software Development, cloud infrastructure, software design patterns, understanding of a distributed application architecture and of software quality assurance principles.</p>

                                            <p>You will be quality focused and have the genuine ability to lead technical architecture and design.   Ideally you will have been a software engineer then progressed either as senior enterprise, domain or solution architect.</p>

                                            <p>You will have demonstrable experience of either working as lead architect for a major IT transformation program or lead execution of IT solution design for a start-up program as an Enterprise Architect.</p>
                                        </div>

                                        <h3>Key Responsibilities:</h3>
                                        <div class="text">
                                            <ul>
                                                <li>Develop an in-depth knowledge of the business's processes, strategy and products and align the architectural designs accordingly.</li>
                                                <li>Define the systems architectural strategy.</li>
                                                <li>Provide cost and / or resource estimates into lean business cases to enable quick and well-informed business decisions.</li>
                                                <li>Proactively keep up with the latest industry thinking and technologies and feed them back to the Architectural runway to help shape the future of the Aurum PropTech architectural solutions.</li>
                                                <li>Review, analysis and decision making regarding appropriate, modern technologies to use in delivering business solutions.</li>
                                                <li>Work with 3rd party vendors to assess potential bought-in or licensed solutions where appropriate.</li>
                                                <li>Lead in the communication of architectural and security concepts and requirements to key stakeholders, articulating decisions and strategies, whilst responding to challenges</li>
                                                <li>Lead in the communication of concepts and guidelines to other architects and development teams</li>
                                                <li>Work with the Technical Architecture team in the governance of design, coding, and software delivery practices for solutions developed internally.</li>
                                                <li>Work with the solution architecture team to define a comprehensive roadmap for Aurum PropTech digital journey</li>
                                                <li>Working within the Architecture function; the Architect will be responsible for all aspects of our digital and data agenda, setting principles around our enterprise architecture and supporting the business goals.</li>
                                                <li>Work closely with the product owner; You will develop and maintain architectural artefact for core solution architecture design, principles and guidelines, championing them across the company and any third-party IT providers we use. </li>
                                                <li>Create solutions cognizant of strategic commercial goals, non-functional requirements, and business value.</li>
                                            </ul>
                                        </div>

                                        <h3>What we are looking for:</h3>
                                        <b>Essential Skills</b>
                                        <div class="text">
                                            <ul>
                                                <li>7+ years’ experience in Architecture, and technical leadership around software engineering</li>
                                                <li>Excellent technical Knowledge - a very good broad understanding of a wide variety of technologies pertinent, inclusive of emerging technologies & trends. (e.g. AWS, Java, Adaptive and Responsive design, ML, AI, NLP.. etc.)</li>
                                                <li>Data architecture patterns and principles e.g. ingestion, streaming (pub/sub, kafka), eventing, big data technology, data lakes, data privacy, compliance, multi tenancy, integration patterns etc.</li>
                                                <li>Strong architectural framework experience</li>
                                                <li>Strong UML and sequence diagram knowledge</li>
                                                <li>Expert in Micro-Services adoption of solution design practices</li>
                                                <li>Recent experience in application mediation, in the form of APIs, API management and event driven architecture</li>
                                                <li>Architecting applications using containers and orchestration technology to deliver modern cloud native applications.</li>
                                                <li>Strong background in Agile/DevOps delivery in a scaled environment, also comfortable delivering using waterfall methodology</li>
                                                <li>Experience with current cloud-based data stacks and engineering processes</li>
                                                <li>Communicating efficiently and work together within a multi-disciplinary team, fostering the open and collaborative culture.</li>
                                                <li>Good knowledge of Continuous Integration (CI/CD)</li>
                                                <li>Experience of working on scalable architecture.</li>
                                                <li>Ability to be hands on by contributing to proof of concepts.</li>
                                                <li>Highly analytical mindset coupled with a commercial and practical approach.</li>
                                                <li>Able to translate technical issues into simple language.</li>
                                                <li>Desired but not Essential Skills</li>
                                                <li>TOGAF certification</li>
                                                <li>Experience working with Cloud products (Azure, Google Cloud, AWS)
                                                <li>Experienceof BPMN beneficial but not essential</li>
                                                <li>Relevant experience of PropTech industry but not essential</li>
                                            </ul>
                                        </div>

                                        <h3>Who we are looking for:</h3>
                                        <div class="text">
                                            <ul>
                                                <li>A senior Enterprise architect who is able to own &amp; lead our digital &amp; data roadmap.</li>
                                                <li>Span the technology stack, providers, look outward as well as inward, aligning the executive team and product strategy with changes in technology (Has the ability to present opportunity value and benefits to C-level)</li>
                                                <li>Have the gravitas to inspire our teams and help coach and mentor less experienced IT members.</li>
                                                <li>Delivery and goals orientation within a start-up environment</li>
                                                <li>Be a strategic thinker and also a hands-on contributor.</li>
                                                <li>Self-starter who enjoys ownership of driving and shaping architecture (and product) roadmaps</li>
                                                <li>A considerate team player</li>
                                                <li>Strong communicator who can present to internal and external audiences.</li>
                                                <li>Curious with a continuous learning mindset</li>
                                                <li>Business focused.</li>
                                            </ul>
                                        </div>

                                        <h3>Qualifications:</h3>
                                        <div class="text">
                                            <ul>
                                                <li>A bachelor’s degree in information technology&nbsp;</li>
                                                <li>Strong written and spoken communication skills.</li>
                                                <li>Proven experience of leading and managing IT programs.</li>
                                                <li>Good working knowledge of IT project estimations</li>
                                                <li>Strong technical background</li>
                                                <li>Relevant certification as an architect (highly desired)</li>
                                                <li>Agile certification (highly desired)</li>
                                            </ul>
                                        </div>

                                        <h3>Tools Knowledge:</h3>
                                        <div class="text"><p>MS suite, MS TEAMS, SharePoint, Planview or SAL (AWS) or Bizz Design</p></div>

                                        <h3>No. of Positions:</h3>
                                        <div class="text"><p>1</p></div>

                                        <div class="text show-modal-popup">
                                            <a href="" class="theme-btn btn-style-one apply_career_form" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal1 -->
    <div class="modal fade" id="myModal7" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bio-head" style="padding:10px 20px;">
                        <h3 class="text-center" style="margin-top: 0px;"><strong>Senior Solution Architect</strong></h3>
                        <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                    </div>    
                    <div class="bio-content">
                        <!--Form Column-->
                        <div class="form-column col-lg-12 col-md-12 col-sm-12">
                            <div class="inner">
                                <!--Form Box-->
                                <div class="form-box">
                                    <div class="default-form career-form">
                                        <h3>Job Description:</h3>
                                        <div class="text">
                                            <p>Working as part of Aurum digital team, the right candidate will provide a great deal of technical oversight and technical leadership across the product evolution as we define our digital journey into the PropTech market.</p> 
                                            <p>Join us to be a key part of a team that’s changing the future of PropTech market, with the freedom to make your mark. Together, we’ll push the boundaries of what’s possible; the role is for Senior solution architect, with the ability to act as a functional technical manager. Dynamic, capable and pro-active, you will own the technical solution architecture &amp; design.</p>
                                            <p>You will be quality focused and have the genuine ability to lead technical architecture and design. Ideally you will have been a software engineer then progressed either as senior enterprise, domain or solution architect and possibly have grown/scaled up a team.</p>
                                            <p>You will have demonstrable experience of either working as lead architect for a major IT transformation program or lead execution of IT solution design for a start-up program.</p>
                                        </div>

                                        <h3>Key Responsibilities:</h3>
                                        <div class="text">
                                            <ul>
                                                <li>Working within the Architecture function; the Architect will be responsible for all aspects of our digital and data agenda, setting principles around our solution architecture and supporting the business goals.</li>
                                                <li>Work closely with the product owner; You will develop and maintain architectural artefact for core solution architecture design, principles and guidelines, championing them across the company and any third-party IT providers we use.</li>
                                                <li>Responsible for solutions architecture to maintain and expand a digital platform that meets future company goals. You will own the technical roadmap and vision.</li>
                                                <li>Create solutions cognisant of strategic commercial goals, non-functional requirements, and business value.</li>
                                                <li>Support, contribute and champion company-wide digital &amp; data agenda and governance disciplines.</li>
                                                <li>Work collaboratively with Product owner, Business Analyst, Development, Data Engineering, and Data Science teams to understand requirements and architect solutions that conform with our digital and data architecture principles.</li>
                                                <li>Lead technical working groups to set out our build and design vision, ensuring adherence to standards and principles.</li>
                                                <li>Defining, promoting re-usable, extendible, scalable, maintainable solution considering trade-off for cost vs benefit.</li>
                                                <li>Own, manage and lead technical discussions with our third-party suppliers.</li>
                                            </ul>
                                        </div>

                                        <h3>What we are looking for</h3>
                                        <p><strong>Essential Skills</strong></p>
                                        <div class="text">
                                            <ul>
                                                <li>10+ years’ experience in Architecture, and technical leadership around software engineering</li>
                                                <li>Excellent technical Knowledge - a very good broad understanding of a wide variety of technologies pertinent, inclusive of emerging technologies &amp; trends. (e.g. AWS, Java, Adaptive and Responsive design, ML, AI, NLP.. etc.)</li>
                                                <li>Data architecture patterns and principles e.g. ingestion, streaming (pub/sub, kafka), eventing, big data technology, data lakes, data privacy, compliance, multi tenancy, integration patterns etc.</li>
                                                <li>Strong architectural framework experience</li>
                                                <li>Strong UML and sequence diagram knowledge</li>
                                                <li>Expert in Micro-Services adoption of solution design practices</li>
                                                <li>Recent experience in application mediation, in the form of APIs, API management and event driven architecture</li>
                                                <li>Architecting applications using containers and orchestration technology to deliver modern cloud native applications.</li>
                                                <li>Strong background in Agile/DevOps delivery in a scaled environment, also comfortable delivering using waterfall methodology.</li>
                                                <li>Experience with current cloud-based data stacks and engineering processes.</li>
                                                <li>Communicating efficiently and work together within a multi-disciplinary team, fostering the open and collaborative culture.</li>
                                                <li>Good knowledge of Continuous Integration (CI/CD)</li>
                                                <li>Experience of working on scalable architecture.</li>
                                                <li>Ability to be hands on by contributing to proof of concepts.</li>
                                                <li>Highly analytical mindset coupled with a commercial and practical approach.</li>
                                                <li>Able to translate technical issues into simple language.</li>
                                            </ul>
                                            <p><strong>Desired but not Essential Skills&nbsp;</strong></p>
                                            <ul>
                                                <li>TOGAF certification</li>
                                                <li>Experience working with Cloud products (Azure, Google Cloud, AWS)</li>
                                                <li>Experience of BPMN beneficial but not essential</li>
                                                <li>Relevant experience of PropTech industry but not essential&nbsp;</li>
                                            </ul>
                                        </div>

                                        <h3>Who we are looking for:</h3>
                                        <div class="text">
                                            <ul>
                                                <li>A senior architect who is able to own &amp; lead our digital &amp; data roadmap.</li>
                                                <li>Span the technology stack, providers, look outward as well as inward, aligning the executive team and product strategy with changes in technology (Has the ability to present opportunity value and benefits to C-level)</li>
                                                <li>Have the gravitas to inspire our teams and help coach and mentor less experienced IT members.</li>
                                                <li>Delivery and goals orientation within a start-up environment</li>
                                                <li>Be a strategic thinker and also a hands-on contributor.</li>
                                                <li>Self-starter who enjoys ownership of driving and shaping architecture (and product) roadmaps</li>
                                                <li>A considerate team player</li>
                                                <li>Strong communicator who can present to internal and external audiences.</li>
                                                <li>Curious with a continuous learning mindset</li>
                                                <li>Business focused.</li>
                                            </ul>
                                        </div>

                                        <h3>Qualifications</h3>
                                        <div class="text">
                                            <ul>
                                                <li>A bachelor’s degree in information technology&nbsp;</li>
                                                <li>Strong written and spoken communication skills.</li>
                                                <li>Proven experience of leading and managing IT programs.</li>
                                                <li>Good working knowledge of IT project estimations</li>
                                                <li>Strong technical background</li>
                                                <li>Relevant certification as an architect (desired)</li>
                                                <li>Agile certification (desired)</li>
                                            </ul>
                                        </div>

                                        <h3>Tool Knowledge</h3>
                                        <div class="text">
                                            <p>MS suite, MS TEAMS, SharePoint, Planview or SAL (AWS) or Bizz Design&nbsp;</p>
                                        </div>
                        
                                        <h3>No of Positions</h3>
                                        <div class="text">
                                            <p>1</p>
                                        </div>

                                        <div class="text show-modal-popup">
                                            <a href="" class="theme-btn btn-style-one apply_career_form" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal1 -->
    <div class="modal fade" id="myModal8" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bio-head" style="padding:10px 20px;">
                        <h3 class="text-center" style="margin-top: 0px;"><strong>System Administrator- IT Infrastructure</strong></h3>
                        <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                    </div>    
                    <div class="bio-content">
                        <!--Form Column-->
                        <div class="form-column col-lg-12 col-md-12 col-sm-12">
                            <div class="inner">
                                <!--Form Box-->
                                <div class="form-box">
                                    <div class="default-form career-form">
                                        <h3>Job Description</h3>
                                        <div class="text">
                                            <p>An exciting opportunity for an Infrastructure Engineer to join an exciting property venture team. This role will involve administering day-to-day IT Infrastructure and Services. Working closely with Architecture, Service Delivery and Service Desk Teams you will effectively support the full life cycle of IT Infrastructure operations, services, projects and initiatives.</p>
                                            <p>As we set-up to meet out digital agenda, the role will evolve to a complex and demanding IT environment and will give ambitious individual a great chance to develop their skills and capabilities to a high level across a range of up-to-date technologies.&nbsp; &nbsp;This role covers both on premise and cloud infrastructure, desktop support, Network management, computer operations, implementation management, environment management and software releases.&nbsp;</p>
                                            <p>Join us to be a key part of a team that is changing the future of the PropTech market, which gives you an opportunity to explore, manage, maintain, and create complex Infrastructure automation solutions to improvise the service delivery.</p>
                                        </div>
                                        <h3>Key Responsibilities:</h3>
                                        <div class="text">
                                            <ul>
                                                <li>Evaluate, Install and manage a wide range of hardware stack from Server, Storage and Network Devices etc..</li>
                                                <li>Create Virtual Environment as per requirement from Business. This includes evaluating, installing, monitoring and maintaining Windows and Linux based software and licenses and servicing hardware in a virtualized environment.</li>
                                                <li>Managing and Maintain Web Servers.</li>
                                                <li>Administer a Windows/Active Directory LDAP environment.</li>
                                                <li>Optimize and maintain the Name Server infrastructure</li>
                                                <li>Analyse and diagnose system failures recover and restore operating systems and file systems.</li>
                                                <li>Perform RCA for Major Incidents, create mitigation and contingency plans.</li>
                                                <li>Provide IT and server-based support to other employee’s internal projects.</li>
                                                <li>&nbsp;Conduct research and recommend changes in services, products, protocols, and standards to support development efforts and infrastructure procurement.</li>
                                                <li>Supervise data centre group and lead, direct and utilize knowledge on best practices in area related to infrastructure.</li>
                                                <li>Establishes and controls systems access and security.</li>
                                                <li>Manages computer operation scheduling, backup, storage and retrieval functions.</li>
                                                <li>Develops, maintains, and tests disaster recovery plans.</li>
                                                <li>Test server performance plus provide network presentation statistics.</li>
                                                <li>Ensure to practice IT asset management inclusive of component inventory maintenance and associated documentation.</li>  
                                            </ul>
                                        </div>

                                        <h3>What we are looking for:</h3>
                                        <div class="text">
                                            <p><strong>Essential Skills&nbsp;</strong></p>
                                            <ul>
                                                <li>4+ Years of Experience with complex architectures within Cloud and Hosting Technologies (Microsoft Azure, AWS, etc...)</li>
                                                <li>Bachelor’s degree in computer science, information technology, electronics, or any related field.</li>
                                                <li>Hands on operating Knowledge working on various operating systems (Windows, LINUX, UNIX etc..)</li>
                                                <li>Good understanding of server technologies and the server team, Like Physical and Virtual Server environment, Operating system, Storage and Backup System.</li>
                                                <li>Deployment, testing, delivery and maintenance for the full life cycle of activities related to Systems and subscriptions on Public &amp; Private Cloud</li>
                                                <li>Designing and implementing solutions leveraging Cloud Components: COMPUTE, STORAGE, NETWORK, SECURITY</li>
                                                <li>Experience on EMC SAN configuration and maintenance</li>
                                                <li>Experience on Data centre and user management and back solutions</li>
                                                <li>Experience on internal and external Audit is preferable.</li>
                                            </ul>
                                        </div>

                                        <h3>Who we are looking for:</h3>
                                        <div class="text">
                                            <ul>
                                                <li>Hands-on experience of installing &amp;configuring Windows servers and Linux servers.</li>
                                                <li>Cloud Infrastructure exposure (AWS, Azure, GCP)</li>
                                                <li>Knowledge of network principals such as TCP/IP, Routing, Firewalls and wireless technologies</li>
                                                <li>Mandatory Skills are to work on DNS, DHCP, Active Directory, Group Policies, Backup, recovery and storage etc..</li>
                                                <li>Operating knowledge of Web server based platforms (Apache-TomCat, IBM Websphere, Oracle Weblogic)</li>
                                                <li>Operating knowledge on Windows Based Platforms, LINUX, UNIX.</li>
                                                <li>Understanding in Communication protocols, MQ, NDM, Apache-Kafka</li>
                                                <li>Experience of working directly with customer technical teams to implement and test solutions. Clear methodical thinker able to work under pressure.</li>
                                                <li>ITIL certification will be added advantage.</li>
                                                <li>Strong interpersonal and communication skills</li>
                                                <li>Experience working for a Managed Service Provider</li>
                                                <li>Knowledge on Security/Management (MDM, Firewalls - Fortigate, SIEM, CEH, CompTIA)</li>
                                                <li>Availability outside of working hours to resolve emergency issues promptly.</li>
                                                <li>Strong written and spoken communication skills.</li>
                                            </ul>
                                        </div>

                                        <h3>Qualifications</h3>
                                        <div class="text">
                                            <ul>
                                                <li>Bachelor’s degree in computer science, information technology, or any related field.</li>
                                                <li>Proven experience of managing IT Infrastructure or Data center experience.</li>
                                                <li>Knowledge in managing Linux, Workstation, Win10, Directory Services, ACTIVE Directory, Group Policy, WSUS management</li>
                                            </ul>
                                        </div>

                                        <h3>Tools Knowledge</h3>
                                        <div class="text">
                                            <p>JIRA software, SolarWinds, ServiceNow, Kaseya Monitoring and Management tool</p>
                                        </div>
                                                                
                                        <h3>No of Positions</h3>
                                        <div class="text">
                                                <p>1</p>
                                        </div>
                                    
                                        <div class="text show-modal-popup">
                                            <a href="" class="theme-btn btn-style-one apply_career_form" data-toggle="modal" data-target="#myModal1">
                                                <div class="btn-title">Apply</div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal1 -->
    <div class="modal fade" id="myModal9" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bio-head" style="padding:10px 20px;">
                        <h3 class="text-center" style="margin-top: 0px;"><strong>Development Team Lead</strong></h3>
                        <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                    </div>    
                    <div class="bio-content">
                        <!--Form Column-->
                        <div class="form-column col-lg-12 col-md-12 col-sm-12">
                            <div class="inner">
                                <!--Form Box-->
                                <div class="form-box">
                                    <div class="default-form career-form">
                                        <h3>Job Description:</h3><div class="text">
                                        <p>Leading Aurum PropTech Development Team, person will be responsible for working with Product Development and User Experience team. Your Role is key role to Develop right quality software as per company standards and delivery it to Project Team and customers.</p>
                                        <p>As an Experience Team Leader, whilst highly technical and hands-on capable, you will lead development project using multiple technologies and platforms. Managing the successful delivery of projects will require you to plan, coordinate and lead activities across the full delivery lifecycle. This includes, working closely with Project Manager, Coordinate with Business Analyst, Architects and Developer.</p>
                                        <p>You will be responsible for Structured and coordinated planning with your Team. Your technical background and experience will ensure you can offer the support and guidance required being able to support and assist developers at code level.&nbsp;</p>
                                        <p>This role offers challenges across a wide variety of projects and responsibilities, including the opportunity to influence the future direction of the organization and systems used across the business.</p>
                                        </div><h3>Key Responsibilities:</h3><div class="text">
                                        <ul>
                                        <li>Managing the delivery of multiple complex simultaneous system development projects from design through to release.</li>
                                        <li>Work closely with Solution Architects and Domain Experts in determining the technologies, toolsets and frameworks that will be used across Aurum PropTech with the aim of maximising the efficiency and productivity.&nbsp;</li>
                                        <li>Expected to be hands-on taking on some of the development responsibilities. As Aurum PropTech grow over time you will become responsible for several cross development functional</li>
                                        <li>You will be expected to put in places practices and procedures and select toolsets that enable end to end Agile product development and a DevOps model.</li>
                                        <li>You will be expected to manage relationships with, and delivery from, 3rd party consultancies that will augment the team with specialist expertise.&nbsp; There will be an element of requirement to visit and operate of 3rd party vendor sites.</li>
                                        <li>You will be expected to set code standards and ensure that both internal and 3rd party developers adhere to these standards.</li>
                                        <li>Create and deliver POCs and Pilots to gather initial feedback from Testers.</li>
                                        <li>Define delivery phases of the project including activities, sub-activities, and milestones ensuring these are documented and used as the basis for the project event log, issues and risk log and any subsequent reporting.</li>
                                        <li>Ownership, development, and management of allocated Development Team.</li>
                                        <li>Analyse and resolve technical and application problems.</li>
                                        <li>Research and evaluate a variety of software products.</li>
                                        <li>Participate in reviews and meetings and provide updates on project progress.</li>
                                        <li>Provide technical leadership to teammates through coaching and mentorship.</li>
                                        <li>Contributing to post implementation reviews helping to demonstrate success or otherwise of projects.</li>
                                        <li>Take responsibility for making key decisions to ensure the successful implementation of all initiatives.</li>
                                        </ul>
                                        </div><h3>What we are looking for</h3><div class="text">
                                        <p><strong>Essential Skills&nbsp;</strong></p>
                                        <ul>
                                        <li>10+ years’ experience in Software Development.</li>
                                        <li>Prior experience in a technical leadership position.</li>
                                        <li>Bachelor’s degree in computer science, information technology, or any related field.</li>
                                        <li>Capable of understanding and contributing to the technical solution from design through to code level.</li>
                                        <li>Extensive experience designing and developing enterprise grade software.</li>
                                        <li>Experience with automated testing</li>
                                        <li>Experience with agile development methodologies including Kanban and Scrum.</li>
                                        <li>Experience with debugging, performance profiling and optimization</li>
                                        <li>Comprehensive understanding of object-oriented and service-oriented application development techniques and theories</li>
                                        <li>Experience with web development technologies including ASP.NET, MVC3, JavaScript, and CSS</li>
                                        <li>Experience with database development including relational database design, SQL and ORM.</li>
                                        <li>Experience with user interface design and prototyping</li>
                                        </ul>
                                        </div><h3>Who we are looking for:</h3><div class="text">
                                        <ul>
                                        <li>A proven track record in building and running development and operations teams (ideally cross functional DevOps teams).</li>
                                        <li>Experience of managing delivery teams across 3rd party suppliers.</li>
                                        <li>A strong software development background, ideally Java/Node.JS/Angular</li>
                                        <li>Experience of Agile product development.</li>
                                        <li>Practical experience of the application of Continuous Improvement in a Learning Organisation.</li>
                                        <li>Experience in developing UX-focused applications.</li>
                                        <li>Strong communication skills with both internal team members and external business stakeholders</li>
                                        <li>Experience in people management and the ability to lead and influence others.</li>
                                        <li>Experience in Outsourcing and vendor management</li>
                                        <li>A desire to remain technically capable and an expert in current technologies.</li>
                                        </ul>
                                        </div><h3>Qualifications</h3><div class="text">
                                        <ul>
                                        <li>Bachelor’s degree in computer science, information technology or electronics,&nbsp;</li>
                                        <li>Proficient with Java, Nodejs, .Net Core, React or AngularJs and SQL</li>
                                        <li>In-depth knowledge and experience with developing web applications or Mobile Applications&nbsp;</li>
                                        <li>C And C++ Certifications</li>
                                        </ul>
                                        </div><h3>Tools Knowledge</h3><div class="text">
                                        <p>JavaScript, HTML or AJAX, JavaScript, .NET and Objective-C</p>
                                                                    
                                                                        </div><h3>No of Positions</h3><div class="text">
                                                                        <p>1</p></div>              
                                            <div class="text show-modal-popup">
                                            <a href="" class="theme-btn btn-style-one apply_career_form" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal1 -->
    <div class="modal fade" id="myModal10" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bio-head" style="padding:10px 20px;">
                        <h3 class="text-center" style="margin-top: 0px;"><strong>iOS Developer</strong></h3>
                        <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                    </div>    
                    <div class="bio-content">
                        <!--Form Column-->
                        <div class="form-column col-lg-12 col-md-12 col-sm-12">
                            <div class="inner">
                                <!--Form Box-->
                                <div class="form-box">
                                    <div class="default-form career-form">
                                        <h3>Job Description:</h3>
                                        <div class="text">
                                            <p>Part of Aurum PropTech Development Team, person will be responsible for working with Development Manager. Your Role is to Develop right quality Code for application which will be deployed on Apple Store. You will be responsible for the development, design and implementation of new or modified software products and release to app stores with ongoing support.</p>
                                            <p>You will assure quality of application, fixing application bugs, maintaining the code, and implementing application updates. You will be working closely with other developers, UX designers, business, and systems analysts.</p>
                                            <p>To ensure success as an iOS Developer, you should have a strong working knowledge of iOS Frameworks, be proficient in Objective-C, and be able to work as part of a team. Ultimately, an outstanding iOS Developer should be able to create functional, attractive applications that perfectly meet the needs of the user.</p>
                                        </div>
                                        <h3>Key Responsibilities:</h3>
                                        <div class="text">
                                            <ul>
                                            <li>Researching, Design, write and implement high Quality Codes for Apple’s iOS platform.</li>
                                            <li>Ensure the performance, quality, and responsiveness of applications.</li>
                                            <li>Develop technical documentation to support application maintenance.</li>
                                            <li>Developing quality assurance procedures, Identify and correct bottlenecks , Testing code and Bug Fixing.</li>
                                            <li>Work closely with product management &amp; UX to execute an idea from concept to delivery using excellent software design, coding, &amp; processes</li>
                                            <li>Adhere to Coding standard set by the Development Manager.</li>
                                            <li>Worked closely with Tester to get feedback on POCs and Pilots.</li>
                                            <li>Partner with UX designers, product managers and back-end engineers to build intuitive user interfaces from the ground up using the latest web technologies.</li>
                                            </ul>
                                        </div>
                                        <h3>What we are looking for</h3>
                                        <div class="text">
                                            <p><strong>Essential Skills&nbsp;</strong></p>
                                            <ul>
                                            <li>4 years’ experience in iOS&nbsp; Development.</li>
                                            <li>Knowledge of Apple’s design principles and application interface guidelines.</li>
                                            <li>Hands on IOS App developments with strong working knowledge of iOS Framework</li>
                                            <li>Experience with iOS frameworks such as Core Data, Core Animation, etc</li>
                                            <li>Proficient in Objective-C, Swift, and Cocoa Touch.&nbsp;</li>
                                            <li>Familiarity with RESTful APIs to connect iOS applications to back-end services</li>
                                            <li>Knowledge of other web technologies and UI/UX standards</li>
                                            <li>Independently craft project solutions by applying solid Object-Oriented-Design principles</li>
                                            <li>Knowledge of low-level C-based libraries is preferred</li>
                                            <li>Experience with performance and memory tuning with tools.</li>
                                            <li>Familiarity with cloud message APIs and push notifications</li>
                                            <li>Knack for benchmarking and optimization</li>
                                            <li>Proficient understanding of code versioning tools</li>
                                            <li>Familiarity with continuous / Agile Methodology integration</li>
                                            <li>Knowledge of Size classes and Auto Layout.</li>
                                            <li>Good experience in memory management techniques, code analysis, debugging and profiling tools available within XCode</li>
                                            </ul>
                                        </div>
                                        <h3>Who we are looking for:</h3>
                                        <div class="text">
                                            <ul>
                                            <li>Bachelor’s degree in Computer Science, Engineering or a related subject</li>
                                            <li>4+&nbsp; years of experience with iOS, Objective-C, Cocoa</li>
                                            <li>Hands on experience with to HTML5, CSS, XML, API</li>
                                            <li>Coursework in Object-Oriented programming languages (C++/Java, etc)</li>
                                            <li>Must have experience in Mac OS platforms</li>
                                            <li>A passion for technology and the ability to learn new concepts quickly</li>
                                            <li>Solid understanding of the full mobile development life cycle</li>
                                            </ul>
                                        </div>
                                        <h3>Qualifications</h3>
                                        <div class="text">
                                            <ul>
                                            <li>Bachelor’s degree in computer science, information technology or electronics,&nbsp;</li>
                                            <li>Strong Experience in Objective-C, Swift, and Cocoa Touch</li>
                                            <li>Knowledge of HTML5, CSS, XML, API&nbsp;</li>
                                            </ul>
                                            </div><h3>Tools Knowledge</h3><div class="text">
                                            <p>XCode / AppCode / Code Runner,&nbsp; AZURE, Jira, Dreamweaver etc..</p>
                                                                    
                                        </div>
                                        <h3>No of Positions</h3>
                                        <div class="text">
                                            <p>1</p>
                                        </div>    
                                        <div class="text show-modal-popup">
                                            <a href="" class="theme-btn btn-style-one apply_career_form" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal1 -->
    <div class="modal fade" id="myModal11" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bio-head" style="padding:10px 20px;">
                        <h3 class="text-center" style="margin-top: 0px;"><strong>Software Developer-Android</strong></h3>
                        <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                    </div>    
                    <div class="bio-content">
                        <!--Form Column-->
                        <div class="form-column col-lg-12 col-md-12 col-sm-12">
                            <div class="inner">
                                <!--Form Box-->
                                <div class="form-box">
                                    <div class="default-form career-form">
                                        <h3>Job Description:</h3><div class="text">
                                        <p>Part of Aurum PropTech Development Team, person will be responsible for working with Development Manager and developing applications for devices powered by the Android operating system. Due to the fragmentation of this ecosystem, you must pay special attention to the application’s compatibility with multiple versions of Android and device types.</p>
                                        <p>You will be working closely with other developers, UX designers, business, and systems analysts and responsible Develop, test, and analyse applications as well as Determine, develop, and document software specifications throughout production lifecycle. Analyse and rectify software errors .</p>
                                        <p>To ensure success as an Android Developer, you should demonstrate proficiency in one of the mainstream programming languages, and a sound understanding of the traditional product life cycle. An exceptional Android Developer will be fearless but respectful in the pursuit of excellence, continually striving to shape the ways in which Android apps impact the world around us.</p>
                                        </div><h3>Key Responsibilities:</h3><div class="text">
                                        <ul>
                                        <li>Researching, Design, write and implement high Quality Codes for Android-based Application.</li>
                                        <li>Collaborate with cross-functional teams to define, design, and Implement new features.</li>
                                        <li>Develop technical documentation to support application maintenance.</li>
                                        <li>Developing quality assurance procedures, Testing code and Bug Fixing.</li>
                                        <li>Monitoring the technical performance of Application, upgrade and offer patches to existing code to improve performance.</li>
                                        <li>Adhere to Coding standard set by the Development Manager.</li>
                                        <li>Worked closely with Tester to get feedback on POCs and Pilots.</li>
                                        <li>Continuously discover, evaluate, and implement new technologies to maximize development efficiency.</li>
                                        </ul>
                                        </div><h3>What we are looking for</h3><div class="text">
                                        <p><strong>Essential Skills&nbsp;</strong></p>
                                        <ul>
                                        <li>4 years’ experience in Software Development.</li>
                                        <li>Bachelor’s degree in computer science, information technology, or any related field.</li>
                                        <li>Proven experience with Java, Kotlin, or C++.</li>
                                        <li>Have published at least one original Android app</li>
                                        <li>Experience with Android SDK</li>
                                        <li>Experience working with remote data via REST and JSON</li>
                                        <li>Experience with third-party libraries and APIs</li>
                                        <li>Experience with user interface design and prototyping</li>
                                        <li>Experience publishing a high-quality Android application to the Google Play Store</li>
                                        </ul>
                                        </div><h3>Who we are looking for:</h3><div class="text">
                                        <ul>
                                        <li>Must also have a strong understanding of the patterns and practices that revolve around such a platform.</li>
                                        <li>Experience with Native platforms for mobile development like Analytics, Push Notifications, Cloud Storage, Ad Networks,</li>
                                        <li>Strong knowledge of Android UI design principles, patterns, and best practices.</li>
                                        <li>Experience with offline storage, threading, and performance tuning.</li>
                                        <li>Knowledge of the open-source Android ecosystem and the libraries available for common tasks</li>
                                        <li>Ability to understand business requirements and translate them into technical requirements.</li>
                                        <li>Familiarity with cloud message APIs and push notifications</li>
                                        </ul>
                                        </div><h3>Qualifications</h3><div class="text">
                                        <ul>
                                        <li>Bachelor’s degree in computer science, information technology or electronics,&nbsp;</li>
                                        <li>knowledge of Android SDK, JAVA, XML, Android Studio, APIs, Database and Material Design.</li>
                                        <li>knowledge and experience with developing Mobile Applications.</li>
                                        <li>C And C++ Certifications</li>
                                        </ul>
                                        </div><h3>Tools Knowledge</h3><div class="text">
                                        <p>Eclipse, Android Studio, Android SDK, AZURE, Jira, Dreamweaver etc..</p>
                                                                    
                                                                        </div><h3>No of Positions</h3><div class="text">
                                                                        <p>1</p></div>
                                        <div class="text show-modal-popup">
                                            <a href="" class="theme-btn btn-style-one apply_career_form" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal1 -->
    <div class="modal fade" id="myModal12" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bio-head" style="padding:10px 20px;">
                        <h3 class="text-center" style="margin-top: 0px;"><strong>Full Stack developer</strong></h3>
                        <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                    </div>    
                    <div class="bio-content">
                        <!--Form Column-->
                        <div class="form-column col-lg-12 col-md-12 col-sm-12">
                            <div class="inner">
                                <!--Form Box-->
                                <div class="form-box">
                                    <div class="default-form career-form">
                                        <h3>Job Description:</h3><div class="text">
                                        <p>Part of Aurum PropTech Development Team, person will be responsible for working with Development Manager and assist with all functions of software coding and design. You will be working closely with other developers, UX designers, business, and systems analysts.</p>
                                        <p>You will be responsible Develop, Test, and Analyse programming applications in Agile method, as well as Determine, develop, and document software specifications throughout production lifecycle. Analyse and rectify software errors and present precise status reports on time.</p>
                                        <p>Your primary focus will be the development of all server-side logic, definition and maintenance of the central database, and ensuring high performance and responsiveness to requests from the front-end. You will also be responsible for integrating the front-end elements built by your co-workers into the application. Therefore, a basic understanding of front-end technologies is necessary as well.</p>
                                        <p>To ensure success as a Software Developer, you should have a good working knowledge of basic programming languages, the ability to learn new technology quickly, and the ability to work in a team environment. Ultimately, a top-class Software Developer provides valuable support to the design team while continually improving their coding and design skills.</p>
                                        <p>&nbsp;</p>
                                        </div><h3>Key Responsibilities:</h3><div class="text">
                                        <ul>
                                        <li>Developing high-performance applications by writing testable, reusable, and efficient code.&nbsp;</li>
                                        <li>Cooperate with web designers to match visual design intent.</li>
                                        <li>Develop technical documentation to support application maintenance.</li>
                                        <li>Documenting Node.js processes, including database schemas, as well as preparing reports.</li>
                                        <li>Developing and maintaining all server-side network components.</li>
                                        <li>Developing quality assurance procedures, Testing website and codes for Bug Fixing.</li>
                                        <li>Monitoring the technical performance of Application, upgrade and offer patches to existing code to improve performance.</li>
                                        <li>Implementing effective security protocols, data protection measures, and storage solutions.</li>
                                        <li>Ensuring optimal performance of the central database and responsiveness to front-end requests.</li>
                                        <li>Worked closely with Tester to get feedback on POCs and Pilots.</li>
                                        <li>Evaluating code to ensure it meets industry standards, is valid, is properly structured, and is compatible with browsers, devices, or operating systems.</li>
                                        <li>Coordinate with other designers and programmers to develop web projects.</li>
                                        <li>Collaborate with staff and teams to develop, format, and deploy content.</li>
                                        <li>Assist and support in the upkeep and maintenance of web sites and Optimising sites for maximum speed and scalability.</li>
                                        <li>Implementing contingency plans in case the website goes down.</li>
                                        </ul>
                                        </div><h3>What we are looking for</h3><div class="text">
                                        <p><strong>Essential Skills&nbsp;</strong></p>
                                        <ul>
                                        <li>6+ years’ experience in either in .Net or Java and At least two years' experience as a Node.js developer.</li>
                                        <li>Knowledge of Node.js and frameworks available for it.</li>
                                        <li>At least two years' experience as a Node.js developer.</li>
                                        <li>Individual with experience of working on mission critical three tier architecture platform, build either using .Net or Java programming.</li>
                                        <li>Working knowledge on LINUX and/or UNIX platform.</li>
                                        <li>Experience of developing Web based Platforms is pre-requisite.&nbsp;</li>
                                        <li>Maintaining an understanding of the latest Web applications and programming practices through education, study, and participation in conferences, workshops, and groups.</li>
                                        </ul>
                                        </div><h3>Who we are looking for:</h3><div class="text">
                                        <ul>
                                        <li>Solid knowledge and experience in programming applications, specially .Net and Java based platforms.</li>
                                        <li>Strong knowledge of multiple front-end languages and libraries (e.g. HTML/ CSS, JavaScript, XML, jQuery)</li>
                                        <li>Knowledge of multiple back-end languages (e.g. C#, Java, Python) and JavaScript frameworks (e.g. Angular, React, Node.js)</li>
                                        <li>Operating knowledge of Web server-based platforms (Apache-Tomcat, IBM WebSphere, Oracle WebLogic)</li>
                                        <li>Operating knowledge on Windows Based Platforms, LINUX, UNIX.</li>
                                        <li>Understanding in Communication protocols, MQ, NDM, Apache-Kafka.</li>
                                        <li>Familiarity with databases (e.g. MySQL, MongoDB), and UI/UX design</li>
                                        <li>Understanding of fundamental design principles behind a scalable application</li>
                                        <li>Understanding of Software Development Life Cycle and Agile methodologies.</li>
                                        <li>Ability to implement automated testing platforms and unit tests.</li>
                                        <li>Proficient understanding of code versioning tools</li>
                                        <li>Familiarity with development aiding tools</li>
                                        </ul>
                                        </div><h3>Qualifications</h3><div class="text">
                                        <ul>
                                        <li>Bachelor’s degree in computer science, BCA, MCA, Or information technology</li>
                                        <li>Proficient with .Net, Java, Jquery, JavaScript (Angular, React, Node.js) and SQL, AngularJS, PHP,&nbsp;</li>
                                        <li>knowledge and experience with developing web applications&nbsp;</li>
                                        <li>C And C++ Certifications</li>
                                        <li>Certification in Responsive Web Design will have added advantage.</li>
                                        </ul>
                                        <p>&nbsp;</p>
                                        </div><h3>Tools Knowledge</h3><div class="text">
                                        <p>.Net, Core Java, HTML5 Builder, AZURE, Jira, Dreamweaver etc.</p><p></p>
                                                                    
                                                                        </div><h3>No of Positions</h3><div class="text">
                                                                        <p>3</p></div>
                                        <div class="text show-modal-popup">
                                            <a href="" class="theme-btn btn-style-one apply_career_form" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal1 -->
    <div class="modal fade" id="myModal13" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bio-head" style="padding:10px 20px;">
                        <h3 class="text-center" style="margin-top: 0px;"><strong>Graphic Designer</strong></h3>
                        <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                    </div>    
                    <div class="bio-content">
                        <!--Form Column-->
                        <div class="form-column col-lg-12 col-md-12 col-sm-12">
                            <div class="inner">
                                <!--Form Box-->
                                <div class="form-box">
                                    <div class="default-form career-form">
                                        <h3>Job Description:</h3><div class="text">
                                        <p>Working as part of Aurum Digital Team, Graphic Designer will have to visualizing and creating graphics including illustrations, logos, layouts, Application Pages, and photos. You will be the one to shape the visual aspects of websites, Mobile Application pages which are compatible to Android as well as iOS.</p>
                                        <p>You will also work with Branding and Marketing team to create Aurum’s Marketing Collators.&nbsp; You will work to create Brand guidelines. You will work on a variety of products and activities, such as websites, computer Applications, posters, Brochures, Digital campaigns, corporate communications and corporate identity, i.e., giving organizations a visual brand.</p>
                                        <p>Your graphics should capture the attention of those who see them and communicate the right message. For this, you need to have a creative flair and a strong ability to translate requirements into design. If you can communicate well and work methodically as part of a team, we would like to meet you.</p>
                                        <p>&nbsp;</p>
                                        </div><h3>Graphic Designer Responsibilities:</h3><div class="text">
                                        <ul>
                                        <li>Works effectively and individually to Study design briefs and determine requirements.</li>
                                        <li>Work with Product Development to create customer experience as part of UI/UX team.</li>
                                        <li>Work with Marketing team, to create Campaign designs, Brochures and Digital content</li>
                                        <li>Conceptualizing visuals based on requirements.</li>
                                        <li>Prepare rough drafts and present ideas.</li>
                                        <li>Creating designs by hand drawing or painting or by using Technology &amp; Tools or using computer software to achieve similar ends.</li>
                                        <li>Visualizing and designing websites templates, UI designs, Creative web banners, e- mailers, creative logos, and web-based art- works as per current market trend &amp; Business requirement.</li>
                                        <li>Obtains approval of concept by submitting rough layout for approval.</li>
                                        <li>Use digital illustration, photo editing software, and layout software to create designs.</li>
                                        <li>Determining size and arrangement of copy and illustrative material, as well as font style and size</li>
                                        <li>Design layouts, including selection of colours, images, and typefaces as per Brand Guideline.</li>
                                        <li>Working with a range of media, including computer-aided design (CAD), and keeping up to date with emerging technologies</li>
                                        <li>Test graphics across various Medias and Platforms</li>
                                        <li>Ensure final graphics and layouts are visually appealing and on-brand.</li>
                                        </ul>
                                        <p>&nbsp;</p>
                                        </div><h3>What we are looking for:</h3><div class="text">
                                        <p><strong>Essential Skills&nbsp;</strong></p>
                                        <ul>
                                        <li>Bachelor's degree with a major or concentration in graphic design, either at a traditional college or an art institute.</li>
                                        <li>A minimum of 3+ years of Experienced Graphic Designer with a demonstrated history in Application Development Projects.&nbsp;</li>
                                        <li>Proven graphic designing experience in designing Social Media Campaign, Digital banners, offline collaterals.</li>
                                        <li>Proven Experience working in the industry as a Lead Graphic Designer.</li>
                                        <li>Any experience working in Real Estate Industry is added Advantage.</li>
                                        <li>Professional graduated from Reputed University/School of ART.</li>
                                        <li>Degree in Design, Fine Arts or related field is a plus.</li>
                                        <li>Strong skilled in CorelDRAW, HTML, Adobe Photoshop, InDesign and/or Dreamweaver.&nbsp;</li>
                                        <li>A knowledge of both typography and colour theory is critical.</li>
                                        <li>Accuracy and attention to detail</li>
                                        </ul>
                                        <p>&emsp;</p>
                                        </div><h3>Who we are looking for:</h3><div class="text">
                                        <ul>
                                        <li>Candidate must be able to confer advice and suggestions from clients, as well as understand what they want.</li>
                                        <li>Designers must be able to collaborate and get along with others when they work as part of a design team.</li>
                                        <li>Excellent Creativity &amp; Ability to handle multiple projects simultaneously.</li>
                                        <li>Exceptional creativity and innovation</li>
                                        <li>Excellent communication skills</li>
                                        <li>Ability to work methodically and meet deadlines.</li>
                                        <li>Handles rejection.</li>
                                        </ul>
                                        </div><h3>Qualifications</h3><div class="text">
                                        <ul>
                                        <li>Bachelor’s Degree in Graphic Design Or Master of Arts.</li>
                                        <li>Degree in computer Applications is added advantage.</li>
                                        <li>Certification in DTP Graphic designing&nbsp;</li>
                                        <li>Strong Communication skill with ability to influence senior management.</li>
                                        </ul>
                                        </div><h3>Tool Knowledge</h3><div class="text">
                                        <p>ProofHub, Adobe Photoshop, Adobe Illustrator, CorelDraw, InDesign</p>
                                        </div>  
                                        <h3>No of Positions</h3>
                                        <div class="text">
                                            <p>1</p>        
                                        </div>              
                                            <div class="text show-modal-popup">
                                            <a href="" class="theme-btn btn-style-one apply_career_form" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal1 -->
    <div class="modal fade" id="myModal14" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bio-head" style="padding:10px 20px;">
                        <h3 class="text-center" style="margin-top: 0px;"><strong>UI_UX Designer</strong></h3>
                        <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                    </div>    
                    <div class="bio-content">
                        <!--Form Column-->
                        <div class="form-column col-lg-12 col-md-12 col-sm-12">
                            <div class="inner">
                                <!--Form Box-->
                                <div class="form-box">
                                    <div class="default-form career-form">
                                        <h3>Job Description</h3><div class="text">
                                        <p>This is a role as part of our product development, and you will be responsible for supporting Aurum PropTech to create delightful digital experiences by building better digital services.</p>
                                        <p>The role will provide direction of the Digital Design to Aurum PropTech ensuring the capability is at the required standard and volume to support our Digital ambitions.</p>
                                        <p>The role will require you to have significant experience in strategic design / digital transformation, with a portfolio that demonstrates systematic and creative design thinking and human-centered design, as well as a deep understanding of UI/UX best practices.&nbsp; &nbsp;It will entail you having a proven track record in a digital design role and experience of coaching and developing a team.&nbsp; &nbsp;The ability to understand wider market and customer trends and translate into the organization's digital strategy is a pre-requites for this role.</p>
                                        <p>You should be able to understand our business requirements and any technical limitations, as well as be responsible for conceiving and conducting user research, interviews and surveys, and translating them into sitemaps, user flows, customer journey maps, wireframes, mock-ups and prototypes.</p>
                                        <p>If you have a demonstrable creative side of yours for effective user experience and there is no limit to exploring, then come join us at Aurum PropTech, to spearhead your creative digital function</p>
                                        </div><h3>Key Responsibilities:</h3><div class="text">
                                        <ul>
                                        <li>Leading on efforts to implement consistent, high-quality, and user-centric experiences across Aurum PropTech portfolio</li>
                                        <li>Analyzing user requirements and product strategies to conceive and design world-class user interfaces for a range of products that run on desktop, mobile, and tablet (IOS &amp; Android devices).</li>
                                        <li>Promoting a culture of design excellence within the Aurum PropTech.</li>
                                        <li>Contributing to the establishment of best practice techniques and methods for all aspects of UX, Interaction Design and Visual Design, including research, prototyping, testing and implementation</li>
                                        <li>Overseeing the introduction of a library of internal UI design patterns, components and a shared design vocabulary</li>
                                        <li>Playing a major role in the discovery and innovation activities around the early-stage formation of new products and features</li>
                                        <li>Build storyboards to conceptualize designs and convey project plans to clients and management.</li>
                                        <li>Translate concepts into user flows, wireframes, mock-ups and prototypes that lead to intuitive user experiences.</li>
                                        <li>Design and deliver wireframes, user stories, user journeys, and mock-ups optimized for a wide range of devices and interfaces.</li>
                                        <li>Identify design problems and devise elegant solutions, Prepare and present rough drafts to internal teams and key stakeholders.</li>
                                        <li>Develops website and mobile layout designs, graphics, and landing pages.</li>
                                        <li>Make strategic design and user-experience decisions related to core, and new, functions and features.</li>
                                        <li>Analyse customer responses and website data to determine high traffic web pages and mobile Interfaces.</li>
                                        <li>Conduct testing of completed applications, websites and software to Assess user experience.</li>
                                        </ul>
                                        </div><h3>What we are looking for:</h3><div class="text">
                                        <p><strong>Essential Skills&nbsp;</strong></p>
                                        <ul>
                                        <li>5+ years of UX design experience. Preference will be given to candidates who have experience designing complex solutions for complete digital environments.</li>
                                        <li>A portfolio of professional UI/UX design work for both web and mobile platforms.</li>
                                        <li>Expertise in standard UX software such as Sketch, InVision, UXPin etc..&nbsp;</li>
                                        <li>Proficiency in HTML5, CSS3, JavaScript, jQuery Mobile, and ability to troubleshoot.</li>
                                        <li>Solid grasp of user-centered design (UCD), planning and conducting user research, user testing, rapid prototyping, usability and accessibility concerns.</li>
                                        <li>Solid experience in creating wireframes, storyboards, user flows, process flows and site maps</li>
                                        <li>Proficiency in Photoshop, Illustrator, Visual design and wire-framing tools</li>
                                        <li>Knowledge of Adobe Illustrator.</li>
                                        <li>Attention to details and creative thinking – the ability to think out the box!</li>
                                        </ul>
                                        </div><h3>Who we are looking for:</h3><div class="text">
                                        <ul>
                                        <li>You will have previously exhibited exceptional performance in a UX, UI, or Product design role. You will be expected to be amongst the best, and to continue to develop and hone your craft to remain the best.&nbsp; You will also bring:</li>
                                        <li>Strong capabilities to conceive, design and promote outstanding user interfaces, experiences and flows which match the very best of web-based applications</li>
                                        <li>Deep understanding of the principles of good Information Architecture, Interaction Design and Visual Design (typography, layout, use of color and spacing, responsive design etc.)</li>
                                        <li>Understanding of modern UI implementation approaches i.e. component-based composability &amp; reusability</li>
                                        <li>Skilled in major graphics design software, prototyping software and tools (e.g. InDesign, Illustrator, XD)</li>
                                        <li>Intimate knowledge of the opportunities and limitations presented by web-based platforms, including hand-on experience in modern HTML/CSS layout techniques.</li>
                                        <li>Deep understanding of mobile-first and responsive design</li>
                                        <li>In-depth knowledge of Android/iOS mobile UI design patterns</li>
                                        <li>A strong believer of mobile-first and responsive design</li>
                                        <li>Strong illustration and mobile website design ability with sensitivity to user-system interaction</li>
                                        <li>Excellent communication skills and empathy for the user</li>
                                        <li>Be passionate about resolving user pain points through great design.</li>
                                        <li>Knowledge of wireframe tools</li>
                                        <li>Be excited about collaborating and communicating closely with teams and other stakeholders via a distributed model, to regularly deliver design solutions for approval.</li>
                                        </ul>
                                        </div><h3>Qualifications</h3><div class="text">
                                        <ul>
                                        <li>Mastery of Adobe Creative Suite (Photoshop, Illustrator, Flash, Fireworks, etc) and CorelDraw, Sketch</li>
                                        <li>PHP fair knowledge is added Advantage.</li>
                                        <li>3-5 years of UI/UX design experience</li>
                                        <li>Graphic designer or web designer</li>
                                        <li>Professional written and interpersonal skills</li>
                                        </ul>
                                        </div><h3>Tools Knowledge</h3><div class="text">
                                        <p>Sketch or Adobe Xd or flowmap, Wireframe</p>
                                                                    
                                        </div><h3>No of Positions</h3><div class="text">
                                            <p>2</p>
                                        </div>              
                                            <div class="text show-modal-popup">
                                            <a href="" class="theme-btn btn-style-one apply_career_form" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal1 -->
    <div class="modal fade" id="myModal15" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bio-head" style="padding:10px 20px;">
                        <h3 class="text-center" style="margin-top: 0px;"><strong>AWS Cloud Engineer</strong></h3>
                        <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                    </div>    
                    <div class="bio-content">
                        <!--Form Column-->
                        <div class="form-column col-lg-12 col-md-12 col-sm-12">
                            <div class="inner">
                                <!--Form Box-->
                                <div class="form-box">
                                    <div class="default-form career-form">
                                        <h3>Job Description:</h3><div class="text">
                                        <p>An exciting opportunity for Cloud Infrastructure Engineer to join an exciting property venture team. This role will involve design, configuration, and support of cloud enterprise application platforms.&nbsp; Working closely with Cloud Architect you will design, manage, monitor, support and troubleshoot cloud Infrastructure. The position requires working with many other technology/Functional groups to ensure the optimal stability and timely delivery of solutions for our customers.&nbsp;</p>
                                        <p>As we set-up to meet out digital agenda, the role will evolve to a complex and demanding IT environment and will give an ambitious individual a great chance to develop their skills and capabilities to a high level across a range of up-to-date technologies</p>
                                        <p>Join us to be a key part of a team that is changing the future of PropTech market, which gives you opportunity to explore, manage, maintain, and create complex Cloud solutions to improvise the service delivery.</p>
                                        </div><h3>Key Responsibilities:</h3><div class="text">
                                        <ul>
                                        <li>Collaborating with development teams to evaluate and identify optimal cloud solutions.</li>
                                        <li>Developing and maintaining cloud solutions in accordance with best practices.</li>
                                        <li>Designing, developing and deploying modular cloud-based systems.</li>
                                        <li>Identifying, analyzing, and resolving infrastructure vulnerabilities and application deployment issues.</li>
                                        <li>Ensuring efficient functioning of data storage and processing functions in accordance with company security policies and best practices in cloud security.</li>
                                        <li>Developing and implementing automated cloud solutions using tools.</li>
                                        </ul>
                                        </div><h3>What we are looking for:</h3><div class="text">
                                        <p>&nbsp;<strong>Essential Skills&nbsp;</strong></p>
                                        <ul>
                                        <li>3+ Years of Experience with complex architectures within Cloud and Hosting Technologies, AWS.</li>
                                        <li>Bachelor’s degree in computer science, information technology, electronics, or any related field.</li>
                                        <li>Excellent knowledge of cloud computing technologies and current computing trends.</li>
                                        <li>Expert-Level experience in Configuring and Administration of High Availability environment</li>
                                        <li>Good understanding of Cloud server Storage and Backup System.</li>
                                        <li>Implementing solutions leveraging Cloud Components: COMPUTE, STORAGE, NETWORK, SECURITY</li>
                                        <li>Experience on internal and external Audit is preferable.</li>
                                        </ul>
                                        </div><h3>Who we are looking for:</h3><div class="text">
                                        <ul>
                                        <li>Hands-on experience of installing &amp;configuring AWS Cloud Servers.</li>
                                        <li>Strong interpersonal and communication skills</li>
                                        <li>Hands-on experience with AWS services (VPC, IAM, EC2, RDS, Route53, ElasticCache, S3, Glacier, Inspector, Elastic Beanstalk, AWS Whitepapers, Lambada, CloudFront, CodeCommit, CodeDeploy, SES, SNS, Kinesis, Certificate Manager etc.)&nbsp;</li>
                                        <li>Hands-on experience with AWS Security and understanding of SSH, Firewalls, Iptable, PKI, Public key, Private Key, Cipher suites. TLS and SSL. Encryption, Hashing and MAC. SSL certificates, root and intermediate certificates.</li>
                                        <li>Hands-on experience with AWS Load balancing infrastructure and methods. Geographical load balancing. Understanding of CDN. Load balancing in cloud.</li>
                                        <li>Hands-on with Network layers. Routers, domain controllers, etc. Networks and subnets. IP address. VPN.DNS. Firewall. IP tables. Network access between applications (ACL). Networking in AWS.</li>
                                        <li>Detailed understanding of the basic architectural principles of building on the AWS Cloud</li>
                                        <li>Availability outside of working hours to resolve emergency issues promptly.</li>
                                        <li>Strong written and spoken communication skills.</li>
                                        </ul>
                                        </div><h3>Qualifications</h3><div class="text">
                                        <ul>
                                        <li>Bachelor’s degree in computer science, information technology, or any related field.</li>
                                        <li>AWS Certified Solutions Architect – Associate preferred.</li>
                                        </ul>
                                        </div><h3>Tools Knowledge</h3><div class="text">
                                        <p>Nagios, CloudWatch JIRA, Terraforma, Jenkins, Docker, Kubernetes, Veeam or Acronis (Cloud Backup Solution)</p>
                                        </div><h3>Development Values</h3><div class="text">
                                        <p>New technology is exciting, but at Aurum PropTech we recognize that people are the key to our successful digital transformation.&nbsp; &nbsp;We are creating a workplace where they can focus on high-value, strategic work, high-rewarding career path and be inspired to enhance our client’s digital services - with an Agile mindset.&nbsp;</p>
                                        <p>This is a fantastic opportunity to join a fluid and fast-paced organization offering an attractive working environment both in terms of work type, technologies, and projects.&nbsp; &nbsp;It’s somewhere you can thrive, grow and be challenged.&nbsp; &nbsp;The program is currently at its nascent stage and will entail owning challenging and fulfilling role using cutting-edge technology.&nbsp; &nbsp;We will be willing to invest on relevant training for the right candidate.&nbsp;</p>
                                        <p>COME JOIN US as we empower our teams with digital skills.</p>
                                                                    
                                        </div><h3>No of Positions</h3><div class="text">
                                            <p>1</p>
                                        </div>
                                        <div class="text show-modal-popup">
                                            <a href="" class="theme-btn btn-style-one apply_career_form" data-toggle="modal" data-target="#myModal1"><div class="btn-title">Apply</div></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    
    $(document).ready(function() {
        $(".accordion-box .block").hide();
        $(".accordion-box .pdm").show();

        $(".apply_career_form").click(function() {
            $(".bio-close").trigger('click');
        });

        $(".accordion").click(function(){
            var val = $(this).find(".acc-btn").text().trim();
            $("#career-form").find("#job").text(val);
            $("#career-form").find("#job").val(val);
            //$("#career-form").find(".job-select option[value='"+val+"']").removeAttr('disabled').attr('selected', 'selected');;
        });

        $(".show-modal-popup").click(function() {
            $("#career-form").find(".error").next("span").remove();
            $("#career-form").find(".error").removeClass("span");
            $("#career-form").trigger('reset');
        });

        $(".job-family").click(function() {
            var op = $(this).attr("data-id");
            $(".job-family").removeClass('active');
            $(this).addClass('active');
            $(".accordion-box .block").hide();
            $(".accordion-box ."+op).show();
        })

        $("#submit-career").click(function() {
                var error = false;
                var email_error= false;
                if($("#username").val() == "") {
                    error= true;
                    $("#username").addClass("error");
                }else{$("#username").removeClass("error"); $("#username").next("span").remove();}
                if($("#email").val() == "") {
                    error= true;
                    $("#email").addClass("error");
                }else{
                    if(IsEmail($("#email").val().trim())) {
                        email_error= true;
                        error= true;
                        $("#email").addClass("error");
                        var msg="Invalid Email";
                    }else{
                        $("#email").removeClass("error"); $("#email").next("span").remove();
                    }
                }
                if($("#phone").val() == "") {
                    error= true;
                    $("#phone").addClass("error");
                }else{$("#phone").removeClass("error"); $("#phone").next("span").remove();}
                if($("#resume").val() == "") {
                    error= true;
                    $("#resume").addClass("error");
                }else{$("#resume").removeClass("error"); $("#resume").next("span").remove();}
                if($("#job").val() == "") {
                    error= true;
                    $("#job").addClass("error");
                }else{$("#job").removeClass("error"); $("#job").next("span").remove();}

                if(error) {
                    $(".error").next("span").remove();
                    if(email_error){
                        $(".error").after("<span style='color:red'>"+msg+"</span>");
                    }else{
                        $(".error").after("<span style='color:red'>This Field is Required</span>");
                    }
                    
                    return false;
                }
                
                var data = $("#career-form").serialize();
                var formData = new FormData(document.getElementById("career-form"));
                formData.append('resume', $('#resume')[0].files[0]);
                $(".form-loading").show().css("display","flex");
                $.ajax({
                    url: "ajax-action.php",
                    method: "post",
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    data: formData,
                    success: function(res) {
                        if(res.status) {
                            Swal.fire({
                                icon: 'success',
                                text: res.msg,
                            }).then(function() {
                                location.reload();
                            });

                        }else{
                            Swal.fire({
                                icon: 'error',
                                text: res.msg,
                            })
                        }
                    },
                    error: function (jqXHR, exception) {
                        var err = jqXHR.responseText;
                        console.log(err);
                        Swal.fire({
                            icon: 'error',
                            text: 'Something went wrong, please try again!!!',
                        }) 
                        $('#contact-form').reset();
                    }
                })
                $(".form-loading").hide();
            return false;
        });

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        function IsEmail(email) {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if(!regex.test(email)) {
                return true;
            }else{
                return false;
            }
        }

        $("#phone").keypress(function(e){
            if(isNumber(e)){	
                if($("#phone").val().length < 10) {
                    return true;
                }else{
                    e.preventDefault();
                }
                
            }else{
                e.preventDefault();
            }
        })
    })
    function Validate(id) 
    {
        var val = document.getElementById(id).value;
        if(val!=''){
            if (!val.match(/^[A-Za-z][A-Za-z\s]*$/)) 
            {
                alert('Only alphabets are allowed');
                document.getElementById(id).value="";
                return false;
            }
        }
        return true;
    }

    function inpNum(e) {
        e = e || window.event;
        var charCode = (typeof e.which == "undefined") ? e.keyCode : e.which;
        var charStr = String.fromCharCode(charCode);
        if (!charStr.match(/^[0-9]+$/))
        e.preventDefault();
    }
</script>
