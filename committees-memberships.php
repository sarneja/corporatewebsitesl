<?php
include "header.php";
?>
 <!-- Inner Banner Section -->
 <section class="inner-banner alternate">
        <div class="banner-curve"></div>
		<div class="auto-container">
            <div class="inner">
                <div class="theme-icon"></div>
    			<div class="title-box">
                    <h1>Committees Memberships</h1>
                </div>
            </div>
		</div>
    </section>
    <!--End Banner Section -->

    <div class="sidebar-page-container services-page">
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                    <div class="services-content">
                        <div class="service-details">
                            <div class="image-box">
                                <u><a target="_blank" href="com-member/List-of-Committees-and-Composition.pdf">List of Committees &amp; Memberships</a></u>
                            </div>
                        </div>
                    </div>
                </div>

                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
                    <aside class="sidebar services-sidebar">
                        <!--Services Widget-->
                        <div class="sidebar-widget services-widget">
                            <div class="widget-inner">
                                <ul>
                                    <li><a href="company-profile.php">Company Profile</a></li>
                                    <li><a href="board-members.php">Board Of Director</a></li>
                                    <li><a href="fractional-ownership.php">Key Managerial Personnel</a></li>
                                    <li><a href="fractional-ownership.php">Cooperate Structure</a></li>
                                    <li class="active"><a href="committees-memberships.php">Committees & MemberShips</a></li>
                                </ul>
                            </div>
                        </div>
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
<?php
include "footer.php";
?>