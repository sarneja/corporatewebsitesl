<?php
include "header.php";
?>
 <!-- Inner Banner Section -->
 <section class="inner-banner alternate">
        <div class="banner-curve"></div>
		<div class="auto-container">
            <div class="inner">
                <div class="theme-icon"></div>
    			<div class="title-box">
                    <h1>Company Profile</h1>
                </div>
            </div>
		</div>
    </section>
    <!--End Banner Section -->

    <!--About Section-->
    <section class="about-section-three">
        <div class="auto-container">
            <!--
            <div class="sec-title centered">
                <div class="upper-text">Aurum Proptech</div>
                <h2><strong>Your next Preferred IT Partner</strong></h2>
            </div>-->

            <div class="upper-row">
            	<div class="row clearfix">
                    <!--Text Column-->
                    <div class="text-column col-lg-12 col-md-12 col-sm-12">
                        <div class="inner">
                            <div class="text">
                                <i><h3 style="color: #dcab1c;
                                                font-size: 23px; 
                                                margin-top:-50px; 
                                                margin-bottom:50px; 
                                                font-family: fangsong;
                                                text-align:center;
                                                text-transform: capitalize;">
                                                <i class="fa fa-quote-left" style="font-size:10px; position:absolute; top:-5px; left:25px;"></i>One stop tech driven Real Estate platform offering Software as a Service 'SaaS' and Real Estate as a Service 'RaaS'<i class="fa fa-quote-right" style="font-size:10px; position:absolute; top:-5px; right:20px;"></i></h3></i>
                                <p>Aurum PropTech Ltd is Software Technology and Services company listed on the National Stock Exchange (NSE) as “AURUM” and Bombay Stock Exchange as "BSE: 539289" with its headquarters at Navi Mumbai, India.  Aurum PropTech Ltd was founded in 2015 as an Insurance Tech company and was formerly known as Majesco Limited. In March 2021, Aurum Platz IT Pvt Ltd which now owns the promoter shareholding pivoted it to a PropTech – Property Technology company.</p>
                            </div>

                            <h3>Who We Are</h3>
                            <div class="text">
                                <p>Aurum PropTech is a new-age Technology company revolutionizing Real Estate with an integrated PropTech ecosystem that brings real estate, people, and technology together. The PropTech ecosystem shall boost data driven real estate investment and financing, increase enterprise efficiency, enhance customer experience, and promote connected living. We are a diverse and young team with exceptional talent, visionary leadership with customer centricity at its heart.</p>
                            </div>

                            <h3>What We Do</h3>
                            <div class="text">
                                <p>We build and operate PropTech products, services and platforms focused on the B2B, B2C, B2B2C and D2C Real Estate Value Chains. Our combined real estate domain experience and tech expertise elevate experiences for creators, consumers, and capital allocators of Real Estate. Our full stack range of integrated ecosystem includes products, services, and platforms with cutting edge technologies like Artificial Intelligence, Machine Learning, Block Chain, Augmented Realty and Virtual Reality.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php
include "footer.php";
?>