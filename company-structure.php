<?php
include "header.php";
?>

<style>
    .tree ul {
    padding-top: 20px; position: relative;
	
	transition: all 0.5s;
	-webkit-transition: all 0.5s;
	-moz-transition: all 0.5s;
    }

    .tree li {
        float: left; text-align: center;
        list-style-type: none;
        position: relative;
        padding: 20px 5px 0 5px;
        
        transition: all 0.5s;
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
    }

    /*We will use ::before and ::after to draw the connectors*/

    .tree li::before, .tree li::after{
        content: '';
        position: absolute; top: 0; right: 50%;
        border-top: 1px solid #a1a9b4;
        width: 50%; height: 20px;
    }
    .tree li::after{
        right: auto; left: 50%;
        border-left: 1px solid #a1a9b4;
    }

    /*We need to remove left-right connectors from elements without 
    any siblings*/
    .tree li:only-child::after, .tree li:only-child::before {
        display: none;
    }

    /*Remove space from the top of single children*/
    .tree li:only-child{ padding-top: 0;}

    /*Remove left connector from first child and 
    right connector from last child*/
    .tree li:first-child::before, .tree li:last-child::after{
        border: 0 none;
    }
    /*Adding back the vertical connector to the last nodes*/
    .tree li:last-child::before{
        border-right: 1px solid #a1a9b4;
        border-radius: 0 5px 0 0;
        -webkit-border-radius: 0 5px 0 0;
        -moz-border-radius: 0 5px 0 0;
    }
    .tree li:first-child::after{
        border-radius: 5px 0 0 0;
        -webkit-border-radius: 5px 0 0 0;
        -moz-border-radius: 5px 0 0 0;
    }

    /*Time to add downward connectors from parents*/
    .tree ul ul::before{
        content: '';
        position: absolute; top: 0; left: 50%;
        border-left: 1px solid #a1a9b4;
        width: 0; height: 20px;
    }

    .tree li a{
        border: 1px solid #ccc;
        padding: 15px 12px;
        text-decoration: none;
        color: #666;
        font-family: arial, verdana, tahoma;
        font-size: 17px;
        display: inline-block;
        
        border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        
        transition: all 0.5s;
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;

        height: 188px;

        background: #d2cfe7; color: #000; border: 1px solid #94a0b4;
    }

    /*Time for some hover effects*/
    /*We will apply the hover effect the the lineage of the element also*/
    .tree li a:hover, .tree li a:hover+ul li a {
        /*background: #c8e4f8; color: #000; border: 1px solid #94a0b4;*/
    }
    /*Connector styles on hover*/
    .tree li a:hover+ul li::after, 
    .tree li a:hover+ul li::before, 
    .tree li a:hover+ul::before, 
    .tree li a:hover+ul ul::before{
        border-color:  #94a0b4;
    }
    .tree ul>li>ul>li {
        width: 202px;
    }

    .tree ul>li>ul>li > a{
        width: 100%;
    }
</style>
 <!-- Inner Banner Section -->
 <section class="inner-banner alternate">
        <div class="banner-curve"></div>
		<div class="auto-container">
            <div class="inner">
                <div class="theme-icon"></div>
    			<div class="title-box">
                    <h1>Corporate Structure</h1>
                </div>
            </div>
		</div>
    </section>
    <!--End Banner Section -->

    <!--About Section-->
    <section class="about-section-three">
        <div class="auto-container">
            <!--
            <div class="sec-title centered">
                <div class="upper-text">Aurum Proptech</div>
                <h2><strong>Your next Preferred IT Partner</strong></h2>
            </div>-->

            <div class="upper-row">
            	<div class="row clearfix">
                    <!--Text Column-->
                    <div class="text-column col-lg-12 col-md-12 col-sm-12">
                        <div class="inner" style="padding-right:0px;">
                        <div class="upper-row">
                <div class="tree">
                    <ul>
                        <li>
                            <a href="javascript:void(0);" style="cursor: alias;height: 66px !important;" style="">Aurum PropTech Ltd:</a>
                            <ul>
                                <li>
                                    <a href="javascript:void(0);" style="cursor: alias;">Aurum Softwares and Solutions <br>Private Limited <br>(wholly owned subsidiary) </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" style="cursor: alias;">Aurum RealTech Services<br> Private Limited<br> (wholly owned subsidiary)</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" style="cursor: alias;">K2V2 Technologies Pvt Ltd<br> (strategic 51% stake)</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" style="cursor: alias;">Monk Tech Labs Pte. Ltd<br> (strategic 51% stake)</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" style="cursor: alias;">Integrow Asset Management Pvt Ltd<br> (strategic 49% stake)</a>
                                </li>

                                <li>
                                    <a href="javascript:void(0);" style="cursor: alias;">Grexter Housing Solutions Pvt Ltd<br> (strategic 53% stake)</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php
include "footer.php";
?>