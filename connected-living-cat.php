<?php
include "header.php";
?>
<!-- Inner Banner Section -->
<section class="inner-banner">
        <div class="banner-curve"></div>
		<div class="auto-container">
            <div class="inner">
                <div class="theme-icon"></div>
    			<div class="title-box">
                    <h1>Connected Living</h1>
                    
                </div>
            </div>
		</div>
    </section>
    <!--End Banner Section -->

	<!--News Section-->
    <section class="news-section blog-grid">
        <div class="auto-container">
            <div class="row clearfix">
                <!--News Block-->
                <div class="news-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <a href="amenities-management.php"><img src="images/amenity.jpg" alt="" title=""></a>
                        </div>
                        <div class="lower-box">
                            <!--<div class="category">IT Projects</div>-->
                            <h3><a href="amenities-management.php">Amenities Management</a></h3>
                            <!--
                            <div class="meta-info">
                                <ul class="clearfix">
                                    <li><a href="#">By Admin</a></li>
                                    <li><a href="#">24 June 2019</a></li>
                                </ul>
                            </div>-->
                            <div class="more-link"><a href="amenities-management.php"><span class="fa fa-arrow-right"></span></a></div>
                        </div>
                    </div>
                </div>
                <!--News Block-->
                <div class="news-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <a href="rental-management.php"><img src="images/rental.jpg" alt="" title=""></a>
                        </div>
                        <div class="lower-box">
                            <!--<div class="category">IT Projects</div>-->
                            <h3><a href="rental-management.php">Rental Management</a></h3>
                            <!--
                            <div class="meta-info">
                                <ul class="clearfix">
                                    <li><a href="#">By Admin</a></li>
                                    <li><a href="#">24 June 2019</a></li>
                                </ul>
                            </div>-->
                            <div class="more-link"><a href="rental-management.php"><span class="fa fa-arrow-right"></span></a></div>
                        </div>
                    </div>
                </div>
                <!--News Block-->
                <div class="news-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <a href="co-living.php"><img src="images/coliving.jpg" alt="" title=""></a>
                        </div>
                        <div class="lower-box">
                            <!--<div class="category">IT Projects</div>-->
                            <h3><a href="co-living.php">Co-Living</a></h3>
                            <!--
                            <div class="meta-info">
                                <ul class="clearfix">
                                    <li><a href="#">By Admin</a></li>
                                    <li><a href="#">24 June 2019</a></li>
                                </ul>
                            </div>-->
                            <div class="more-link"><a href="co-living.php"><span class="fa fa-arrow-right"></span></a></div>
                        </div>
                    </div>
                </div>
                <!--News Block-->
                <div class="news-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <a href="co-working.php"><img src="images/coliving (1).jpg" alt="" title=""></a>
                        </div>
                        <div class="lower-box">
                            <!--<div class="category">IT Projects</div>-->
                            <h3><a href="co-working.php">Co-Working</a></h3>
                            <!--
                            <div class="meta-info">
                                <ul class="clearfix">
                                    <li><a href="#">By Admin</a></li>
                                    <li><a href="#">24 June 2019</a></li>
                                </ul>
                            </div>-->
                            <div class="more-link"><a href="co-working.php"><span class="fa fa-arrow-right"></span></a></div>
                        </div>
                    </div>
                </div>
                <!--News Block-->
                <div class="news-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <a href="smart-building-workplace-tech.php"><img src="images/smart.jpg" alt="" title=""></a>
                        </div>
                        <div class="lower-box">
                            <!--<div class="category">IT Projects</div>-->
                            <h3><a href="smart-building-workplace-tech.php">Smart Building And Workplace Tech</a></h3>
                            <!--
                            <div class="meta-info">
                                <ul class="clearfix">
                                    <li><a href="#">By Admin</a></li>
                                    <li><a href="#">24 June 2019</a></li>
                                </ul>
                            </div>-->
                            <div class="more-link"><a href="smart-building-workplace-tech.php"><span class="fa fa-arrow-right"></span></a></div>
                        </div>
                    </div>
                </div>
                <div class="news-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <a href="property-management.php"><img src="images/propertymanagement.jpg" alt="" title=""></a>
                        </div>
                        <div class="lower-box">
                            <!--<div class="category">IT Projects</div>-->
                            <h3><a href="property-management.php">Property Management</a></h3>
                            <!--
                            <div class="meta-info">
                                <ul class="clearfix">
                                    <li><a href="#">By Admin</a></li>
                                    <li><a href="#">24 June 2019</a></li>
                                </ul>
                            </div>-->
                            <div class="more-link"><a href="property-management.php"><span class="fa fa-arrow-right"></span></a></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--
            <div class="load-more link-box">
                <a href="blog-fullwidth.html" class="theme-btn btn-style-two"><div class="btn-title">Load More News</div></a>
            </div>-->

        </div>
    </section>

<?php
include "footer.php";
?>