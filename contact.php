<?php
include "header.php";
?>
 <!-- Inner Banner Section -->
 <section class="inner-banner alternate">
        <div class="image-layer" style="background-image: url(images/background/banner-bg-2.jpg);"></div>
		<div class="auto-container">
            <div class="inner">
    			<div class="title-box">
                    <h1>Contact</h1>
                    
                </div>
            </div>
		</div>
    </section>
    <!--End Banner Section -->

	<!--Contact Section-->
    <section class="contact-section-two">
        <div class="auto-container">
            <div class="upper-row">
                <div class="row clearfix">
                    <!--Text Column-->
                    <div class="text-column col-lg-6 col-md-12 col-sm-12">
                        <div class="inner">
                            <div class="sec-title">
                                <div class="upper-text">Send us a Message</div>
                                <h2>Do You Have Any Questions? We will Be Happy To Assist!</h2>
                               
                            </div>

                            <div class="social-links">
                                <ul class="clearfix">
                                    <li><a href="https://www.facebook.com/AurumPropTech" target="_blank"><span class="fab fa-facebook-square"></span></a></li>
                                    <li><a href="https://www.linkedin.com/uas/login?session_redirect=https%3A%2F%2Fwww.linkedin.com%2Fcompany%2F71413509%2Fadmin" target="_blank"><span class="fab fa-linkedin"></span></a></li>
                                    <li><a href="https://www.instagram.com/aurumproptech/" target="_blank"><span class="fab fa-instagram"></span></a></li>
                                                                 </ul>
                            </div>
                        </div>
                    </div>
                    <!--Form Column-->
<script>
function Validate(id) 
{
    var val = document.getElementById(id).value;
     
if(val!=''){
   if (!val.match(/^[A-Za-z][A-Za-z\s]*$/)) 
    {
        alert('Only alphabets are allowed');
	document.getElementById(id).value="";
        return false;
    }
}
    
    return true;
}

function ValidateEmail(mail) 
{
var re = /\S+@\S+\.\S+/;
 if (re.test(mail))
  {
    return (true)
  }else{
    alert("You have entered an invalid email address!");
	document.getElementById('email').value="";

    return (false)
 }
}
  function inpNum(e) {
  e = e || window.event;
  var charCode = (typeof e.which == "undefined") ? e.keyCode : e.which;
  var charStr = String.fromCharCode(charCode);
  if (!charStr.match(/^[0-9]+$/))
    e.preventDefault();
}
</script>

                    <div class="form-column col-lg-6 col-md-12 col-sm-12">
                        <div class="inner">
                            <!--Form Box-->
                            <div class="form-box">
                                <div class="default-form contact-form">
                                     <form method="post" action="" id="contact-form">
                                    <p style="color:#000;"><b>Note :</b> <i><span class="tx-danger">*</span> Fields are Mandatory</i></p>
                                        <div class="row clearfix">                                    
                                            <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                                <input type="text" name="username" id="username" placeholder="Your Name*" required="" onkeyup="Validate('username')" value="">
                                            </div>
                                            
                                            <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                                <input type="email" name="email" id="email" placeholder="Your Email*" required="" onchange="ValidateEmail(this.value)" value="">
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                                <input type="text" name="phone" id="phone" placeholder="Your Phone Number*" required="" value="" onkeypress="inpNum(event)">
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                                <input type="text" name="subject" id="subject" placeholder="Subject*" required="" value="">
                                            </div>

                                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                                <textarea name="message" id="message" placeholder="Message*" required=""></textarea>
                                            </div>
                    
                                            <div class="form-group col-md-12 col-sm-12">
                                                <button type="button" class="theme-btn btn-style-one contact-submit"><span class="btn-title">Make a Request</span></button>
                                            </div>
                                        </div>
                                    </form>                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="lower-row">
                <div class="row clearfix">
                    <!--Info Block-->
                    <div class="contact-info-block col-lg-6 col-md-6 col-sm-12">
                        <div class="inner wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <div class="content-box">
                                <div class="title-box">
                                    <h4>Shareholder Contact</h4>
                                    <div class="sub-text">AURUM PROPTECH LIMITED (FORMERLY KNOWN AS MAJESCO LIMITED)</div>
                                </div>
                                <div class="text-content">
                                    <div class="info">
                                        <ul>
                                            <li>
Aurum Building Q1, Gen-4/1, TTC Industrial Area, Thane Belapur Road, Ghansoli, Navi Mumbai - 400710 MH IN
</li>
                                            <li><b>Call: </b> +91 22 3000 1700 <b>/</b> 2778 1271</li>
                                                                                        <li><b>Email: </b><a href="mailto:investors.grievances@aurumproptech.in ">investors.grievances@aurumproptech.in </a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <!--Map Box-->
                     <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4482.944366988504!2d73.0078973281419!3d19.12116987661406!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7c0c429a42e41%3A0x50f21f0b45c5e285!2sAurum%20PropTech%20Limited!5e0!3m2!1sen!2sin!4v1645608900361!5m2!1sen!2sin" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>                        </div>
                    </div>
                    <!--Info Block-->
                    <div class="contact-info-block col-lg-6 col-md-6 col-sm-12">
                        <div class="inner wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                            <div class="content-box">
                                <div class="title-box">
                                    <h4>Transfer Agent</h4>
                                    <div class="sub-text">KFin Technologies Pvt. Limited</div>
                                </div>
                                <div class="text-content">
                                    <div class="info">
                                        <ul>
                                            <li>Aurum PropTech Limited (formerly known as Majesco Limited) Karvy Selenium Tower B, Plot 31-32, Financial District, Nanakramguda, Serilingampally Mandal, Hyderabad - 500 032, Telangana, India</li>
                                            <li><b>Toll free number: </b>- 1- 800-309-4001</li> 
                                            <li><b>E-mail: </b><a href="mailto:einward.ris@kfintech.com">einward.ris@kfintech.com</a></li>
                                            <li><b>Web: </b><a href="https://ris.kfintech.com/"> https://ris.kfintech.com/</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <!--Map Box-->
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3806.8005706503623!2d78.33097261479689!3d17.42135578805826!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb937dde7fafb3%3A0x3ab58aee0b12590f!2sKFintech%20Pvt%20Ltd!5e0!3m2!1sen!2sin!4v1644323951534!5m2!1sen!2sin" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>                         
<?php
include "footer.php";
?>
