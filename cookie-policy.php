<?php
include "header.php";
?>
 <!-- Inner Banner Section -->
 <section class="inner-banner alternate">
        <div class="banner-curve"></div>
		<div class="auto-container">
            <div class="inner">
                <div class="theme-icon"></div>
    			<div class="title-box">
                    <h1>Cookie Policy</h1>
                </div>
            </div>
		</div>
    </section>
    <!--End Banner Section -->

    <div class="sidebar-page-container services-page">
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Content Side-->

                <div class="content-side col-lg-12 col-md-12 col-sm-12">
                    <div class="service-details product-details">

<p>To make this site work properly, we sometimes place small data files called cookies on your device. A cookie is a small text file that a website saves on your computer or mobile device when you visit the site. It enables the website to remember your actions and preferences (such as login, shopping cart, or other preferences) over a period of time, so you do not have to keep re-entering them whenever you come back to the site or browse from one page to another.</p>
<p>We have put our cookies into the following categories to make it easier for you to understand why we need them:</p>

<ol style="margin-left:20px; list-style:auto;">
<li style="margin-left:20px; list-style:auto;"><b>Strictly necessary:</b> These are used to help make our website work efficiently, provide security features on our website, and provide services you've asked for, like shopping carts and billing.</li>
<li style="margin-left:20px; list-style:auto;"><b>Performance:</b> These are used to analyze the way our website works and how we can improve it.</li>
<li style="margin-left:20px; list-style:auto;"><b>Functionality:</b> These help to enhance your experience by remembering choices you've made concerning the features of the website (e.g. username, language, region you are in).</li>
<li style="margin-left:20px; list-style:auto;"><b>Advertising/Targeting.</b> These cookies are used to deliver advertisements more relevant to you and your interests. They remember that you have visited a website and this information is shared with other organizations such as advertisers.</li>
</ol>

                 </div></div>

                               
            </div>
        </div>
    </div>
<?php
include "footer.php";
?>