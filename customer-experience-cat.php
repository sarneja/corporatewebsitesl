<?php
include "header.php";
?>
<!-- Inner Banner Section -->
<section class="inner-banner">
        <div class="banner-curve"></div>
		<div class="auto-container">
            <div class="inner">
                <div class="theme-icon"></div>
    			<div class="title-box">
                    <h1>Customer Experience</h1>
                    
                </div>
            </div>
		</div>
    </section>
    <!--End Banner Section -->

	<!--News Section-->
    <section class="news-section blog-grid">
        <div class="auto-container">
            <div class="row clearfix">
                <!--News Block-->
                <div class="news-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <a href="virtual-reality.php"><img src="images/virtual.jpg" alt="" title=""></a>
                        </div>
                        <div class="lower-box">
                            <!--<div class="category">IT Projects</div>-->
                            <h3><a href="virtual-reality.php">Virtual Reality</a></h3>
                            <!--
                            <div class="meta-info">
                                <ul class="clearfix">
                                    <li><a href="#">By Admin</a></li>
                                    <li><a href="#">24 June 2019</a></li>
                                </ul>
                            </div>-->
                            <div class="more-link"><a href="virtual-reality.php"><span class="fa fa-arrow-right"></span></a></div>
                        </div>
                    </div>
                </div>
                <!--News Block-->
                <div class="news-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <a href="home-loan.php"><img src="images/loan.jpg" alt="" title=""></a>
                        </div>
                        <div class="lower-box">
                            <!--<div class="category">IT Projects</div>-->
                            <h3><a href="home-loan.php">Home Loans</a></h3>
                            <!--
                            <div class="meta-info">
                                <ul class="clearfix">
                                    <li><a href="#">By Admin</a></li>
                                    <li><a href="#">24 June 2019</a></li>
                                </ul>
                            </div>-->
                            <div class="more-link"><a href="home-loan.php"><span class="fa fa-arrow-right"></span></a></div>
                        </div>
                    </div>
                </div>
                <!--News Block-->
                <div class="news-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <a href="digital-escrow.php"><img src="images/digitales.jpg" alt="" title=""></a>
                        </div>
                        <div class="lower-box">
                            <!--<div class="category">IT Projects</div>-->
                            <h3><a href="digital-escrow.php">Digital Escrow</a></h3>
                            <!--
                            <div class="meta-info">
                                <ul class="clearfix">
                                    <li><a href="#">By Admin</a></li>
                                    <li><a href="#">24 June 2019</a></li>
                                </ul>
                            </div>-->
                            <div class="more-link"><a href="digital-escrow.php"><span class="fa fa-arrow-right"></span></a></div>
                        </div>
                    </div>
                </div>
                <!--News Block-->
                <div class="news-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <a href="interior-design.php"><img src="images/interior.jpg" alt="" title=""></a>
                        </div>
                        <div class="lower-box">
                            <!--<div class="category">IT Projects</div>-->
                            <h3><a href="interior-design.php">Interior Design</a></h3>
                            <!--
                            <div class="meta-info">
                                <ul class="clearfix">
                                    <li><a href="#">By Admin</a></li>
                                    <li><a href="#">24 June 2019</a></li>
                                </ul>
                            </div>-->
                            <div class="more-link"><a href="interior-design.php"><span class="fa fa-arrow-right"></span></a></div>
                        </div>
                    </div>
                </div>
                <!--News Block-->
                <div class="news-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <a href="home-furnishing.php"><img src="images/homell.jpg" alt="" title=""></a>
                        </div>
                        <div class="lower-box">
                            <!--<div class="category">IT Projects</div>-->
                            <h3><a href="home-furnishing.php">Home Furnishing</a></h3>
                            <!--
                            <div class="meta-info">
                                <ul class="clearfix">
                                    <li><a href="#">By Admin</a></li>
                                    <li><a href="#">24 June 2019</a></li>
                                </ul>
                            </div>-->
                            <div class="more-link"><a href="home-furnishing.php"><span class="fa fa-arrow-right"></span></a></div>
                        </div>
                    </div>
                </div>
                <div class="news-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <a href="lifestyle.php"><img src="images/lifestyle.jpg" alt="" title=""></a>
                        </div>
                        <div class="lower-box">
                            <!--<div class="category">IT Projects</div>-->
                            <h3><a href="lifestyle.php">Lifestyle</a></h3>
                            <!--
                            <div class="meta-info">
                                <ul class="clearfix">
                                    <li><a href="#">By Admin</a></li>
                                    <li><a href="#">24 June 2019</a></li>
                                </ul>
                            </div>-->
                            <div class="more-link"><a href="lifestyle.php"><span class="fa fa-arrow-right"></span></a></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--
            <div class="load-more link-box">
                <a href="blog-fullwidth.html" class="theme-btn btn-style-two"><div class="btn-title">Load More News</div></a>
            </div>-->

        </div>
    </section>

<?php
include "footer.php";
?>