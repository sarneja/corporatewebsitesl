<?php
include "header.php";
?>
<style>
  

</style>
 <!-- Inner Banner Section -->
 <section class="inner-banner alternate">
        <div class="image-layer" style="background-image: url(images/background/banner-bg-1.jpg);"></div>
        <div class="auto-container">
            <div class="inner">
                <div class="title-box">
                    <h1>Data Science And Analytics</h1>
                </div>
            </div>
        </div>
    </section>
    <!--End Banner Section -->

    <div class="sidebar-page-container services-page">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                        <div class="service-details product-details">
                            <h3 class=""><strong> Data Science And Analytics</strong></h3>
                            <p>Our Data Science department has developed to analytical tools namely Aurum Isight and Aurum SEA. Aurum Isight is built to use various type of data and market trends into machine learning algorithms to give users an insight into the valuation aspect of a particular property. Aurum SEA - Sound Emotion Analyzer, on the other hand, uses audio recordings of Real Estate Transaction discussions in multiple languages to understand and predict a buyer's sentiment about the transaction.</p>
                            <div class="row">
                                <!-- DEMO 1 Item-->
                                <div class="col-lg-12 mb-12 mb-lg-12 img-div">
                                    <div class="hover hover-1 text-white rounded"><img src="images/modern-graphic-interface-shows-massive-information-business-sale-report-profit-chart-stock-market-trends-analysis-screen-monitor_31965-11843.jpg" alt="">
                                    <div class="hover-overlay"></div>
                                    <div class="hover-1-content px-5 py-4">
                                        <h3 class="hover-1-title text-uppercase font-weight-bold mb-0">AURUM ISIGHT</h3>
                                        <p class="hover-1-description font-weight-light mb-0">AI-based home valuation tool that estimates a home’s market value. It incorporates publicly available, MLS, and user-submitted data into machine learning algorithms also taking into account facts like GIS data and market trends. The tool can be used by retail buyers to make informed property decisions, get insight on the factors affecting the overall valuation, and enables real estate developers plan their supply and development.</p>
                                    </div>
                                    </div>
                                </div>
                                <!-- DEMO 1 Item-->
                                <div class="col-lg-12 mb-12 mb-lg-12 img-div">
                                    <div class="hover hover-1 text-white rounded"><img src="images/data-analysis-business-finance-concept_31965-4141.jpg" alt="">
                                    <div class="hover-overlay"></div>
                                    <div class="hover-1-content px-5 py-4">
                                        <h3 class="hover-1-title text-uppercase font-weight-bold mb-0"> <span class="font-weight-light">Aurum SEA</h3>
                                        <p class="hover-1-description font-weight-light mb-0">Sound Emotion Analyzer (SEA) analyses audio recordings of Real Estate transaction discussions and predicts buyers’ sentiment by detecting emotional sentiment, targeting specific keywords, predicting purchasers’ sound modulation, and the likelihood of the buyer making a purchase decision using state of the art Artificial Intelligence models. It also analyses the seller's sales pitch and gives him a rating based on the customer engagement obtained during the process. The framework uses various Natural Language Processing components which makes it capable to analyze any language including vernacular languages.</p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="gallery">
                          <div class="prev-next-button next"><a href="digital-lending"></a></div>
                        </div> --> 
                </div>

                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
                    <aside class="sidebar services-sidebar">
                        <!--Services Widget-->
                        <div class="sidebar-widget services-widget">
                            <div class="widget-inner">
                                <ul>
                                    <li class="active"><a href="data-science-and-analytics">Data Science And Analytics</a></li>
                                    <li><a href="digital-lending">Digital Lending</a></li>
                                    <li><a href="fractional-ownership">Fractional Ownership</a></li>
                                </ul>
                            </div>
                        </div>
                        <!--Info Widget-->
                        <div class="sidebar-widget info-widget">
                            <div class="widget-inner">
                                <div class="image"><img src="images/resource/side-contact.jpg" alt=""></div>
                                <div class="lower">
                                    <div class="subtitle">Got any Questions? <br>Call us Today!</div>
                                    <div class="icon-box"><span class="flaticon-telephone"></span></div>
                                    <div class="phone"><a href="tel:+91 22 3000 1700 / 2778 1271">+91 22 3000 1700 / 2778 1271</a></div>
                                    <div class="email"><a href="mailto:corporate@aurumproptech.in">corporate@aurumproptech.in</a></div>
                                </div>
                            </div>
                        </div>
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
<?php
include "footer.php";
?>