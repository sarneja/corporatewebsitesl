    <!-- Main Footer -->
    <footer class="main-footer">
        <div class="top-pattern-layer-dark"></div>
        
        <!--Call To Action-->
       <!-- <section class="call-to-action">
            <div class="map-pattern-layer"></div>

            <div class="auto-container">
                <div class="row clearfix">
                    <div class="title-column col-xl-5 col-lg-12 col-md-12 col-sm-12">
                        <div class="inner">
                            <h2>NEWSLETTER</h2>
                        </div>
                    </div>
                    <div class="links-column col-xl-7 col-lg-12 col-md-12 col-sm-12">
                        <div class="inner">
                            <div class="newsletter-form">
                                <form method="post" action="">
                                    <div class="form-group clearfix">
                                        <input type="email" name="email" value="" placeholder="Email address" required>
                                        <button type="submit" class="theme-btn newsletter-btn"><span class="icon fa fa-paper-plane"></span></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->       
        <!--Widgets Section-->
        <div class="widgets-section">
            <div class="auto-container">
                <div class="row clearfix">
                    
                    <!--Column-->
                    <div class="column col-xl-4 col-lg-12 col-md-12 col-sm-12" style="box-shadow: #d2cfe7 -62rem 0px 0px inset;
    ;margin-left:-10px;padding:6px 40px;">
                        <div class="footer-widget about-widget">
                            <div class="logo">
                               <a href="#"><img src="images/Horizontal_AurumPropTech (1).png" alt=""></a>                            </div>
                                                       <div class="info">
                                <ul>
                                    <li style="list-style:none;">MNDC, MBP-P-136, Mahape, Navi Mumbai - 400 710 MH India</li>
<li style="list-style:none;"><strong>Call us :</strong>  <a href="tel:(+1)-500.369.2580">+91 22 3000 1700 / 2778 1271</a></li>
                                    <li style="list-style:none;"><strong>Email : </strong><a href="mailto:corporate@aurumproptech.in ">corporate@aurumproptech.in </a></li>
                                                                 </ul>
                            </div>
 <div class="info">
<h4 style="padding-top: 20px;color:#22477b;font-weight: bold;text-transform: uppercase;">Subscribe to our Newsletter</h4>
                        <div class="inner">
                            <div class="newsletter-form">
                                <form method="post" action="">
                                    <div class="form-group clearfix">
                                        <input type="email" name="email" value="" placeholder="Email address" required>
                                        <button type="submit" class="theme-btn newsletter-btn"><span class="icon fa fa-paper-plane"></span></button>
                                    </div>
                                </form>
                            </div>
                        </div>

<div class="title text" style="margin-top:20px;margin-bottom:0;">Get the latest news &amp; updates</div>
<ul class="social-links" style="text-align:left;margin-top:10px;">
                                            <li><a href="https://www.facebook.com/AurumPropTech" target="_blank"><span class="fab fa-facebook-square fa-lg" style="color:#224779;"></span></a></li>
                                            <li><a href="https://www.instagram.com/aurumproptech/" target="_blank"><span class="fab fa-instagram fa-lg"  style="color:#224779;"></span></a></li>
                                            <li><a href="https://www.linkedin.com/uas/login?session_redirect=https%3A%2F%2Fwww.linkedin.com%2Fcompany%2F71413509%2Fadmin" target="_blank"><span class="fab fa-linkedin fa-lg"  style="color:#224779;"></span></a></li>
                                        </ul>
                    </div>

                        </div>
                    </div>
                    
                    <!--Column-->
                    <div class="column col-xl-8 col-lg-12 col-md-12 col-sm-12">
                        <div class="footer-widget links-widget">
                            <div class="widget-content">
                                <div class="row clearfix" style="padding-top:36px;">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="row clearfix">
                                            <div class="column col-lg-12 col-md-12 col-sm-12">
<p style="text-align:center;font-size: 33px;font-weight: bold;color:#dcab1c;">Future of PropTech Starts Here</p>
</div>
                                            <div class="column col-lg-4 col-md-4 col-sm-12">
                                                <div class="widget-title">
                                                    <h4 style="color:#fff;">About Us</h4>
                                                </div>
                                                <ul class="links" style="padding-left: 20px;">
 <li style="list-style:circle;"><a href="">Aurum Listing</a></li>
 <li style="list-style:circle;"><a href="">Aurum Ventures</a></li>

                                                  <!--  <li><a href="company-profile.php">Company Profile</a></li>
                                                    <li><a href="board-members.php">Board Of Directors</a></li>
                                                    <li><a href="key-managerial-personnel.php">Key Managerial Personnel</a></li>
                                                    <li><a href="">Corporate Structure</a></li>
                                                    <li><a target="_blank" href="com-member/List-of-Committees-and-Composition.pdf">Committees & Memberships</a></li>-->
                                                    <!--<li><a href="">Management Team</a></li>-->
                                                </ul>
                                            </div>
 <div class="column col-lg-4 col-md-4 col-sm-12">
                                                <div class="widget-title">
                                                    <h4 style="color:#fff;">Our Solutions</h4>
                                                </div>
                                                <ul class="links" style="padding-left: 20px;">
                                                <li style="list-style:circle;"><a target="_blank" href="https://www.sell.do/">Sell.do </a></li>
                                                <li style="list-style:circle;"><a target="_blank" href="https://kylas.io/">Kylas </a></li>												
												<li style="list-style:circle;"><a target="_blank" href="https://www.aurumproptech.com/CREX/">CREX</a></li>

												<li style="list-style:circle;"><a target="_blank" href="https://thehousemonk.com/">TheHouseMonk</a></li>
												<li style="list-style:circle;"><a target="_blank" href="http://www.integrowamc.com/">Integrow Asset Management</a></li>
                                                <li style="list-style:circle;"><a target="_blank" href="https://grexter.in/">Grexter</a></li>
                                                </ul>
                                            </div>
                                            <div class="column col-lg-4 col-md-4 col-sm-12">
                                                <div class="widget-title">
                                                    <h4 style="color:#fff;">Products & Services</h4>
                                                </div>
                                                <ul class="links" style="padding-left: 20px;">
                                                  <li style="list-style:circle;"><a href="">Invest & Finance</a></li>
                                                <li style="list-style:circle;"><a href="">Enterprise Efficiency </a></li>
												  <li style="list-style:circle;"><a href="">Customer Experience</a></li>
												    <li style="list-style:circle;"><a href="">Connected Living</a></li>
                                                </ul>

                                                                                            </div>
                                        </div>
                                    </div>
                                                                   </div>
                            </div>  
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <!-- Footer Bottom -->
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="inner">

<div class="copyright" style="text-align:center;color:#224779;padding-left:428px;">&copy; 2022 <strong style="color:#224779;">Aurum Proptech</strong>. All rights reserved. 
<a href="#" style="color:#224779;">Privacy Policy</a> | <a href="#" style="color:#224779;">Terms & Conditions</a></div>


                    
                </div>
            </div>
        </div>
        
    </footer>

</div>
<!--End pagewrapper-->

<script src="js/jquery.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/owl.js"></script>
<script src="js/scrollbar.js"></script>
<script src="js/validate.js"></script>
<script src="js/appear.js"></script>
<script src="js/wow.js"></script>
<script src="js/custom-script.js"></script>

</body>