<!-- Main Footer -->
    <link rel="preload" as="style" href="https://fonts.googleapis.com/css?family=Rubik:400,500%7CPoppins:600,400,500,700&amp;display=swap&amp;ver=1629726924" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Rubik:400,500%7CPoppins:600,400,500,700&amp;display=swap&amp;ver=1629726924" media="print" onload="this.media='all'">

<noscript>

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Rubik:400,500%7CPoppins:600,400,500,700&display=swap&ver=1629726924" />

</noscript>

    <footer class="main-footer">
        <!--Widgets Section-->
        <div class="widgets-section top-part">
            <div class="auto-container">
                <div class="row clearfix">
                    <div class="top-div col-xl-5 col-lg-12 col-md-12 col-sm-12">
                        <div class="inner">
                            <p>Stay up-to-date on the latest industry news, digital <br>products and Innovation impacting insurance.</p>
                        </div>
                    </div>

                    <div class="top-div col-xl-7 col-lg-12 col-md-12 col-sm-12">
                        <div class="inner">
                            <div class="newsletter-form">
                                <form method="post" action="" id="newsletter-form">
                                    <div class="form-group clearfix row">
                                        <input type="text" class="col-md-7" name="news-email" id="news-email" value="" placeholder="Enter your email address" required>
                                        <button id="newsletter-submit" type="submit" class="col-md-4">Subscribe <i class="fas fa-long-arrow-alt-right"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="widgets-section mid-part">
            <div class="auto-container">
                <div class="row clearfix">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                        <div class="inner">
                            <h3>Democratizing the Real Estate</h3>
                        </div>
                    </div>

                    <!--Column-->
                    <div class="column col-xl-12 col-lg-12 col-md-12 col-sm-12">
                        <div class="footer-widget links-widget">
                            <div class="widget-content">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="row clearfix">
                                            <div class="column col-lg-3 col-md-3 col-sm-12">
                                                <div class="widget-title">
                                                    <h4>Partners Solutions</h4>
                                                </div>
                                                <ul class="links">
                                                    <li><a target="_blank" href="https://www.aurumproptech.com/">-  Aurum Premium Listing</a></li>
                                                    <li><a target="_blank" href="https://www.aurumventures.in/">-  Aurum Ventures</a></li>
                                                    <li><a target="_blank" href="https://aurumproptech.in/">-  Aurum PropTech</a></li>
                                                    <li><a target="_blank" href="https://auruminfinity.com/">-  Aurum Infinity</a></li>
                                                    <li ><a target="_blank" href="https://www.aurumproptech.com/CREX/">-  Aurum CREX</a></li>
                                                </ul>
                                            </div>

                                            <div class="column col-lg-3 col-md-3 col-sm-12">
                                                <div class="widget-title">
                                                    <h4>Our Solutions</h4>
                                                </div>
                                                <ul class="links">
                                                    <li><a target="_blank" href="https://www.sell.do/">-  Sell.do </a></li>
                                                    <li><a target="_blank" href="https://kylas.io/">-  Kylas </a></li>

                                                    <li ><a target="_blank" href="https://thehousemonk.com/">-  TheHouseMonk</a></li>
                                                    <li ><a target="_blank" href="https://www.integrowamc.com/">-  Integrow AMC</a></li>
                                                    <li ><a target="_blank" href="https://grexter.in/">-  Grexter Living</a></li>
                                                    <li ><a target="_blank" href="https://beyondwalls.com/">-  Beyondwalls</a></li>
                                                </ul>
                                            </div>

                                            <div class="column col-lg-3 col-md-3 col-sm-12">
                                                <div class="widget-title">
                                                    <h4>Latest Launch</h4>
                                                </div>
                                                <ul class="links">
                                                    <li><a href="index.php">-  Aurum Q5</a></li>
                                                </ul>
                                            </div>

                                            <div class="column col-lg-3 col-md-3 col-sm-12">
                                                <div class="widget-title">
                                                    <h4>Contact Us</h4>
                                                </div>
                                                <ul class="links" style="padding-left: 0px;">
                                                    <li style="font-size:18px;opacity:0.8">Aurum Building Q1</li>
                                                    <li style="font-size:16px;opacity:0.5;">Thane - Belapur Rd, Ghansoli, Navi Mumbai, Maharashtra 400710</li>
                                                </ul>
                                                <div class="widget-title">
                                                    <h4>Follow Us</h4>
                                                </div>
                                                <ul class="social-links">
                                                    <li><a href="https://www.facebook.com/AurumPropTech" target="_blank"><span class="fab fa-facebook-square fa-lg"></span></a></li>
                                                    <li><a href="https://www.instagram.com/aurumproptech/" target="_blank"><span class="fab fa-instagram fa-lg"></span></a></li>
                                                    <li><a href="https://www.linkedin.com/uas/login?session_redirect=https%3A%2F%2Fwww.linkedin.com%2Fcompany%2F71413509%2Fadmin" target="_blank"><span class="fab fa-linkedin fa-lg"></span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>       
                    
        
        <!-- Footer Bottom -->
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="inner">
                    <div class="copyright">
                   &copy; 2022 Aurum PropTech Limited | <a href="cookie-policy.php">Cookie Policy</a> | <a href="privacy-policy.php">Privacy Policy</a>                    </div>
                </div>
            </div>
        </div>
        
    </footer>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<a id="button"></a>

</div>

<!--End pagewrapper-->

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script> 
<script src="js/jquery.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/owl.js"></script>
<script src="js/scrollbar.js"></script>
<script src="js/validate.js"></script>
<script src="js/appear.js"></script>
<script src="js/wow.js"></script>
<script src="js/custom-script.js"></script>
<script src="js/accept-cookie.js"></script>
<script>
var btn = $('#button');

$(window).scroll(function() {
  if ($(window).scrollTop() > 300) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});

btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
});


</script>
<style>
#button {
  display: inline-block;
  background-color: #4f42a1;
  width: 50px;
  height: 50px;
  text-align: center;
  border-radius: 4px;
  position: fixed;
  bottom: 30px;
  right: 30px;
  transition: background-color .3s, 
    opacity .5s, visibility .5s;
  opacity: 0;
  visibility: hidden;
  z-index: 1000;
}
#button::after {
  content: "\f077";
  font-family: FontAwesome;
  font-weight: normal;
  font-style: normal;
  font-size: 2em;
  line-height: 50px;
  color: #fff;
}
#button:hover {
  cursor: pointer;
  background-color: #333;
}
#button:active {
  background-color: #555;
}
#button.show {
  opacity: 1;
  visibility: visible;
}


</style>
</body>