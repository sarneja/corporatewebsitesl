<?php
include "header.php";
?>
 <!-- Inner Banner Section -->
 <section class="inner-banner alternate">
        <div class="image-layer" style="background-image: url(images/background/banner-bg-1.jpg);"></div>
        <div class="auto-container">
            <div class="inner">
                <div class="title-box">
                    <h1>Fractional Ownership</h1>
                </div>
            </div>
        </div>
    </section>
    <!--End Banner Section -->

    <div class="sidebar-page-container services-page">
        <div class="auto-container">
            <div class="row clearfix">

                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                    <div class="service-details product-details">
                        <h3 class="h-text"><strong>Fractional Ownership</strong></h3>
                        <p>Our Fractional Ownership platform, Aurum Infinity, enables its users not only to list their assets to procure investments but also invest in assets by using safe, secure and transparently held asset tokens thus enabling them to co-own tokenized institutional grade assets. </p>
                        <div class="row">
                            <!-- DEMO 1 Item-->
                            <div class="col-lg-12 mb-12 mb-lg-12 img-div">
                                <div class="hover hover-1 text-white rounded"><img src="images/fractional.jpg" alt="">
                                <div class="hover-overlay"></div>
                                <div class="hover-1-content px-5 py-4">
                                    <h3 class="hover-1-title text-uppercase font-weight-bold mb-0">Aurum Infinity</h3>
                                    <p class="hover-1-description font-weight-light mb-0">Blockchain-based Fractional ownership platform that enables individuals to invest and co-own tokenized institutional grade assets. The asset tokens can be held on a public blockchain, in a safe, secure, and transparent manner. It provides a platform for real estate developers to list their assets market their projects; retail investors have access to co-ownership to the fractions of these assets.</p>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <!-- <div class="gallery">
                        <div class="prev-next-button previous"><a href="digital-lending.php"></a></div>
                    </div> -->
                </div>

                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
                    <aside class="sidebar services-sidebar">
                        <!--Services Widget-->
                        <div class="sidebar-widget services-widget">
                            <div class="widget-inner">
                                <ul>
                                    <li><a href="data-science-and-analytics">Data Science And Analytics</a></li>
                                    <li><a href="digital-lending">Digital Lending</a></li>
                                    <li class="active"><a href="fractional-ownership">Fractional Ownership</a></li>
                                </ul>
                            </div>
                        </div>
                        <!--Info Widget-->
                        <div class="sidebar-widget info-widget">
                            <div class="widget-inner">
                                <div class="image"><img src="images/resource/side-contact.jpg" alt=""></div>
                                <div class="lower">
                                    <div class="subtitle">Got any Questions? <br>Call us Today!</div>
                                    <div class="icon-box"><span class="flaticon-telephone"></span></div>
                                    <div class="phone"><a href="tel:+91 22 3000 1700 / 2778 1271">+91 22 3000 1700 / 2778 1271</a></div>
                                    <div class="email"><a href="mailto:corporate@aurumproptech.in">corporate@aurumproptech.in</a></div>
                                </div>
                            </div>
                        </div>
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
<?php
include "footer.php";
?>