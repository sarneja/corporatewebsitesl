<?php
include "header.php";
?>
 <!-- Inner Banner Section -->
 <section class="inner-banner alternate">
        <div class="image-layer" style="background-image: url(images/background/banner-bg-1.jpg);"></div>
        <div class="auto-container">
            <div class="inner">
                <div class="title-box">
                    <h1>Fulfillment Centre</h1>
                </div>
            </div>
        </div>
    </section>
    <!--End Banner Section -->

    <div class="sidebar-page-container services-page">
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Content Side-->

                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                    <div class="service-details product-details">
                        <h3 class="h-text"><strong>Fulfillment Centre</strong></h3>
                        <p>Our Aurum CREX - Customer Real Estate Experience developed specifically for developers, purchasers and occupiers offers a variety of services to fulfill all the needs and requirement around customer experience. </p>
                        <div class="row">
                            <!-- DEMO 1 Item-->
                            <div class="col-lg-12 mb-12 mb-lg-12 img-div">
                                <div class="hover hover-1 text-white rounded"><img src="images/fulfillmentcenter2.jpg" alt="">
                                <div class="hover-overlay"></div>
                                <div class="hover-1-content px-5 py-4">
                                    <h3 class="hover-1-title text-uppercase font-weight-bold mb-0">Aurum CREX</h3>
                                    <p class="hover-1-description font-weight-light mb-0">Customer Real Estate Experience (CREX) is full stack integrated fulfillment center providing technology enabled Real Estate Services to developers, purchasers, and occupiers of Real Estate. It provides a range of services from real estate broking, pre-sales, sales, post sales, direct selling agents services to banks, property funding services for retail home loan applicants, property search and listing services, investment advisory services for fractional ownership.</p>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>

                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
                    <aside class="sidebar services-sidebar">
                        <!--Services Widget-->
                        <div class="sidebar-widget services-widget">
                            <div class="widget-inner">
                                <ul>
                                    <li><a href="construction-marketplace">Construction Marketplace</a></li>
									<li><a href="real-time-document-manager">Real Time Document Manager</a></li>
									<li><a href="customer-relationship-management">Customer Relationship Management</a></li>
									<li class="active"><a href="fullfillment-center">Fulfillment Centre</a></li>
									<li><a href="transaction">Transactions </a></li>
                                </ul>
                            </div>
                        </div>
                        <!--Info Widget-->
                        <div class="sidebar-widget info-widget">
                            <div class="widget-inner">
                                <div class="image"><img src="images/resource/side-contact.jpg" alt=""></div>
                                <div class="lower">
                                    <div class="subtitle">Got any Questions? <br>Call us Today!</div>
                                    <div class="icon-box"><span class="flaticon-telephone"></span></div>
                                    <div class="phone"><a href="tel:+91 22 3000 1700 / 2778 1271">+91 22 3000 1700 / 2778 1271</a></div>
                                    <div class="email"><a href="mailto:corporate@aurumproptech.in">corporate@aurumproptech.in</a></div>
                                </div>
                            </div>
                        </div>
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
<?php
include "footer.php";
?>