<!DOCTYPE html>
<html lang="en">


<head>
<meta charset="utf-8">
<title>Aurum PropTech</title>
<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<!-- Responsive File -->
<link href="css/responsive.css" rel="stylesheet">

<link rel="shortcut icon" href="images/icons/theme-icon.png" type="image/x-icon">
<link rel="icon" href="images/icons/theme-icon.png" type="image/x-icon">

<!-- Responsive Settings -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->

<script async src=https://www.googletagmanager.com/gtag/js?id=G-SCPVS91Y1N></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
 
  gtag('config', 'G-SCPVS91Y1N');
</script>
</head>

<body>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v13.0" nonce="Q5qVYwl7"></script>
<div class="page-wrapper">
    <!-- Preloader -->
    <div class="preloader"><div class="icon"></div></div>

    <!-- Main Header -->
    <header class="main-header header-style-one">
        <!-- Header Top -->
        <div class="share-price">
            <marquee onMouseOver="this.stop()" onMouseOut="this.start()"><b><a target="blank" href="https://www.nseindia.com/get-quotes/equity?symbol=AURUM">AURUM NSE:</b> 121.15<span class="share-per" style="color:#090;"> (0.54%) <i class="fa fa-caret-up"></i></span></a>&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;<b><a target="blank" href="https://www.bseindia.com/stock-share-price/majesco-ltd/mjco/539289/">AURUM BSE:</b> 120.95<span class="share-per" style="color:#090"> (+0.88%) <i class="fa fa-caret-up"></i></span></a></marquee>
        </div>

        <div class="notification-bar">
            <span>Integrated PropTech Ecosystem for the Entire Real Estate Value Chain</span><i class='fa fa-times'></i>
        </div>

        <!-- Header Upper -->
        <div class="header-upper">
            <div class="auto-container">
                <div class="inner-container clearfix">
                    <!--Logo-->
                    <div class="logo-box">
                        <div class="logo"><a href="index" title=""><img src="https://aurumproptech.in/wp-content/uploads/2021/11/aurum-proptech.png" alt="" title=""></a></div>
                    </div>
                    <div class="right-nav clearfix">
                        <div class="nav-outer clearfix">
                            <!--Mobile Navigation Toggler-->
                            <div class="mobile-nav-toggler"><span class="icon flaticon-menu-1"></span></div>

                            <!-- Main Menu -->
                            <nav class="main-menu navbar-expand-md navbar-light">
                                <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                                    <ul class="navigation clearfix">
                                        <li class="dropdown"><a >Evolution</a>
                                            <ul class="child-menu">
                                                <li  class="dropdown"><a>PropTech Ecosystem</a>
                                                    <ul>
                                                        <li><a href="company-profile">Company Profile</a></li>
                                                        <li><a href="board-members">Board Of Directors</a></li>
                                                        <li><a href="key-managerial-personnel">Key Managerial Personnel</a></li>
                                                        <li><a href="company-structure">Corporate Structure</a></li>
                                                        <li><a target="_blank" href="images/APT_List of Committees and Compositions.pdf">Committees & Memberships</a></li>
                                                        <!--<li><a href="">Management Team</a></li>-->
                                                    </ul>
                                                </li>
                                                <li class="dropdown"><a>Partners</a>
                                                    <ul>
                                                        <li><a href="aurum-software-solutions-private-limited">Aurum Softwares <span>and</span> Solutions </a></li>
                                                        <li><a href="aurum-realtech-services-private-limited">Aurum RealTech Services</a></li>
                                                        <li><a href="k2v2-technologies">K2V2 Technologies</a></li>
                                                        <li><a href="monktech-labs">MonkTech Labs</a></li>
                                                        <li><a href="integrow-asset-management">Integrow AMC</a></li>
                                                        <li><a target="_blank" href="https://grexter.in/">Grexter Living</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="timeline">Timeline</a></li>
                                                <li><a href="careers">Careers</a></li>
                                                <li><a href="contact">Contact</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown"><a href="product">Products</a>
                                            <ul class="second-menu-parent">
                                                <li class="dropdown"><a href="invest-and-finance-cat">Invest and Finance </a>
													<ul class="second-menu-child">
													<li><a href="data-science-and-analytics">Data Science and Analytics </a></li>
													<li><a href="digital-lending">Digital Lending</a></li>
													<li><a href="fractional-ownership">Fractional Ownership</a></li>
                                                    <li><a href="integrow-asset-management">Integrow AMC</a></li>
													</ul>
												</li>
                                                <li class="dropdown"><a href="enterprise-efficiency-cat">Enterprise Efficiency </a>
													<ul class="second-menu-child">
													<li><a href="construction-marketplace">Construction Marketplace</a></li>
													<li><a href="real-time-document-manager">Real Time Document Manager</a></li>
													<li><a href="customer-relationship-management">Customer Relationship Management</a></li>
													<li><a href="fullfillment-center">Fulfillment Centre</a></li>
													<li><a href="transaction">Transactions </a></li>
                                                    <li><a href="https://www.sell.do/" target="_blank">Sell.Do</a></li>
                                                    <li><a target="_blank" href="https://kylas.io/">Kylas</a></li>
                                                    <li><a target="_blank" href="https://beyondwalls.com/">Beyondwalls</a></li>
                                                    <li><a target="_blank" href="https://www.aurumproptech.com/CREX/">Aurum CREX</a></li>
													</ul>
												</li>
                                                <li class="dropdown"><a href="customer-experience-cat">Customer Experience </a>
													<ul class="second-menu-child">
													<li><a href="virtual-reality">Virtual Reality </a></li>
													<li><a href="home-loan">Home Loans  </a></li>
													<li><a href="digital-escrow">Digital Escrow</a></li>
													<li><a href="interior-design">Interior Design</a></li>
													<li><a href="home-furnishing">Home Furnishing</a></li>
													<li><a href="lifestyle">Lifestyle</a></li>
													</ul>
												</li>
												 <li class="dropdown"><a href="connected-living-cat">Connected Living</a>
													<ul class="second-menu-child">
														<li><a href="amenities-management">Amenities Management</a></li>
														<li><a href="rental-management">Rental Management</a></li>
														<li><a href="co-living">Co-Living</a></li>
														<li><a href="co-working">Co-Working</a></li>
														<li><a href="smart-building-workplace-tech">Smart Buildings and Workplace Tech</a></li>
														<li><a href="property-management">Property Management</a></li>
                                                        <li><a target="_blank" href="https://thehousemonk.com/">TheHouseMonk</a></li>
                                                        <li><a target="_blank" href="https://grexter.in/">Grexter</a></li>
													</ul>
												</li>
												
                                            </ul>
                                        </li>
                                                                          
                                        <!--
										<li><a href="careers">Careers</a></li>
                                        <li><a href="contact">Contact</a></li>-->
										<li><a href="https://aurumproptech.in/investor">Investor</a></li>
                                    </ul>
                                </div>
                            </nav>
                        </div>

                      

                    </div>
                </div>
            </div>
        </div>
        <!--End Header Upper-->

        <!-- Sticky Header  -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="index" title=""><img src="images/Horizontal_AurumPropTech (1).png" alt="" title=""></a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu clearfix">
                        <!--Keep This Empty / Menu will come through Javascript-->
                    </nav><!-- Main Menu End-->
                </div>
            </div>
        </div><!-- End Sticky Menu -->

        <!-- Mobile Menu  -->
        <div class="mobile-menu">
            <div class="menu-backdrop"></div>
            <div class="close-btn"><span class="icon flaticon-targeting-cross"></span></div>
            
            <nav class="menu-box">
                <div class="nav-logo"><a href="index.php"><img src="images/nav-logo.jpg" alt="" title=""></a></div>
                <div class="menu-outer"><!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header--></div>
				<!--Social Links-->
				<div class="social-links">
					<ul class="clearfix">
						<li><a href="#"><span class="fab fa-twitter"></span></a></li>
						<li><a href="#"><span class="fab fa-facebook-square"></span></a></li>
						<li><a href="#"><span class="fab fa-pinterest-p"></span></a></li>
						<li><a href="#"><span class="fab fa-instagram"></span></a></li>
						<li><a href="#"><span class="fab fa-youtube"></span></a></li>
					</ul>
                </div>
            </nav>
        </div><!-- End Mobile Menu -->
    </header>
    <!-- End Main Header -->