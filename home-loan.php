<?php
include "header.php";
?>
 <!-- Inner Banner Section -->
 <section class="inner-banner alternate">
        <div class="image-layer" style="background-image: url(images/background/banner-bg-1.jpg);"></div>
        <div class="auto-container">
            <div class="inner">
                <div class="title-box">
                    <h1>Home Loans</h1>
                </div>
            </div>
        </div>
    </section>
    <!--End Banner Section -->

    <div class="sidebar-page-container services-page">
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Content Side-->

                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                    <div class="service-details product-details">
                        <h3 class="h-text"><strong>Home Loans</strong></h3>
                        <p>Loan Bay- A Home Loan SaaS platform that will help real estate buyers take constructive decisions related to home financing. It does a 360 analysis of various factors producing various financing offers.</p>
                        <div class="row">
                            <!-- DEMO 1 Item-->
                            <div class="col-lg-12 mb-12 mb-lg-12 img-div">
                                <div class="hover hover-1 text-white rounded"><img src="images/loan.jpg" alt="">
                                <div class="hover-overlay"></div>
                                <div class="hover-1-content px-5 py-4">
                                    <h3 class="hover-1-title text-uppercase font-weight-bold mb-0">Loan Bay</h3>
                                    <p class="hover-1-description font-weight-light mb-0">Home Loan SaaS Platform that enables real estate buyers take informed decisions on home financing. The software does a 360 degree analysis of available loan products, buyers credit profile and income and builds financing offers for them. This enhances the buyers purchase experience and provides a one stop solution for his financing options.</p>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>

                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
                    <aside class="sidebar services-sidebar">
                        <!--Services Widget-->
                        <div class="sidebar-widget services-widget">
                            <div class="widget-inner">
                                <ul>
                                    <li><a href="virtual-reality.php">Virtual Reality </a></li>
                                    <li class="active"><a href="home-loan.php">Home Loans  </a></li>
                                    <li><a href="digital-escrow.php">Digital Escrow</a></li>
                                    <li><a href="interior-design.php">Interior Design</a></li>
                                    <li><a href="home-furnishing.php">Home Furnishing</a></li>
                                    <li><a href="lifestyle.php">Lifestyle</a></li>
                                </ul>
                            </div>
                        </div>
                        <!--Info Widget-->
                        <div class="sidebar-widget info-widget">
                            <div class="widget-inner">
                                <div class="image"><img src="images/resource/side-contact.jpg" alt=""></div>
                                <div class="lower">
                                    <div class="subtitle">Got any Questions? <br>Call us Today!</div>
                                    <div class="icon-box"><span class="flaticon-telephone"></span></div>
                                    <div class="phone"><a href="tel:+91 22 3000 1700 / 2778 1271">+91 22 3000 1700 / 2778 1271</a></div>
                                    <div class="email"><a href="mailto:corporate@aurumproptech.in">corporate@aurumproptech.in</a></div>
                                </div>
                            </div>
                        </div>
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
<?php
include "footer.php";
?>