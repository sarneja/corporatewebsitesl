<?php
include "header.php";
?>

<div class="cookie-disclaimer row">

    <div class="col-md-12 text-center" style="cursor:default;">We use cookies to operate our services. We also share information about your use of our site with our advertising and analytics partners. You can reject non-necessary cookies below.<a href="cookie-policy.php" class="ml-1 text-decoration-none" style="color:#aaaeb4"><b> Cookie policy</b></a> </div>

    <div class="col-md-12 text-center">

        <button class="allow-button mr-1 accept-cookie">Allow cookies</button> <button class="allow-button no-accept-cookie">cancel</button>

    </div>

</div>    <!-- Banner Section -->
    <section class="banner-section banner-one">
        <div class="banner-curve"></div>

		<div class="banner-carousel theme-carousel owl-theme owl-carousel" data-options='{"loop": true, "margin": 0, "autoheight":true, "lazyload":true, "nav": true, "dots": true, "autoplay": true, "autoplayTimeout": 6000, "smartSpeed": 2000, "responsive":{ "0" :{ "items": "1" }, "768" :{ "items" : "1" } , "1000":{ "items" : "1" }}}'>
			
            <!-- Slide Item -->
			<div class="slide-item">
				<div class="auto-container">
					<div class="content-box">
                        <div class="round-layer"></div>
                        
                        <div class="content">
                            <div class="inner">
                                <!--<div class="sub-title">IT Solutions For Easy Integration</div>-->
        						<h1 style="font-size:50px;"><strong>Invest and Finance</strong> </h1>
        							<h1 style="font-size:40px;">Technology suite that enables data driven Real Estate investment decisions</h1>
                                <div class="text">Data Driven Capital Allocation</div>
        						<div class="links-box">
                                    <a href="invest-and-finance-cat.php" class="theme-btn btn-style-one"><div class="btn-title">More Details</div></a>
                                    <!--<a href="" class="theme-btn btn-style-two"><div class="btn-title"><span class="icon flaticon-play-button"></span> Watch The Demo</div></a>-->
                                </div>
                            </div>
                        </div>
                        <div class="content-image"><img src="images/main-slider/Investment (2).png" alt="" title=""></div>
					</div>  
				</div>
			</div>

			<!-- Slide Item -->
            <div class="slide-item">

                <div class="auto-container">
                    <div class="content-box">
                        <div class="round-layer"></div>

                        <div class="content">
                            <div class="inner alternate">
                                <h1 style="font-size:50px;"><strong>Enterprise Efficiency </strong></h1>
                                <h1 style="font-size:40px;">Softwares and solutions to increase efficiency of Real Estate business</h1>
                                <div class="text">Softwares and solutions to increase efficiency of Real Estate business</div>
                                <div class="links-box">
                                    <a href="enterprise-efficiency-cat.php" class="theme-btn btn-style-one"><div class="btn-title">More Details</div></a>
                                    <!--<a href="" class="theme-btn btn-style-two"><div class="btn-title">Our Services</div></a>-->
                                </div>
                            </div>
                        </div>
                        <div class="content-image"><img src="images/main-slider/Enterprise efficiency.png" alt="" title=""></div>
                    </div>  
                </div>
            </div>

			<!-- Slide Item -->
            <div class="slide-item">
                <div class="auto-container">
                    <div class="content-box">
                        <div class="round-layer"></div>
                        
                        <div class="content">
                            <div class="inner">
                                <h1 style="font-size:50px;"><strong>Customer Experience</strong></h1>
                                <h1 style="font-size:40px;">Tech platforms to enhance consumer experience of real estate transactions</h1>
                                <div class="text">Enhanced consumer experience of Real Estate purchase</div>
                                <div class="links-box">
                                    <a href="customer-experience-cat.php" class="theme-btn btn-style-one"><div class="btn-title">More Details</div></a>
                                    <!--<a href="" class="theme-btn btn-style-two"><div class="btn-title">Our Services</div></a>-->
                                </div>
                            </div>
                        </div>
                        <div class="content-image"><img src="images/main-slider/customer experience1.png" alt="" title=""></div>
                    </div>  
                </div>
            </div>

            <!-- Slide Item -->
            <div class="slide-item">

                <div class="auto-container">
                    <div class="content-box">
                        <div class="round-layer"></div>

                        <div class="content">
                            <div class="inner alternate">
                                <h1 style="font-size:50px;"><strong>Connected Living</strong></h1>
                                <h1 style="font-size:40px;">Digitisation of real estate to enable social commerce around real estate</h1>
                                <div class="text">End-to-End living experience at life and work</div>
                                <div class="links-box">
                                    <a href="connected-living-cat.php" class="theme-btn btn-style-one"><div class="btn-title">More Details</div></a>
                                    <!--<a href="" class="theme-btn btn-style-two"><div class="btn-title">Our Services</div></a>-->
                                </div>
                            </div>
                        </div>
                        <div class="content-image"><img src="images/main-slider/connected living1.png" alt="" title=""></div>
                    </div>  
                </div>
            </div>

		</div>
    </section>
    <!--End Banner Section -->

    <!--Services Section-->
<section class="services-section" style="
background-image: url(images/background.png);
/*background-position: top right;*/
/* background-color: #120034; */
background-repeat: no-repeat;
/*background-size: 100% auto;*/
opacity: 1;
/*background: linear-gradient(175deg, #e8ecff 0%, #e6e4f9 40%, #e5d7f6 100%) !important;*/
transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
">
<div class="pattern-layer"></div> <div class="auto-container">
<div class="row clearfix">
<!--Column-->
<div class="column col-lg-4 col-md-12 col-sm-12">
<div class="sec-title wow fadeInUp animated" data-wow-delay="0ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInUp;">
<div class="upper-text">PropTech Ecosystem</div>
<h2 style="color:#382b73;"><strong>Products &amp; Services</strong></h2>
<div class="lower-text" style="text-align:justify;">The unique feature of the ecosystem is that it leverages cross functional synergies of all segments because of its integrated nature. The ecosystem is supported by a robust fulfillment center based out of Mumbai. </div>
</div> </div> <!--Column-->
<div class="column col-lg-4 col-md-12 col-sm-12"> <!--Service Block-->
<div class="service-block wow fadeInUp animated" data-wow-delay="0ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInUp;">
<div class="inner-box" style="border-radius: 10px; border:2px solid rgba( 255, 255, 255,0.6)">
<div class="icon-outer">
<span class="icon-bg"></span>
<div class="icon-box"><img src="images/icons/services/2.png" alt="" title=""></div>
</div>
<h3><a href="invest-and-finance-cat.php">Invest and Finance</a></h3>
<div class="text">Data science and analytics tools platforms to enable smart investment decisions for institutional and retail investors.</div>
<div class="more-link"><a href="invest-and-finance-cat.php"><span class="fa fa-arrow-right"></span></a></div>
</div>
</div> <!--Service Block-->
<div class="service-block wow fadeInUp animated" data-wow-delay="300ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 300ms; animation-name: fadeInUp;">
<div class="inner-box" style="border-radius: 10px; border:2px solid rgba( 255, 255, 255,0.6)

">
<div class="icon-outer">
<span class="icon-bg"></span>
<div class="icon-box"><img src="images/icons/services/3.png" alt="" title=""></div>
</div>
<h3><a href="enterprise-efficiency-cat.php">Enterprise Efficiency</a></h3>
<div class="text">The segment offers product suites and platforms to help Real Estate developers and firms increase the efficiency of cost, time and effort in real estate construction. </div>
<div class="more-link"><a href="enterprise-efficiency-cat.php"><span class="fa fa-arrow-right"></span></a></div>
</div>
</div> </div> <!--Column-->
<div class="column col-lg-4 col-md-12 col-sm-12">
<!--Service Block-->
<div class="service-block wow fadeInUp animated" data-wow-delay="0ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInUp;">
<div class="inner-box" style="border-radius: 10px; border:2px solid rgba( 255, 255, 255,0.6)

">
<div class="icon-outer">
<span class="icon-bg"></span>
<div class="icon-box"><img src="images/icons/services/5.png" alt="" title=""></div>
</div>
<h3><a href="customer-experience-cat.php">Customer Experience</a></h3>
<div class="text">Solutions that leverage technology and elevate the purchase experience of Real Estate consumers.</div>
<div class="more-link"><a href="customer-experience-cat.php"><span class="fa fa-arrow-right"></span></a></div>
</div>
</div> <!--Service Block-->
<div class="service-block wow fadeInUp animated" data-wow-delay="300ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 300ms; animation-name: fadeInUp;">
<div class="inner-box" style="border-radius: 10px; border:2px solid rgba( 255, 255, 255,0.6)

">
<div class="icon-outer">
<span class="icon-bg"></span>
<div class="icon-box"><img src="images/icons/services/4.png" alt="" title=""></div>
</div>
<h3><a href="connected-living-cat.php">Connected Living</a></h3>
<div class="text">Technology solutions that create lifestyle experience around real estate spaces and asset management products that preserve and enhance the value of real estate. </div>
<div class="more-link"><a href="connected-living-cat.php"><span class="fa fa-arrow-right"></span></a></div>
</div>
</div> </div> </div>
</div>
</section>



    <!--Locations Section-->
   


    <!--Fun Facts Section-->
    

    <!--Separator-->
    <!--<div class="theme-separator"></div>-->

    <!--Appointment Section-->
    

    <!--Testimonials Section-->
    

    <!--News Section-->
    <section class="news-section" style="background-repeat: no-repeat; background-size: cover; background-position: center center; background-image: url('images/52-521635_high-resolution-computer-theme-light-blu.jpg'); padding: 50px 0px 30px;">
    <div class="overlay-dark" style="
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background: rgb(255 255 255 / 60%);

        "></div>
        <div class="auto-container">

            <div class="sec-title centered">
                <!--<h3 class="upper-text" style="text-transform:capitalize;">Aurum PropTech News</h3>-->
                <h2 style="font-weight: 700;">Latest News & Articles</h2>
                <!--<div class="lower-text">Sit amet consectetur adipisicing elitm sed eiusmod temp sed incididunt labore dolore magna aliquatenim veniam quis ipsum nostrud exer citation ullamco laboris.</div>-->
            </div>
            <div class="upper-row">
                <div class="row clearfix">
                    
                    <!--News Block-->
                    <div class="news-block col-lg-4 col-md-6 col-sm-12">
                        <div class="inner-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                            <div class="image-box">
                                <a target="_blank" href="https://www.ashishdeora.com/aurum-proptech-to-acquire-53-percent-stake-in-co-living-platform-grexter-housing-solutions.php"><img src="images/News/news-1.jpg" alt="" title=""></a>
                            </div>
                            <div class="lower-box" style="height: 220px; background: rgba(255,255,255,0.5) !important;
backdrop-filter: blur(
4.5px

);">
                                <!--<div class="category">Mobile Apps</div>-->
                                <h3><a target="_blank" href="https://www.ashishdeora.com/aurum-proptech-to-acquire-53-percent-stake-in-co-living-platform-grexter-housing-solutions.php">Acquire 53% stake in co-living platform Grexter Housing Solutions</a></h3>
                                <div class="meta-info">
                                    <ul class="clearfix">
                                        <li><a target="_blank" href="https://www.ashishdeora.com/aurum-proptech-to-acquire-53-percent-stake-in-co-living-platform-grexter-housing-solutions.php">07 Feb 2022</a></li>
                                    </ul>
                                </div>
                                <div class="more-link"><a target="_blank" href="https://www.ashishdeora.com/aurum-proptech-to-acquire-53-percent-stake-in-co-living-platform-grexter-housing-solutions.php"><span class="fa fa-arrow-right"></span></a></div>
                            </div>
                        </div>
                    </div>
                    <!--News Block-->
                    <div class="news-block col-lg-4 col-md-6 col-sm-12">
                        <div class="inner-box wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                            <div class="image-box">
                                <a target="_blank" href="https://www.ashishdeora.com/Aurum-PropTech-to-invest-$5-million-in-rental-management-platform-TheHouseMonk.php"><img src="images/News/news-2.jpg" alt="" title=""></a>
                            </div>
                            <div class="lower-box" style="height: 220px; background: rgba(255,255,255,0.5) !important;
backdrop-filter: blur(
4.5px

);">
                                <h3><a target="_blank" href="https://www.ashishdeora.com/Aurum-PropTech-to-invest-$5-million-in-rental-management-platform-TheHouseMonk.php">Aurum PropTech to invest $5 million in rental management platform TheHouseMonk</a></h3>
                                <div class="meta-info">
                                    <ul class="clearfix">
                                        <li><a target="_blank" href="https://www.ashishdeora.com/Aurum-PropTech-to-invest-$5-million-in-rental-management-platform-TheHouseMonk.php">19 Dec 2021</a></li>
                                    </ul>
                                </div>
                                <div class="more-link"><a target="_blank" href="https://www.ashishdeora.com/Aurum-PropTech-to-invest-$5-million-in-rental-management-platform-TheHouseMonk.php"><span class="fa fa-arrow-right"></span></a></div>
                            </div>
                        </div>
                    </div>
                    <!--News Block-->
                    <div class="news-block col-lg-4 col-md-6 col-sm-12">
                        <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <div class="image-box">
                                <a target="_blank" href="https://www.ashishdeora.com/Aurum-Pro-Tech-to-raise-Rs-343-cr-via-rights-issue.php"><img src="images/News/news-3.jpg" alt="" title=""></a>
                            </div>
                            <div class="lower-box" style="height: 220px; background: rgba(255,255,255,0.5) !important;
backdrop-filter: blur(
4.5px

);">
                                <h3><a target="_blank" href="https://www.ashishdeora.com/Aurum-Pro-Tech-to-raise-Rs-343-cr-via-rights-issue.php">Aurum PropTech to raise Rs 343 cr via rights issue</a></h3>
                                <div class="meta-info">
                                    <ul class="clearfix">
                                        <li><a target="_blank" href="https://www.ashishdeora.com/Aurum-Pro-Tech-to-raise-Rs-343-cr-via-rights-issue.php">18 Dec 2021</a></li>
                                    </ul>
                                </div>
                                <div class="more-link"><a target="_blank" href="https://www.ashishdeora.com/Aurum-Pro-Tech-to-raise-Rs-343-cr-via-rights-issue.php"><span class="fa fa-arrow-right"></span></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    

    <!--About Section-->
    <!--About Section Two-->
    <section class="about-section-two">
        <div class="pattern-layer"></div>

        <div class="auto-container">
        	<div class="row clearfix">
                <!--Left Column-->
                <div class="left-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner">
                        <figure class="image">
                            <img src="images/vision.jpg" alt="" title="">
                            <div class="over-icon  wow zoomInStable" data-wow-delay="0ms" data-wow-duration="2500ms">
                                <img src="images/about-logo-image.jpg" alt="" title="">
                            </div>
                        </figure>
                        <!--<div class="sec-title">
                            <div class="upper-text" style="text-transform:uppercase;">Revolutionizing Real Estate</div>
                            <h2>Integrated PropTech Ecosystem for Capital Allocators, Creators and Consumers of Real Estate</h2>
                            <div class="lower-text">The integrated ecosystem includes a wide range of PropTech products, services, and platforms. These are categorized into 4 segments. Invest and Finance, Enterprise Efficiency, Customer Experience and Connected Living. There are more than 20 tech enabled products and services offered in these four segments.</div>
                        </div>-->
                        <div class="text-content">
                            <h3 class="upper-text" style="text-transform: capitalize;opacity: 0.6;">Revolutionizing Real Estate</h3>
                            <h2 style="font-size: 25px;    text-transform: capitalize;">Integrated PropTech Ecosystem for Capital Allocators, Creators and Consumers of Real Estate</h2>
                            <p style="margin-bottom:15px;">The integrated ecosystem includes a wide range of PropTech products, services, and platforms. These are categorized into 4 segments. Invest and Finance, Enterprise Efficiency, Customer Experience and Connected Living. There are more than 20 tech enabled products and services offered in these four segments.</p>
                        </div>

                        <!--Features-->
                        <div class="features" style="margin-top: 37px;">
                            <div class="row clearfix">
                                <!--Feature Block-->
                                <div class="feature-block col-lg-4 col-md-4 col-sm-6">
                                    <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                                        <div class="icon-box">
                                            <span class="flaticon-code"></span>
                                        </div>
                                        <div class="title" style="font-size: 17px;">SaaS</div>
                                    </div>
                                </div>
                                <!--Feature Block-->
                                <div class="feature-block col-lg-4 col-md-4 col-sm-6">
                                    <div class="inner-box wow fadeInLeft" data-wow-delay="150ms" data-wow-duration="1500ms">
                                        <div class="icon-box">
                                            <span class="flaticon-report"></span>
                                        </div>
                                        <div class="title" style="font-size: 17px;">PLATFORM SaaS</div>
                                    </div>
                                </div>
                                <!--Feature Block-->
                                <div class="feature-block col-lg-4 col-md-4 col-sm-6">
                                    <div class="inner-box wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                                        <div class="icon-box">
                                            <span class="flaticon-consult"></span>
                                        </div>
                                        <div class="title" style="font-size: 17px;">RaaS</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <!--Right Column-->
                <div class="right-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner">
                        <figure class="image">
                            <img src="images/vision2.jpg" alt="" title="">
                        </figure>
                        <div class="text-content">
                            <h3 class="upper-text" style="text-transform: capitalize;opacity: 0.6;">Company Mission And Vision</h3>
                            <p>We aim to create a tech driven revolution in Real Estate with an objective of enabling data driven investments, increasing enterprise efficiency, enhancing consumer experience and enabling connected living.</p>
                            
                            <ul class="list-style-one">
                                <li>Data science and analytics tools platforms to enable smart investment decisions for institutional and retail investors.</li>
                                <li>Product suites and platforms to help Real Estate developers and firms increase the efficiency of business.</li>
                                <li>Solutions that leverage technology and elevate the purchase experience of Real Estate consumers.</li>
                                <li>Technology solutions that create lifestyle experience around real estate spaces and asset management products that preserve and enhance the value of real estate.</li>
                            </ul>
                        </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

   

    <!--Contact Section-->
    <section class="contact-section-two" style="
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center center;
        background-image: url('images/WallpaperDog-17205044.jpg');
        /* background-image: url(https://kingstudio.ro/demos/glass-ui/assets/images/blur.jpg); */
        "><div class="overlay-dark" style="
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background: rgba(255,255,255,0.);
        /* backdrop-filter: blur(6px); */
        "></div>
        <div class="auto-container">
        <div class="upper-row">
        <div class="row clearfix">
        <!--Text Column-->
        <div class="text-column col-lg-6 col-md-12 col-sm-12">
        <div class="inner">
        <div class="sec-title">
        <div class="upper-text" style="text-transform:capitalize;">Send us a Message</div>
        <h2 style="font-size:32px;">Do You Have Any Questions? We'll Be Happy To Assist!</h2>
             </div>
        <!--
        <div class="social-links">
        <ul class="clearfix">
        <li><a href="https://www.facebook.com/AurumPropTech"> class="fab fa-facebook-square"></span></a></li>
        <li><a href="#"><span class="fab fa-twitter"></span></a></li>
        <li><a href="#"><span class="fab fa-instagram"></span></a></li>
        <li><a href="#"><span class="fab fa-youtube"></span></a></li>
        <li><a href="#"><span class="fab fa-pinterest"></span></a></li>
        </ul>
        </div>-->
        </div>
        </div>
        <!--Form Column-->
        <div class="form-column contact-form-box col-lg-6 col-md-12 col-sm-12" style="
        background: rgba(255,255,255,0.2) !important;
        backdrop-filter: blur(4.5px);
        ">

<script>
function Validate(id) 
{
    var val = document.getElementById(id).value;
   
if(val!=''){
    if (!val.match(/^[A-Za-z][A-Za-z\s]*$/)) 
    {
        alert('Only alphabets are allowed');
	document.getElementById(id).value="";
        return false;
    }
}
    
    return true;
}

function ValidateEmail(mail) 
{
var re = /\S+@\S+\.\S+/;
 if (re.test(mail))
  {
    return (true)
  }else{
    alert("You have entered an invalid email address!");
	document.getElementById('email').value="";

    return (false)
 }
}

    function inpNum(e) {
  e = e || window.event;
  var charCode = (typeof e.which == "undefined") ? e.keyCode : e.which;
  var charStr = String.fromCharCode(charCode);
  if (!charStr.match(/^[0-9]+$/))
    e.preventDefault();
}
</script>
        <div class="inner">
        <!--Form Box-->
        <div class="form-box">
        <div class="default-form contact-form">
 <form method="post" action="" id="contact-form">
                                    <p style="color:#000;"><b>Note :</b> <i><span class="tx-danger">*</span> Fields are Mandatory</i></p>
                                        <div class="row clearfix">                                    
                                            <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                                <input type="text" name="username" id="username" placeholder="Your Name*" required="" onkeyup="Validate('username')" value="">
                                            </div>
                                            
                                            <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                                <input type="email" name="email" id="email" placeholder="Your Email*" required="" onchange="ValidateEmail(this.value)" value="">
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                                <input type="number" name="phone" id="phone" placeholder="Your Phone Number*" required="" value="" onkeypress="inpNum(event)">
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                                <input type="text" name="subject" id="subject" placeholder="Subject*" required="" value="">
                                            </div>

                                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                                <textarea name="message" id="message" placeholder="Message*" required=""></textarea>
                                            </div>
                    
                                            <div class="form-group col-md-12 col-sm-12">
                                                <button type="button" class="theme-btn btn-style-one contact-submit"><span class="btn-title">Make a Request</span></button>
                                            </div>
                                        </div>
                                    </form>        </div>
        </div>
        </div>
        </div>



        </div>
        </div>
        </div>
    </section>
    
    

    <!--Sponsors Section-->
    <section class="sponsors-section">
        <div class="sponsors-outer">
            <!--Sponsors-->
            <div class="auto-container">
                <!--Sponsors Carousel-->
                <div class="sponsors-carousel theme-carousel owl-theme owl-carousel" data-options='{"loop": true, "margin": 30, "autoheight":true, "lazyload":true, "nav": true, "dots": true, "autoplay": true, "autoplayTimeout": 6000, "smartSpeed": 500, "responsive":{ "0" :{ "items": "1" }, "600" :{ "items" : "2" }, "768" :{ "items" : "3" } , "800":{ "items" : "3" }, "1024":{ "items" : "4" }, "1200":{ "items" : "5" }}}'>
                    <div class="slide-item"><figure class="image-box"><a href="https://www.sell.do/" target="_blank"><img src="images/clients/3.png" alt=""></a></figure></div>
                    <div class="slide-item"><figure class="image-box"><a href="https://thehousemonk.com/" target="_blank"><img src="images/clients/4.png" alt=""></a></figure></div>
                    <div class="slide-item"><figure class="image-box"><a href="https://www.integrowamc.com/" target="_blank"><img src="images/clients/6.png" alt=""></a></figure></div>
                  
                    <div class="slide-item"><figure class="image-box"><a href="https://grexter.in/" target="_blank"><img style="margin-top: 31px;width:80%" src="images/logo-blue.png" alt=""></a></figure></div>
  
                </div>
            </div>
        </div>
    </section>


    <section class="social-section" style="background-image: url(images/background.png);
    background-position: top right;
    /* background-color: #120034; */
    background-repeat: no-repeat;
    background-size: 100% auto;
    opacity: 1;
    /* background: linear-gradient(175deg, #e8ecff 0%, #e6e4f9 40%, #e5d7f6 100%) !important; */
    transition: background 0.3s, border-radius 0.3s, opacity 0.3s;">
        <div class="auto-container">
        	<div class="row clearfix">
                <div class="sec-title centered" style="margin-bottom: 0px; margin-top: 50px;margin-bottom:60px;">
                    <h2 style="font-weight:700;  color: #251d59;
">Connect With Us</h2>
                </div>
         

<div class="container" id="home_feature2">

  
          <div class="row">

            			<div class="col-md-4 col-sm-12">

                <div class="region region-home-youtube">
    <section id="block-homelinkedinwidget" class="block block-block-content block-block-content553c618e-550f-49cf-91e4-f8e42de20d82 clearfix">
  
    

      
            <div><h3 class="title" id="facebookk" style="border-radius: 5px;text-transform:capitalize;padding:15px;color:#ffffff; font-weight:600;    color: #251d59;
    background: rgba(255,255,255,0.2) !important;
    backdrop-filter: blur(4.5px);
    font-weight: 600;font-size:18px;
    box-shadow: 0px 0px 25px 0px rgb(0 0 0 / 20%);border:2px solid rgba( 255, 255, 255,0.6);">TWITTER</h3>

<div class="tab-container">
<div class="tab-panel active" id="1503477967172-76da20e4-eb7c" style="height: 350px;">
<div class="worldmart-products  worldmart_custom_css_821222451  responsive_js_composer_custom_css_775545529" style="    border-radius: 5px;border:2px solid rgba( 255, 255, 255,0.6);padding:6px;padding:14px;text-align:justify;height:311px;overflow-y:scroll;background: rgba(255,255,255,0.2) !important;
    backdrop-filter: blur(4.5px);
    /* font-weight: 600; */
    /* font-size: 18px; */
    box-shadow: 0px 0px 25px 0px rgb(0 0 0 / 20%);
    overflow-y: scroll;">


<a class="twitter-timeline" href="https://twitter.com/AurumProptech?ref_src=twsrc%5Etfw">Tweets by AurumPropTech</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


</div>
</div>
</div>
</div>
      
  </section>


  </div>


            </div>

            <div class="col-md-4 col-sm-12">

                <div class="region region-home-feature-facebook">
    <section id="block-hometwitterscetion" class="block block-block-content block-block-content5901f6e6-51e3-436c-ade6-ce1790694bfb clearfix">
  
    

      
            <div><h3 class="title" id="facebookk" style="border-radius: 5px;text-transform:capitalize;padding:15px;color:#ffffff; font-weight:600;    color: #251d59;
    background: rgba(255,255,255,0.2) !important;
    backdrop-filter: blur(4.5px);
    font-weight: 600;font-size:18px;
    box-shadow: 0px 0px 25px 0px rgb(0 0 0 / 20%);border:2px solid rgba( 255, 255, 255,0.6);">FACEBOOK</h3>

<div class="tab-container">
<div class="tab-panel active" id="1503477967172-76da20e4-eb7c" style="height: 350px;">
<div class="worldmart-products  worldmart_custom_css_821222451  responsive_js_composer_custom_css_775545529" style="border-radius: 5px;border:2px solid rgba( 255, 255, 255,0.6);padding:6px;padding:14px;text-align:justify;height:311px;overflow-y:scroll;background: rgba(255,255,255,0.2) !important;
    backdrop-filter: blur(4.5px);
    /* font-weight: 600; */
    /* font-size: 18px; */
    box-shadow: 0px 0px 25px 0px rgb(0 0 0 / 20%);
    overflow-y: scroll;">

 <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FAurumPropTech&tabs=timeline&width=300&height=450&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="300" height="450" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe></div>
</div>
</div>
</div>
      
  </section>


  </div>


            </div>

			<div class="col-md-4 col-sm-12">

                <div class="region region-home-feature-twitter">
    <section id="block-homepagefacebookblock" class="block block-block-content block-block-content1ee029b5-0b7e-4cd8-977d-14ad6c32699e clearfix">
  
    

      
            <div><h3 class="title" id="facebookk" style="border-radius: 5px;text-transform:capitalize;padding:15px;color:#ffffff; font-weight:600;    color: #251d59;
    background: rgba(255,255,255,0.2) !important;
    backdrop-filter: blur(4.5px);
    font-weight: 600;font-size:18px;
    box-shadow: 0px 0px 25px 0px rgb(0 0 0 / 20%);border:2px solid rgba( 255, 255, 255,0.6);">LINKEDIN</h3>

<div class="tab-container">
<div class="tab-panel active" id="1503477967172-76da20e4-eb7c" style="height: 310px;">
<div class="worldmart-products  worldmart_custom_css_821222451  responsive_js_composer_custom_css_775545529" style="border-radius: 5px;border:2px solid rgba( 255, 255, 255,0.6);padding:6px;padding:14px;text-align:justify;height:311px;overflow-y:scroll;background: rgba(255,255,255,0.2) !important;
    backdrop-filter: blur(4.5px);
    /* font-weight: 600; */
    /* font-size: 18px; */
    box-shadow: 0px 0px 25px 0px rgb(0 0 0 / 20%);
    overflow-y: scroll;">
<iframe src="https://www.linkedin.com/embed/feed/update/urn:li:share:6860146851400736768" height="400" width="300" frameborder="0" allowfullscreen="" title="Embedded post"></iframe>
</div>
</div>
</div>
</div>
      
  </section>


  </div>


            </div>

            

       </div>

  </div>


                </div>
            </div>
        </div>
    </section>



<style>
.eapps-instagram-feed-title {
    font-size: 32px !important;
    font-weight: 700;
    color: #251d59 !important;
    text-align: center;
    line-height: 32px;
    padding: 60px 10px !important;
}
</style>
    <script src="https://apps.elfsight.com/p/platform.js" defer></script>
<div style="margin-bottom:50px;" class="elfsight-app-47dc7def-3e68-4a49-b5e4-198fafd6a783"></div><br><?php
include "footer.php";
?>