<?php
include "header.php";
?>
    <!-- Banner Section -->
    <section class="banner-section banner-one">
        <div class="banner-curve"></div>

		<div class="banner-carousel theme-carousel owl-theme owl-carousel" data-options='{"loop": true, "margin": 0, "autoheight":true, "lazyload":true, "nav": true, "dots": true, "autoplay": true, "autoplayTimeout": 6000, "smartSpeed": 300, "responsive":{ "0" :{ "items": "1" }, "768" :{ "items" : "1" } , "1000":{ "items" : "1" }}}'>
			
            <!-- Slide Item -->
			<div class="slide-item">
				<div class="auto-container">
					<div class="content-box">
                        <div class="round-layer"></div>
                        
                        <div class="content">
                            <div class="inner">
                                <!--<div class="sub-title">IT Solutions For Easy Integration</div>-->
        						<h1>Deliver <strong>Invest and Finance</strong> For Technology Insights</h1>
                                <div class="text">Data Driven Capital Allocation</div>
        						<div class="links-box">
                                    <a href="" class="theme-btn btn-style-one"><div class="btn-title">More Details</div></a>
                                    <!--<a href="" class="theme-btn btn-style-two"><div class="btn-title"><span class="icon flaticon-play-button"></span> Watch The Demo</div></a>-->
                                </div>
                            </div>
                        </div>
                        <div class="content-image"><img src="images/main-slider/content-image-1.png" alt="" title=""></div>
					</div>  
				</div>
			</div>

			<!-- Slide Item -->
            <div class="slide-item">

                <div class="auto-container">
                    <div class="content-box">
                        <div class="round-layer"></div>

                        <div class="content">
                            <div class="inner alternate">
                                <h1><strong>Enterprise Efficiency</strong> To Develop  Solutions Your Way!</h1>
                                <div class="text">Tech driven efficient Real Estate construction and Smart ERP</div>
                                <div class="links-box">
                                    <a href="" class="theme-btn btn-style-one"><div class="btn-title">More Details</div></a>
                                    <!--<a href="" class="theme-btn btn-style-two"><div class="btn-title">Our Services</div></a>-->
                                </div>
                            </div>
                        </div>
                        <div class="content-image"><img src="images/main-slider/content-image-2.png" alt="" title=""></div>
                    </div>  
                </div>
            </div>

			<!-- Slide Item -->
            <div class="slide-item">
                <div class="auto-container">
                    <div class="content-box">
                        <div class="round-layer"></div>
                        
                        <div class="content">
                            <div class="inner">
                                <h1>Deliver <strong>Customer Experience</strong> For Technology Insights</h1>
                                <div class="text">Enhanced consumer experience of Real Estate purchase</div>
                                <div class="links-box">
                                    <a href="" class="theme-btn btn-style-one"><div class="btn-title">More Details</div></a>
                                    <!--<a href="" class="theme-btn btn-style-two"><div class="btn-title">Our Services</div></a>-->
                                </div>
                            </div>
                        </div>
                        <div class="content-image"><img src="images/main-slider/content-image-1.png" alt="" title=""></div>
                    </div>  
                </div>
            </div>

            <!-- Slide Item -->
            <div class="slide-item">

                <div class="auto-container">
                    <div class="content-box">
                        <div class="round-layer"></div>

                        <div class="content">
                            <div class="inner alternate">
                                <h1><strong>Connected Living</strong> To Develop  Solutions Your Way!</h1>
                                <div class="text">End-to-End living experience at life and work</div>
                                <div class="links-box">
                                    <a href="" class="theme-btn btn-style-one"><div class="btn-title">More Details</div></a>
                                    <!--<a href="" class="theme-btn btn-style-two"><div class="btn-title">Our Services</div></a>-->
                                </div>
                            </div>
                        </div>
                        <div class="content-image"><img src="images/main-slider/content-image-2.png" alt="" title=""></div>
                    </div>  
                </div>
            </div>

		</div>
    </section>
    <!--End Banner Section -->

    <!--Services Section-->
    <section class="services-section">
       
        <div class="pattern-layer"></div>

        <div class="auto-container">
            <div class="row clearfix">
                <!--Column-->
                <div class="column col-lg-4 col-md-12 col-sm-12">
                    <div class="sec-title wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="upper-text">PropTech Ecosystem</div>
                        <h2><strong>Products & Services</strong></h2>
                        <div class="lower-text" style="text-align:justify;">The unique feature of the ecosystem is that it leverages cross functional synergies of all segments because of its integrated nature. The ecosystem is supported by a robust fulfillment center based out of Mumbai.  </div>
                    </div>

                   

                </div>

                <!--Column-->
                <div class="column col-lg-4 col-md-12 col-sm-12">

                    <!--Service Block-->
                    <div class="service-block wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <div class="icon-outer">
                                <span class="icon-bg"></span>
                                <div class="icon-box"><img src="images/icons/services/2.png" alt="" title=""></div>
                            </div>
                            <h3><a href="l">Invest and Finance</a></h3>
                            <div class="text">Data science and analytics tools and platforms to enable smart investment decisions for institutional and retail investors.</div>
                            <div class="more-link"><a href="invest-and-finance-cat.php"><span class="fa fa-arrow-right"></span></a></div>
                        </div>
                    </div>

                    <!--Service Block-->
                    <div class="service-block wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <div class="icon-outer">
                                <span class="icon-bg"></span>
                                <div class="icon-box"><img src="images/icons/services/3.png" alt="" title=""></div>
                            </div>
                            <h3><a href="">Enterprise Efficiency</a></h3>
                            <div class="text">The segment offers product suites and platforms to help Real Estate developers and firms increase efficiency of cost, time and effort in real estate construction </div>
                            <div class="more-link"><a href="enterprise-efficiency-cat.php"><span class="fa fa-arrow-right"></span></a></div>
                        </div>
                    </div>

                </div>

                <!--Column-->
                <div class="column col-lg-4 col-md-12 col-sm-12">
                    <!--Service Block-->
                    <div class="service-block wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <div class="icon-outer">
                                <span class="icon-bg"></span>
                                <div class="icon-box"><img src="images/icons/services/5.png" alt="" title=""></div>
                            </div>
                            <h3><a href="">Customer Experience</a></h3>
                            <div class="text">Solutions that leverage technology and elevate the purchase experience of Real Estate consumers.</div>
                            <div class="more-link"><a href="customer-experience-cat.php"><span class="fa fa-arrow-right"></span></a></div>
                        </div>
                    </div>

                    <!--Service Block-->
                    <div class="service-block wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <div class="icon-outer">
                                <span class="icon-bg"></span>
                                <div class="icon-box"><img src="images/icons/services/4.png" alt="" title=""></div>
                            </div>
                            <h3><a href="">Connected Living</a></h3>
                            <div class="text">Technology solutions that create lifestyle experience around real estate spaces and asset management products that preserve and enhance the value of real estate. </div>
                            <div class="more-link"><a href="connected-living-cat.php"><span class="fa fa-arrow-right"></span></a></div>
                        </div>
                    </div>

                </div>

            </div>

           
        </div>
    </section>

    <!--Locations Section-->
   


    <!--Fun Facts Section-->
    

    <!--Separator-->
    <div class="theme-separator"></div>

    <!--Appointment Section-->
    

    <!--Testimonials Section-->
    

    <!--News Section-->
    <section class="news-section">
        <div class="auto-container">

            <div class="sec-title centered">
                <h3 class="upper-text" style="text-transform:capitalize;">Aurum PropTech News</h3>
                <h2>Latest News & Articles</h2>
                <!--<div class="lower-text">Sit amet consectetur adipisicing elitm sed eiusmod temp sed incididunt labore dolore magna aliquatenim veniam quis ipsum nostrud exer citation ullamco laboris.</div>-->
            </div>
            <div class="upper-row">
                <div class="row clearfix">
                    
                    <!--News Block-->
                    <div class="news-block col-lg-4 col-md-6 col-sm-12">
                        <div class="inner-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                            <div class="image-box">
                                <a target="_blank" href="https://www.ashishdeora.com/aurum-proptech-to-acquire-53-percent-stake-in-co-living-platform-grexter-housing-solutions.php"><img src="images/News/news-1.jpg" alt="" title=""></a>
                            </div>
                            <div class="lower-box" style="height: 220px;">
                                <!--<div class="category">Mobile Apps</div>-->
                                <h3><a target="_blank" href="https://www.ashishdeora.com/aurum-proptech-to-acquire-53-percent-stake-in-co-living-platform-grexter-housing-solutions.php">Acquire 53% stake in co-living platform Grexter Housing Solutions</a></h3>
                                <div class="meta-info">
                                    <ul class="clearfix">
                                        <li><a target="_blank" href="https://www.ashishdeora.com/aurum-proptech-to-acquire-53-percent-stake-in-co-living-platform-grexter-housing-solutions.php">07 Feb 2022</a></li>
                                    </ul>
                                </div>
                                <div class="more-link"><a target="_blank" href="https://www.ashishdeora.com/aurum-proptech-to-acquire-53-percent-stake-in-co-living-platform-grexter-housing-solutions.php"><span class="fa fa-arrow-right"></span></a></div>
                            </div>
                        </div>
                    </div>
                    <!--News Block-->
                    <div class="news-block col-lg-4 col-md-6 col-sm-12">
                        <div class="inner-box wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                            <div class="image-box">
                                <a target="_blank" href="https://www.ashishdeora.com/Aurum-PropTech-to-invest-$5-million-in-rental-management-platform-TheHouseMonk.php"><img src="images/News/news-2.jpg" alt="" title=""></a>
                            </div>
                            <div class="lower-box" style="height: 220px;">
                                <h3><a target="_blank" href="https://www.ashishdeora.com/Aurum-PropTech-to-invest-$5-million-in-rental-management-platform-TheHouseMonk.php">Aurum PropTech to invest $5 million in rental management platform TheHouseMonk</a></h3>
                                <div class="meta-info">
                                    <ul class="clearfix">
                                        <li><a target="_blank" href="https://www.ashishdeora.com/Aurum-PropTech-to-invest-$5-million-in-rental-management-platform-TheHouseMonk.php">19 Dec 2021</a></li>
                                    </ul>
                                </div>
                                <div class="more-link"><a target="_blank" href="https://www.ashishdeora.com/Aurum-PropTech-to-invest-$5-million-in-rental-management-platform-TheHouseMonk.php"><span class="fa fa-arrow-right"></span></a></div>
                            </div>
                        </div>
                    </div>
                    <!--News Block-->
                    <div class="news-block col-lg-4 col-md-6 col-sm-12">
                        <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <div class="image-box">
                                <a target="_blank" href="https://www.ashishdeora.com/Aurum-Pro-Tech-to-raise-Rs-343-cr-via-rights-issue.php"><img src="images/News/news-3.jpg" alt="" title=""></a>
                            </div>
                            <div class="lower-box" style="height: 220px;">
                                <h3><a target="_blank" href="https://www.ashishdeora.com/Aurum-Pro-Tech-to-raise-Rs-343-cr-via-rights-issue.php">Aurum PropTech to raise Rs 343 cr via rights issue</a></h3>
                                <div class="meta-info">
                                    <ul class="clearfix">
                                        <li><a target="_blank" href="https://www.ashishdeora.com/Aurum-Pro-Tech-to-raise-Rs-343-cr-via-rights-issue.php">18 Dec 2021</a></li>
                                    </ul>
                                </div>
                                <div class="more-link"><a target="_blank" href="https://www.ashishdeora.com/Aurum-Pro-Tech-to-raise-Rs-343-cr-via-rights-issue.php"><span class="fa fa-arrow-right"></span></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <!--Separator-->
    <div class="theme-separator"></div>

    <!--About Section-->
    <!--About Section Two-->
    <section class="about-section-two">
        <div class="pattern-layer"></div>

        <div class="auto-container">
        	<div class="row clearfix">
                <!--Left Column-->
                <div class="left-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner">
                        <figure class="image">
                            <img src="images/panoramic-view-skyscrapers-modern-cityscape-600w-1892122852.jpg" alt="" title="">
                            <div class="over-icon  wow zoomInStable" data-wow-delay="0ms" data-wow-duration="2500ms">
                                <img src="images/about-logo-image.jpg" alt="" title="">
                            </div>
                        </figure>
                        <!--<div class="sec-title">
                            <div class="upper-text" style="text-transform:uppercase;">Revolutionizing Real Estate</div>
                            <h2>Integrated PropTech Ecosystem for Capital Allocators, Creators and Consumers of Real Estate</h2>
                            <div class="lower-text">The integrated ecosystem includes a wide range of PropTech products, services, and platforms. These are categorized into 4 segments. Invest and Finance, Enterprise Efficiency, Customer Experience and Connected Living. There are more than 20 tech enabled products and services offered in these four segments.</div>
                        </div>-->
                        <div class="text-content">
                            <h3 class="upper-text" style="text-transform: capitalize;opacity: 0.6;">Revolutionizing Real Estate</h3>
                            <h2 style="font-size: 25px;    text-transform: capitalize;">Integrated PropTech Ecosystem for Capital Allocators, Creators and Consumers of Real Estate</h2>
                            <p style="margin-bottom:15px;">The integrated ecosystem includes a wide range of PropTech products, services, and platforms. These are categorized into 4 segments. Invest and Finance, Enterprise Efficiency, Customer Experience and Connected Living. There are more than 20 tech enabled products and services offered in these four segments.</p>
                        </div>

                        <!--Features-->
                        <div class="features" style="margin-top: 65px;">
                            <div class="row clearfix">
                                <!--Feature Block-->
                                <div class="feature-block col-lg-4 col-md-4 col-sm-6">
                                    <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                                        <div class="icon-box">
                                            <span class="flaticon-code"></span>
                                        </div>
                                        <div class="title" style="font-size: 17px;text-transform: uppercase;">SaaS</div>
                                    </div>
                                </div>
                                <!--Feature Block-->
                                <div class="feature-block col-lg-4 col-md-4 col-sm-6">
                                    <div class="inner-box wow fadeInLeft" data-wow-delay="150ms" data-wow-duration="1500ms">
                                        <div class="icon-box">
                                            <span class="flaticon-report"></span>
                                        </div>
                                        <div class="title" style="font-size: 17px;text-transform: uppercase;">Platform SaaS</div>
                                    </div>
                                </div>
                                <!--Feature Block-->
                                <div class="feature-block col-lg-4 col-md-4 col-sm-6">
                                    <div class="inner-box wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                                        <div class="icon-box">
                                            <span class="flaticon-consult"></span>
                                        </div>
                                        <div class="title" style="font-size: 17px;text-transform: uppercase;">Raas</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <!--Right Column-->
                <div class="right-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner">
                        <figure class="image">
                            <img src="images/featured-image-3 (1).jpg" alt="" title="">
                        </figure>
                        <div class="text-content">
                            <h3 class="upper-text" style="text-transform: capitalize;opacity: 0.6;">Company Mission And Vision</h3>
                            <p>We aim to create a tech driven revolution in Real Estate with an objective of enabling data driven investments, increasing enterprise efficiency, enhancing consumer experience and enabling connected living.</p>
                            
                            <ul class="list-style-one">
                                <li>Data science and analytics tools and platforms to enable smart investment decisions for institutional and retail investors.</li>
                                <li>Product suites and platforms to help Real Estate developers and firms increase efficiency of business.</li>
                                <li>Solutions that leverage technology and elevate the purchase experience of Real Estate consumers.</li>
                                <li>Technology solutions that create lifestyle experience around real estate spaces and asset management products that preserve and enhance the value of real estate.</li>
                            </ul>
                        </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

    <!--Separator-->
    <div class="theme-separator"></div>

    <!--Contact Section-->
    <section class="contact-section-two">
        <div class="auto-container">
            <div class="upper-row">
                <div class="row clearfix">
                    <!--Text Column-->
                    <div class="text-column col-lg-6 col-md-12 col-sm-12">
                        <div class="inner">
                            <div class="sec-title">
                                <div class="upper-text" style="text-transform:capitalize;">Send us a Message</div>
                                <h2 style="font-size:32px;">Do You Have Any Questions? We’ll Be Happy To Assist!</h2>
                                <div class="lower-text">Dolor sit amet, consectetur adipisicing elitm sed do eiusmod ut labore etsu dolore magna aliquatenim minim.</div>
                            </div>
                            <!--
                            <div class="social-links">
                                <ul class="clearfix">
                                    <li><a href="https://www.facebook.com/AurumPropTech"><span class="fab fa-facebook-square"></span></a></li>
                                    <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                                    <li><a href="#"><span class="fab fa-instagram"></span></a></li>
                                    <li><a href="#"><span class="fab fa-youtube"></span></a></li>
                                    <li><a href="#"><span class="fab fa-pinterest"></span></a></li>
                                </ul>
                            </div>-->
                        </div>
                    </div>
                    <!--Form Column-->
                    <div class="form-column contact-form-box col-lg-6 col-md-12 col-sm-12">
                        <div class="inner">
                            <!--Form Box-->
                            <div class="form-box">
                                <div class="default-form contact-form">
                                    <form method="post" action="" id="contact-form">
                                        <div class="row clearfix">                                    
                                            <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                                <input type="text" name="username" placeholder="Your Name" required="" value="">
                                            </div>
                                            
                                            <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                                <input type="email" name="email" placeholder="Email" required="" value="">
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                                <input type="text" name="phone" placeholder="Phone" required="" value="">
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                                <input type="text" name="subject" placeholder="Subject" required="" value="">
                                            </div>

                                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                                <textarea name="message" placeholder="Message" required=""></textarea>
                                            </div>
                    
                                            <div class="form-group col-md-12 col-sm-12">
                                                <button type="submit" class="theme-btn btn-style-one"><span class="btn-title">Make a Request</span></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    
    <!--Separator-->
    <div class="theme-separator"></div>

    <!--Sponsors Section-->
    <section class="sponsors-section">
        <div class="sponsors-outer">
            <!--Sponsors-->
            <div class="auto-container">
                <!--Sponsors Carousel-->
                <div class="sponsors-carousel theme-carousel owl-theme owl-carousel" data-options='{"loop": true, "margin": 30, "autoheight":true, "lazyload":true, "nav": true, "dots": true, "autoplay": true, "autoplayTimeout": 6000, "smartSpeed": 500, "responsive":{ "0" :{ "items": "1" }, "600" :{ "items" : "2" }, "768" :{ "items" : "3" } , "800":{ "items" : "3" }, "1024":{ "items" : "4" }, "1200":{ "items" : "5" }}}'>
                    <div class="slide-item"><figure class="image-box"><a href="http://www.integrowamc.com/" target="_blank"><img src="images/clients/3.png" alt=""></a></figure></div>
                    <div class="slide-item"><figure class="image-box"><a href="https://www.sell.do/" target="_blank"><img src="images/clients/4.png" alt=""></a></figure></div>
                    <div class="slide-item"><figure class="image-box"><a href="https://thehousemonk.com/" target="_blank"><img src="images/clients/6.png" alt=""></a></figure></div>
                  
<div class="slide-item"><figure class="image-box"><a href="https://grexter.in/" target="_blank"><img style="margin-top: 39px;width:80%" src="images/logo-blue.png" alt=""></a></figure></div>
  
                </div>
            </div>
        </div>
    </section>
<?php
include "footer.php";
?>