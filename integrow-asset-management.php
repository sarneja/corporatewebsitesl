<?php
include "header.php";
?>
 <!-- Inner Banner Section -->
 <section class="inner-banner alternate">
        <div class="banner-curve"></div>
		<div class="auto-container">
            <div class="inner">
                <div class="theme-icon"></div>
    			<div class="title-box">
                    <h1>Integrow Asset Management Pvt. Ltd.</h1>
                    
                </div>
            </div>
		</div>
    </section>
    <!--End Banner Section -->

    <div class="sidebar-page-container services-page">
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                    <div class="services-content">
                        <div class="service-details">
                            <!--
                            <div class="image-box">
                                <img src="images/resource/blog-image-16.jpg" alt="">
                            </div>-->
                            <!--content-->
                            <div class="content">
                                <h3>Integrow Asset Management Pvt. Ltd.</h3>
                                <div class="text">
                                    <p>Integrow is a Real Estate focused Asset Management firm focusing on transformation through shared insights, effective processes and causing value enhancements for its stakeholders. Integrow is attempting to bridge the vision of sustainable democratization of Real Estate with confidence, energy, and integrity. The team is led by Ramashray Yadav, a seasoned Real Estate industry veteran.</p>
                                    <p><strong>FOR MORE INFO: </strong><u><a href="http://www.integrowamc.com/" target="blank">CLICK HERE</a></u></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
                    <aside class="sidebar services-sidebar">
                        <!--Services Widget-->
                        <div class="sidebar-widget services-widget">
                            <div class="widget-inner">
                                <ul>
                                    <li><a href="aurum-software-solutions-private-limited.php">Aurum Software Solutions Private Limited </a></li>
                                    <li><a href="aurum-realtech-services-private-limited.php">Aurum RealTech Services Private Limited </a></li>
                                    <li><a href="k2v2-technologies.php">K2V2 Technologies</a></li>
                                    <li><a href="monktech-labs.php">MonkTech Labs Pte. Ltd.</a></li>
                                    <li class="active"><a href="integrow-asset-management.php">Integrow Asset Management Pvt. Ltd.</a></li>
                                </ul>
                            </div>
                        </div>
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
<?php
include "footer.php";
?>