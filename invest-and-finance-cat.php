<?php
include "header.php";
?>
<!-- Inner Banner Section -->
<section class="inner-banner">
        <div class="banner-curve"></div>
		<div class="auto-container">
            <div class="inner">
                <div class="theme-icon"></div>
    			<div class="title-box">
                    <h1>Invest and Finance</h1>
                    
                </div>
            </div>
		</div>
</section>
    <!--End Banner Section -->

	<!--News Section-->
    <section class="news-section blog-grid">
        <div class="auto-container">
            <div class="row clearfix">
                <!--News Block-->
                <div class="news-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <a href="data-science-and-analytics"><img src="images/datascience.jpg" alt="" title=""></a>
                        </div>
                        <div class="lower-box">
                            <!--<div class="category">IT Projects</div>-->
                            <h3><a href="data-science-and-analytics">Data Science and Analytics</a></h3>
                            <!--
                            <div class="meta-info">
                                <ul class="clearfix">
                                    <li><a href="#">By Admin</a></li>
                                    <li><a href="#">24 June 2019</a></li>
                                </ul>
                            </div>-->
                            <div class="more-link"><a href="data-science-and-analytics"><span class="fa fa-arrow-right"></span></a></div>
                        </div>
                    </div>
                </div>
                <!--News Block-->
                <div class="news-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <a href="digital-lending"><img src="images/businesspeople-working-finance-accounting-analyze-financi_74952-1411.jpg" alt="" title=""></a>
                        </div>
                        <div class="lower-box">
                            <!--<div class="category">IT Projects</div>-->
                            <h3><a href="digital-lending">Digital Lending</a></h3>
                            <!--
                            <div class="meta-info">
                                <ul class="clearfix">
                                    <li><a href="#">By Admin</a></li>
                                    <li><a href="#">24 June 2019</a></li>
                                </ul>
                            </div>-->
                            <div class="more-link"><a href="digital-lending"><span class="fa fa-arrow-right"></span></a></div>
                        </div>
                    </div>
                </div>
                <!--News Block-->
                <div class="news-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <a href="fractional-ownership"><img src="images/business-man-create-design-modern-building-real-estate_35761-316.jpg" alt="" title=""></a>
                        </div>
                        <div class="lower-box">
                            <!--<div class="category">IT Projects</div>-->
                            <h3><a href="fractional-ownership">Fractional Ownership</a></h3>
                            <!--
                            <div class="meta-info">
                                <ul class="clearfix">
                                    <li><a href="#">By Admin</a></li>
                                    <li><a href="#">24 June 2019</a></li>
                                </ul>
                            </div>-->
                            <div class="more-link"><a href="fractional-ownership"><span class="fa fa-arrow-right"></span></a></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--
            <div class="load-more link-box">
                <a href="blog-fullwidth.html" class="theme-btn btn-style-two"><div class="btn-title">Load More News</div></a>
            </div>-->

        </div>
    </section>

<?php
include "footer.php";
?>