$(document).ready(function() { 
    var cookie = false;
    var nocookie = false;
    var cookieContent = $('.cookie-disclaimer');
    //document.cookie = "nocookie=not_accepted;"

    checkCookie();

    if (cookie === true) {
      cookieContent.hide();
    }else if(nocookie === true) {
      cookieContent.hide();
    }


    function setCookie(cname, cvalue, exdays) {
      var expires = "";
      if(exdays != 0) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        expires = "expires=" + d.toGMTString();
      }
      
      document.cookie = cname + "=" + cvalue + "; " + expires;
    }

    function getCookie(cname) {
      var name = cname + "=";
      var ca = document.cookie.split(';');
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) === 0) return c.substring(name.length, c.length);
      }
      return "";
    }

    function checkCookie() {
      var check = getCookie("acookie");
      if (check !== "") {
        return cookie = true;
      } else {
        var check_nocookie = getCookie("nocookie");
        if (check_nocookie !== "") {
          return nocookie = true;
        }
        nocookie = false;
        return cookie = false; //setCookie("acookie", "accepted", 365);
      }
      
    }
    $('.accept-cookie').click(function () {
      setCookie("acookie", "accepted", 365);
      cookieContent.hide(500);
    });

    $('.no-accept-cookie').click(function () {
      setCookie("nocookie", "not_accepted", 0);
      cookieContent.hide(500);
    });
});