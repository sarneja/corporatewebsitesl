(function($) {
	
	"use strict";
	
	//Hide Loading Box (Preloader)
	function handlePreloader() {
		if($('.preloader').length){
			$('body').addClass('page-loaded');
			$('.preloader').delay(1000).fadeOut(0);
		}
	}

	//Update Header Style and Scroll to Top
	function headerStyle() {
		if($('.main-header').length){
			var windowpos = $(window).scrollTop();
			var siteHeader = $('.main-header');
			var scrollLink = $('.scroll-to-top');
			var sticky_header = $('.main-header .sticky-header');
			if (windowpos > 180) {
				siteHeader.addClass('fixed-header');
				sticky_header.addClass("animated slideInDown");
				scrollLink.fadeIn(300);
			} else {
				siteHeader.removeClass('fixed-header');
				sticky_header.removeClass("animated slideInDown");
				scrollLink.fadeOut(300);
			}
		}
	}
	
	headerStyle();

	//Submenu Dropdown Toggle
	if($('.main-header li.dropdown ul').length){
		$('.main-header .navigation li.dropdown').append('<div class="dropdown-btn"><span class="fa fa-angle-right"></span></div>');
		
	}

	//Mobile Nav Hide Show
	if($('.mobile-menu').length){
		
		$('.mobile-menu .menu-box').mCustomScrollbar();
		
		var mobileMenuContent = $('.main-header .nav-outer .main-menu').html();
		$('.mobile-menu .menu-box .menu-outer').append(mobileMenuContent);
		$('.sticky-header .main-menu').append(mobileMenuContent);
		
		//Dropdown Button
		$('.mobile-menu li.dropdown .dropdown-btn').on('click', function() {
			$(this).toggleClass('open');
			$(this).prev('ul').slideToggle(500);
		});
		//Menu Toggle Btn
		$('.mobile-nav-toggler').on('click', function() {
			$('body').addClass('mobile-menu-visible');
		});

		//Menu Toggle Btn
		$('.mobile-menu .menu-backdrop,.mobile-menu .close-btn').on('click', function() {
			$('body').removeClass('mobile-menu-visible');
		});
	}

	//Search Popup
	if($('#search-popup').length){
		
		//Show Popup
		$('.search-toggler').on('click', function() {
			$('#search-popup').addClass('popup-visible');
			$('body').addClass('search-visible');
		});
		$(document).keydown(function(e){
	        if(e.keyCode === 27) {
	            $('#search-popup').removeClass('popup-visible');
	            $('body').removeClass('search-visible');
	        }
	    });
		//Hide Popup
		$('.close-search,.search-popup .overlay-layer').on('click', function() {
			$('#search-popup').removeClass('popup-visible');
			$('body').removeClass('search-visible');
		});
	}
	
	/////////////////////////////
		//Universal Code for All Owl Carousel Sliders
	/////////////////////////////
	
	if ($('.theme-carousel').length) {
			$(".theme-carousel").each(function (index) {
			var $owlAttr = {navText: [ '<span class="icon fa fa-arrow-left"></span>', '<span class="icon fa fa-arrow-right"></span>' ]},
			$extraAttr = $(this).data("options");
			$.extend($owlAttr, $extraAttr);
			$(this).owlCarousel($owlAttr);
		});
	}
	
	//Fact Counter + Text Count
	if($('.count-box').length){
		$('.count-box').appear(function(){
	
			var $t = $(this),
				n = $t.find(".count-text").attr("data-stop"),
				r = parseInt($t.find(".count-text").attr("data-speed"), 10);
				
			if (!$t.hasClass("counted")) {
				$t.addClass("counted");
				$({
					countNum: $t.find(".count-text").text()
				}).animate({
					countNum: n
				}, {
					duration: r,
					easing: "linear",
					step: function() {
						$t.find(".count-text").text(Math.floor(this.countNum));
					},
					complete: function() {
						$t.find(".count-text").text(this.countNum);
					}
				});
			}
			
		},{accY: 0});
	}
	 
	
	//Tabs Box
	if($('.tabs-box').length){
		$('.tabs-box .tab-buttons .tab-btn').on('click', function(e) {
			e.preventDefault();
			var target = $($(this).attr('data-tab'));
			
			if ($(target).is(':visible')){
				return false;
			}else{
				target.parents('.tabs-box').find('.tab-buttons').find('.tab-btn').removeClass('active-btn');
				$(this).addClass('active-btn');
				target.parents('.tabs-box').find('.tabs-content').find('.tab').fadeOut(0);
				target.parents('.tabs-box').find('.tabs-content').find('.tab').removeClass('active-tab');
				$(target).fadeIn(300);
				$(target).addClass('active-tab');
			}
		});
	}
	
	//Accordion Box
	if($('.accordion-box').length){
		$(".accordion-box").on('click', '.acc-btn', function() {
			
			var outerBox = $(this).parents('.accordion-box');
			var target = $(this).parents('.accordion');
			
			if ($(this).next('.acc-content').is(':visible')){
				//return false;
				$(this).removeClass('active');
				$(this).next('.acc-content').slideUp(300);
				$(outerBox).children('.accordion').removeClass('active-block');
			}else{
				$(outerBox).find('.accordion .acc-btn').removeClass('active');
				$(this).addClass('active');
				$(outerBox).children('.accordion').removeClass('active-block');
				$(outerBox).find('.accordion').children('.acc-content').slideUp(300);
				target.addClass('active-block');
				$(this).next('.acc-content').slideDown(300);	
			}
		});	
	}
	
	//Custom Seclect Box
	if($('.custom-select-box').length){
		$('.custom-select-box').selectmenu().selectmenu('menuWidget').addClass('overflow');
	}

	//LightBox / Fancybox
	if($('.lightbox-image').length) {
		$('.lightbox-image').fancybox({
			openEffect  : 'fade',
			closeEffect : 'fade',
			helpers : {
				media : {}
			}
		});
	}
	
	//Contact Form Validation
	// if($('#contact-form').length){
	// 	$('#contact-form').validate({
	// 		rules: {
	// 			username: {
	// 				required: true
	// 			},
	// 			email: {
	// 				required: true,
	// 				email: true
	// 			},
	// 			phone: {
	// 				required: true
	// 			},
	// 			subject: {
	// 				required: true
	// 			},
	// 			message: {
	// 				required: true
	// 			}
	// 		}
	// 	});
	// }

	function isNumber(evt) {
		evt = (evt) ? evt : window.event;
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
		return true;
	}

	$("#phone").keypress(function(e){
		if($("#phone").val().length < 10) {
			return true;
		}else{
			e.preventDefault();
		}
	})

	
	$(".contact-submit").click(function() {
		// $("#contact-form").validate();
		if($("#contact-form").valid()) {
			$(".form-loading").show().css("display","flex");
			var data = $("#contact-form").serialize();
			$.ajax({
				url: "ajax-action.php",
				method: "post",
				dataType: "json",
				data: {
					data: data, contact:1
				},
				success: function(res) {
					if(res.status) {
						Swal.fire({
							icon: 'success',
							text: 'Thank you for contacting us, Our representative will contact you shortly.',
						})
						$('#contact-form').trigger('reset');
					}else{
						Swal.fire({
							icon: 'error',
							text: 'Something went wrong, please try again!!!',
						})
					}
				},
				error: function (jqXHR, exception) {
					Swal.fire({
						icon: 'error',
						text: 'Something went wrong, please try again!!!',
					}) 
					$('#contact-form').trigger('reset');
				}
			})
			$(".form-loading").hide();
		}
		return false;
	});

	

	function IsEmail(email) {
		var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(!regex.test(email)) {
		  	return true;
		}else{
			return false;
		}
	}

	$("#newsletter-submit").click(function() {
		var error = false;
		if($("#news-email").val() == "") {
			error= true;
			$("#news-email").addClass("error");
			var msg="This Field is required";
		}else{
			if(IsEmail($("#news-email").val().trim())) {
				error= true;
				$("#news-email").addClass("error");
				var msg="Invalid Email";
			}else{
				$("#news-email").removeClass("error"); $("#news-email").next("span").remove();
			}
		}
		if(error) {
			$(".error").next("span").remove();
			$(".error").after("<span style='color:white;'>"+msg+"</span>");
			return false;
		}
		$(".form-loading").show().css("display","flex");
		$.ajax({
			url: "ajax-action.php",
			method: "post",
			dataType: "json",
			data: {
				email: $("#news-email").val().trim(), footer_newsletter:1
			},
			success: function(res) {
				if(res.status) {
					Swal.fire({
						icon: 'success',
						text: res.msg,
					})
					$("#news-email").val("");
				}else{
					Swal.fire({
						icon: 'error',
						text: res.msg,
					})
				}
			},
			error: function (jqXHR, exception) {
				Swal.fire({
					icon: 'error',
					text: 'Something went wrong!!!',
				}) 
				$('#news-email').trigger('reset');
			}
		})
		$(".form-loading").hide();
		return false;
	});
	
	// Scroll to a Specific Div
	if($('.scroll-to-target').length){
		$(".scroll-to-target").on('click', function() {
			var target = $(this).attr('data-target');
		   // animate
		   $('html, body').animate({
			   scrollTop: $(target).offset().top
			 }, 1500);
	
		});
	}
	
	// Elements Animation
	if($('.wow').length){
		var wow = new WOW(
		  {
			boxClass:     'wow',      // animated element css class (default is wow)
			animateClass: 'animated', // animation css class (default is animated)
			offset:       0,          // distance to the element when triggering the animation (default is 0)
			mobile:       false,       // trigger animations on mobile devices (default is true)
			live:         true       // act on asynchronously loaded content (default is true)
		  }
		);
		wow.init();
	}


/* ==========================================================================
   When document is Scrollig, do
   ========================================================================== */
	
	$(window).on('scroll', function() {
		headerStyle();
	});

/* ==========================================================================
   When document is Resized, do
   ========================================================================== */
	
	$(window).on('resize', function() {
		
	});	

/* ==========================================================================
   When document is loading, do
   ========================================================================== */
	
	$(window).on('load', function() {
		handlePreloader();
	});	


	// Script for top Notification bar
	$( '.notification-bar i' ).click(function() {
		$( '.notification-bar' ).slideUp();
	});

})(window.jQuery);