<?php
include "header.php";
?>
 <!-- Inner Banner Section -->
 <section class="inner-banner alternate">
        <div class="banner-curve"></div>
		<div class="auto-container">
            <div class="inner">
                <div class="theme-icon"></div>
    			<div class="title-box">
                    <h1>K2V2 Technologies</h1>
                    
                </div>
            </div>
		</div>
    </section>
    <!--End Banner Section -->

    <div class="sidebar-page-container services-page">
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                    <div class="services-content">
                        <div class="service-details">
                            <!--content-->
                            <div class="content">
                                <h3>K2V2 Technologies</h3>
                                <div class="text">
                                    <p>K2V2 technologies is engaged in the business of Sales, Marketing, Automation and CRM thereby providing an integrated solution for effective management of real estate Sales, Marketing and CRM engine. It has an industry leading Real Estate CRM software Sell.do which is deployed across various Real Estate Developer across the country. It also has a broker aggregation and Real Estate product launch platform Beyond Walls. K2V2 also has a strong in house digital marketing practice specifically catering Real Estate. K2V2 has a 200 people strong team  led by Ketan Sabnis and Vinayak Katkar. The company is ISO 27001 compliant company and a winner of multiple PropTech awards.</p>
                                    <p><strong>FOR MORE INFO: </strong><u><a href="https://www.sell.do/" target="_blank">CLICK HERE</a></u></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
                    <aside class="sidebar services-sidebar">
                        <!--Services Widget-->
                        <div class="sidebar-widget services-widget">
                            <div class="widget-inner">
                                <ul>
                                    <li><a href="aurum-software-solutions-private-limited">Aurum Software Solutions Private Limited </a></li>
                                    <li><a href="aurum-realtech-services-private-limited">Aurum RealTech Services Private Limited </a></li>
                                    <li class="active"><a href="k2v2-technologies">K2V2 Technologies</a></li>
                                    <li><a href="monktech-labs">MonkTech Labs Pte. Ltd.</a></li>
                                    <li><a href="integrow-asset-management">Integrow Asset Management Pvt. Ltd.</a></li>
                                </ul>
                            </div>
                        </div>
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
<?php
include "footer.php";
?>