<?php
include "header.php";
?>
 <!-- Inner Banner Section -->
 <section class="inner-banner alternate">
  <div class="banner-curve"></div>
  <div class="auto-container">
    <div class="inner">
      <div class="theme-icon"></div>
      <div class="title-box">
        <h1>Key Managerial Personnel</h1>
      </div>
    </div>
  </div>
</section>
        
<!--End Banner Section -->
<link href="css/member-style.css" rel="stylesheet">


	<!--Team Section-->

<section id="team" class="team section-bg">
      <div class="container" data-aos="fade-up">

        <div class="row">

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div class="member text-center" data-aos="fade-up" data-aos-delay="100">
              <div class="member-img">
                <img src="images/key-managerial-personnel/Onkar-Shetye.jpg" class="img-fluid" alt="">
                <div class="social">
                  <a href=""><i class="fab fa-twitter"></i></a>
                  <a href=""><i class="fab fa-linkedin"></i></a>
                </div>
              </div>
              <a href="" data-toggle="modal" data-target="#myModal1"><div class="member-info">
                <h4>Onkar Shetye</h4>
                <span>Executive Director</span>
              </div></a>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div class="member text-center" data-aos="fade-up" data-aos-delay="100">
              <div class="member-img">
                <img src="images/key-managerial-personnel/Kunal.jpg" class="img-fluid" alt="" style="height:297px;">
                <div class="social">
                  <a href=""><i class="fab fa-twitter"></i></a>
                  <a href=""><i class="fab fa-linkedin"></i></a>
                </div>
              </div>
              <a  href="" data-toggle="modal" data-target="#myModal3"><div class="member-info">
                <h4>Kunal Karan</h4>
                <span>Chief Financial Officer</span>
              </div></a>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div class="member text-center" data-aos="fade-up" data-aos-delay="100">
              <div class="member-img">
                <img src="images/key-managerial-personnel/Khushbu_profile.jpg" class="img-fluid" alt="">
                <div class="social">
                  <a href=""><i class="fab fa-twitter"></i></a>
                  <a href=""><i class="fab fa-linkedin"></i></a>
                </div>
              </div>
              <a  href="" data-toggle="modal" data-target="#myModal2"><div class="member-info">
                <h4>Khushbu Rakhecha</h4>
                <span>Compliance Officer</span>
              </div></a>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div class="member text-center" data-aos="fade-up" data-aos-delay="100">
              <div class="member-img">
                <img src="images/key-managerial-personnel/neha.jpg" class="img-fluid" alt="" style="height:297px;">
                <div class="social">
                  <a href=""><i class="fab fa-twitter"></i></a>
                  <a href=""><i class="fab fa-linkedin"></i></a>
                </div>
              </div>
              <a  href="" data-toggle="modal" data-target="#myModal4"><div class="member-info">
                <h4>Neha Sangam</h4>
                <span>Company Secretary</span>
              </div></a>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Team Section -->

<?php
include "footer.php";
?>
<!----Modal Popup---->
<div class="container">
        <!-- Modal1 -->
        <div class="modal fade" id="myModal1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="bio-head">
                            <h3><strong>Onkar Shetye</strong></h3>
                            <p>Executive Director</p>
                            <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                        </div>    
                        <div class="bio-content">
                            <img src="images/key-managerial-personnel/Onkar-Shetye.jpg" class="member-img"></img>
                            <div class="bio-text">
                                <p>Onkar has 15 years of multisectoral experience and has driven strategic and transformational initiatives at multiple organizations across industries like Energy, Real Estate, Mineral Exploration, and Information Technology. He has worked with diverse teams across India, Europe, and Africa.</p>

                                <p>Onkar brings a large toolbox to the table and works with teams to validate, catalyze, and scale new ventures by refining competitive dynamics, honing their business plans and refining go to market strategies.</p>
                                <p>He has successfully managed multifunctional teams reporting to him and done Project Management for On Time in Time implementation.</p>

                                <p>Onkar has a Bachelor’s degree in Science from University of Mumbai and a Master’s degree from the prestigious Russel Group Universities, UK.</p>


                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>

        <!-- Modal2 -->
        <div class="modal fade" id="myModal2" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="bio-head">
                            <h3><strong>Khushbu Rakhecha</strong></h3>
                            <p>Compliance Officer</p>
                            <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                        </div>    
                        <div class="bio-content">
                        <img src="images/key-managerial-personnel/Khushbu_profile.jpg" class="member-img"></img>
                        <div class="bio-text">    
                        <p>Khushbu Rakhecha is a Compliance Officer at Aurum PropTech. She is also an associate member at the Institute of Company Secretaries of India with over three years of experience in the real estate industry.  
                            </p></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal3 -->
        <div class="modal fade" id="myModal3" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="bio-head">
                            <h3><strong>Kunal Karan</strong></h3>
                            <p>Chief Financial Officer</p>
                            <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                        </div>    
                        <div class="bio-content">
                            <img src="images/key-managerial-personnel/Kunal.jpg" class="member-img"></img>
                            <div class="bio-text">
                                <p>Kunal Karan is the Chief Financial Officer (CFO) at Aurum PropTech. A CA by profession, Mr. Kunal has over two decades of industry experience, and he has served prestigious organizations like Reliance Communications and Mastek Ltd. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal4 -->
        <div class="modal fade" id="myModal4" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="bio-head">
                            <h3><strong>Neha Sangam</strong></h3>
                            <p>Company Secretary</p>
                            <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                        </div>    
                        <div class="bio-content">
                            <img src="images/key-managerial-personnel/neha.jpg" class="member-img"></img>
                            <div class="bio-text">
                                <p>Ms. Neha Sangam is a Company Secretary at Aurum PropTech. She is an associate member of ICSAI, and she also carries a Master’s Degree in Commerce. Ms. Neha has over eight years of industry experience. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>