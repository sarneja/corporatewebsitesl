<?php
include "header.php";
?>
 <!-- Inner Banner Section -->
 <section class="inner-banner alternate">
  <div class="banner-curve"></div>
  <div class="auto-container">
    <div class="inner">
      <div class="theme-icon"></div>
      <div class="title-box">
        <h1>Key Managerial Personnel</h1>
      </div>
    </div>
  </div>
</section>
        
<!--End Banner Section -->
<!--<link href="css/member-style.css" rel="stylesheet">-->

<section style="margin-top: 70px;">
	<!--Team Section-->
  <div id="rs-team" class="rs-team fullwidth-team pt-100 pb-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="team-item">
                    <div class="team-img">
                        <img src="images/key-managerial-personnel/Onkar-Shetye.jpg" alt="team Image">
                        <div class="normal-text">
                            <h4 class="team-name"><strong>Onkar Shetye</strong></h4>
                            <span class="subtitle">Executive Director</span>
                        </div>
                    </div>
                    <div class="team-content">
                        <div class="display-table">
                            <div class="display-table-cell">
                                <div class="share-icons">
                                    <ul class="team-social icons-1">
                                        <li><a href="https://www.linkedin.com/in/onkarshetye/" target="_blank" class="social-icon"><i class="fab fa-linkedin"></i></a>
                                        </li>
                                        <li><a target="_blank" href="https://twitter.com/onkar_shetye" class="social-icon"><i class="fab fa-twitter"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="team-details" data-toggle="modal" data-target="#myModal1">
                                    <h4 class="team-name">
                                        <a href="" data-toggle="modal" data-target="#myModal1"><strong>Onkar Shetye</strong></a>
                                    </h4>
                                    <span class="postion">Executive Director</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6">
                <div class="team-item">
                    <div class="team-img">
                        <img src="images/key-managerial-personnel/Kunal.jpg" alt="team Image" style="height: 404px;">
                        <div class="normal-text">
                            <h4 class="team-name"><strong>Kunal Karan</strong></h4>
                            <span class="subtitle">Chief Financial Officer</span>
                        </div>
                    </div>
                    <div class="team-content">
                        <div class="display-table">
                            <div class="display-table-cell">
                                <div class="share-icons">
                                    
                                    <ul class="team-social icons-1">
                                        <li><a target="_blank" href="https://www.linkedin.com/in/kunal-karan-1ab23b74/" class="social-icon"><i class="fab fa-linkedin"></i></a>
                                        </li>
                                        <!-- <li><a target="_blank" href="#" class="social-icon"><i class="fab fa-twitter"></i></a>
                                        </li> -->
                                    </ul>
                                </div>
                                <div class="team-details" data-toggle="modal" data-target="#myModal3">
                                    <h4 class="team-name">
                                        <a href="" data-toggle="modal" data-target="#myModal3"><strong>Kunal Karan</strong></a>
                                    </h4>
                                    <span class="postion">Chief Financial Officer</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6">
                <div class="team-item">
                    <div class="team-img">
                        <img src="images/key-managerial-personnel/Khushbu_profile.jpg" alt="team Image" style="height:404px;">
                        <div class="normal-text">
                            <h4 class="team-name"><strong>Khushbu Rakhecha</strong></h4>
                            <span class="subtitle">Compliance Officer</span>
                        </div>
                    </div>
                    <div class="team-content">
                        <div class="display-table">
                            <div class="display-table-cell">
                                <div class="share-icons">
                                    
                                    <ul class="team-social icons-1">
                                        <li><a target="_blank" href="https://www.linkedin.com/in/khushburakhecha/" class="social-icon"><i class="fab fa-linkedin"></i></a>
                                        </li>
                                        <!-- <li><a target="_blank" href="#" class="social-icon"><i class="fab fa-twitter"></i></a>
                                        </li> -->
                                    </ul>
                                </div>
                                <div class="team-details" data-toggle="modal" data-target="#myModal2">
                                    <h4 class="team-name">
                                        <a href="" data-toggle="modal" data-target="#myModal2"><strong>Khushbu Rakhecha</a></strong>
                                    </h4>
                                    <span class="postion">Compliance Officer</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-4 col-md-6">
                <div class="team-item">
                    <div class="team-img">
                        <img src="images/key-managerial-personnel/neha.jpg" alt="team Image">
                        <div class="normal-text">
                            <h4 class="team-name"><strong>Neha Sangam</strong></h4>
                            <span class="subtitle">Company Secretary</span>
                        </div>
                    </div>
                    <div class="team-content">
                        <div class="display-table">
                            <div class="display-table-cell">
                                <div class="share-icons">
                                    
                                    <ul class="team-social icons-1">
                                        <li><a target="_blank" href="https://www.linkedin.com/in/cs-neha-sangam-232947a7/" class="social-icon"><i class="fab fa-linkedin"></i></a>
                                        </li>
                                        <li><a target="_blank" href="https://twitter.com/Nehasangam2" class="social-icon"><i class="fab fa-twitter"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="team-details" data-toggle="modal" data-target="#myModal4">
                                    <h4 class="team-name">
                                        <a href="" data-toggle="modal" data-target="#myModal4"><strong>Neha Sangam</a></strong>
                                    </h4>
                                    <span class="postion">Company Secretary</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .container-fullwidth -->
</div>
</section>

<?php
include "footer.php";
?>
<!----Modal Popup---->
<div class="container">
        <!-- Modal1 -->
        <div class="modal fade" id="myModal1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="bio-head">
                            <h3><strong>Onkar Shetye</strong></h3>
                            <p>Executive Director</p>
                            <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                        </div>    
                        <div class="bio-content">
                            <img src="images/key-managerial-personnel/Onkar-Shetye.jpg" class="member-img"></img>
                            <div class="bio-text">
                                <p>Onkar has 15 years of multisectoral experience and has driven strategic and transformational initiatives at multiple organizations across industries like Energy, Real Estate, Mineral Exploration, and Information Technology. He has worked with diverse teams across India, Europe, and Africa.</p>

                                <p>Onkar brings a large toolbox to the table and works with teams to validate, catalyze, and scale new ventures by refining competitive dynamics, honing their business plans and refining go to market strategies.</p>
                                <p>He has successfully managed multifunctional teams reporting to him and done Project Management for On Time in Time implementation.</p>

                                <p>Onkar has a Bachelor’s degree in Science from University of Mumbai and a Master’s degree from the prestigious Russel Group Universities, UK.</p>


                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>

        <!-- Modal2 -->
        <div class="modal fade" id="myModal2" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="bio-head">
                            <h3><strong>Khushbu Rakhecha</strong></h3>
                            <p>Compliance Officer</p>
                            <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                        </div>    
                        <div class="bio-content">
                        <img src="images/key-managerial-personnel/Khushbu_profile.jpg" class="member-img"></img>
                        <div class="bio-text">    
                        <p>Khushbu Rakhecha is a Compliance Officer at Aurum PropTech. She is also an associate member at the Institute of Company Secretaries of India with over three years of experience in the real estate industry.  
                            </p></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal3 -->
        <div class="modal fade" id="myModal3" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="bio-head">
                            <h3><strong>Kunal Karan</strong></h3>
                            <p>Chief Financial Officer</p>
                            <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                        </div>    
                        <div class="bio-content">
                            <img src="images/key-managerial-personnel/Kunal.jpg" class="member-img"></img>
                            <div class="bio-text">
                                <p>Kunal Karan is the Chief Financial Officer (CFO) at Aurum PropTech. A CA by profession, Mr. Kunal has over two decades of industry experience, and he has served prestigious organizations like Reliance Communications and Mastek Ltd. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal4 -->
        <div class="modal fade" id="myModal4" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="bio-head">
                            <h3><strong>Neha Sangam</strong></h3>
                            <p>Company Secretary</p>
                            <i class="fa fa-times-circle bio-close" data-dismiss="modal"></i>
                        </div>    
                        <div class="bio-content">
                            <img src="images/key-managerial-personnel/neha.jpg" class="member-img"></img>
                            <div class="bio-text">
                                <p>Ms. Neha Sangam is a Company Secretary at Aurum PropTech. She is an associate member of ICSAI, and she also carries a Master’s Degree in Commerce. Ms. Neha has over eight years of industry experience. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>