<?php
include "header.php";
?>
 <!-- Inner Banner Section -->
 <section class="inner-banner alternate">
        <div class="banner-curve"></div>
		<div class="auto-container">
            <div class="inner">
                <div class="theme-icon"></div>
    			<div class="title-box">
                    <h1>MonkTech Labs Pte. Ltd.</h1>
                    
                </div>
            </div>
		</div>
    </section>
    <!--End Banner Section -->

    <div class="sidebar-page-container services-page">
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                    <div class="services-content">
                        <div class="service-details">
                            <!--
                            <div class="image-box">
                                <img src="images/resource/blog-image-16.jpg" alt="">
                            </div>-->
                            <!--content-->
                            <div class="content">
                                <h3>MonkTech Labs Pte. Ltd.</h3>
                                <div class="text">
                                    <p>MonkTech Labs Pte. Ltd is a SaaS based digital service provider for the growth and management of rental real estate. Clients include residential rental managers, real estate developers, property and facility managers, co-living providers, and many more. The products offered by the Company intend to provide the following facilities: Property Management System for Portfolio Management, Operations Management Tools and Platform for Elevating Tenant Experience. It has a flag ship product TheHouseMonk, is a rental management software providing property management software that enables property managers and owners to grow & manage all their  rental portfolios while delivering an amazing tenant experience. It has two modules RE:Core which helps drive profitability, manage rental portfolio efficiently and RE:Xp which helps build a rental property portal for tenants to find, book , live in properties and experience all the services within a rented premise. The team is led by Ajay Kumar and Balaji Vardharajan.</p>
                                    <p><strong>FOR MORE INFO: </strong><u><a href="https://monktechlabs.com/" target="blank">CLICK HERE</a></u></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
                    <aside class="sidebar services-sidebar">
                        <!--Services Widget-->
                        <div class="sidebar-widget services-widget">
                            <div class="widget-inner">
                                <ul>
                                    <li><a href="aurum-software-solutions-private-limited">Aurum Software Solutions Private Limited </a></li>
                                    <li><a href="aurum-realtech-services-private-limited">Aurum RealTech Services Private Limited </a></li>
                                    <li><a href="k2v2-technologies">K2V2 Technologies</a></li>
                                    <li class="active"><a href="monktech-labs">MonkTech Labs Pte. Ltd.</a></li>
                                    <li><a href="integrow-asset-management">Integrow Asset Management Pvt. Ltd.</a></li>
                                </ul>
                            </div>
                        </div>
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
<?php
include "footer.php";
?>