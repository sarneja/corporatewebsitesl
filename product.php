<?php
include "header.php";
?>
<!-- Inner Banner Section -->
<section class="inner-banner">
        <div class="banner-curve"></div>
		<div class="auto-container">
            <div class="inner">
                <div class="theme-icon"></div>
    			<div class="title-box">
                    <h1>Our Products</h1>
                    
                </div>
            </div>
		</div>
    </section>
    <!--End Banner Section -->

    <!--Cases Section-->
    <section class="cases-section cases-page">
        <div class="auto-container">

            <!--Carousel Box-->
            <div class="cases-box">
                <div class="row clearfix">
                    <!--Case Block-->
                    <div class="case-block col-lg-6 col-md-12 col-sm-12">
                        <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <figure class="image-box">
                                <a href="invest-and-finance-cat.php"><img src="images/resource/investandfinace.jpg" alt="" title=""></a>
                            </figure>
                            <div class="content-box">
                                <div class="title-box">
                                    <h4><a href="invest-and-finance-cat.php">Invest & Finance</a></h4>
                                    <!--<div class="sub-text">IT Networking</div>-->
                                </div>
                                <div class="text-content">
                                    <div class="text">Data Driven Capital Allocation.</div>
                                    <div class="link-box"><a href="invest-and-finance-cat.php">View More <span class="arrow fa fa-arrow-right"></span></a></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Case Block-->
                    <div class="case-block col-lg-6 col-md-12 col-sm-12">
                        <div class="inner-box wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                            <figure class="image-box">
                                <a href="enterprise-efficiency-cat.php"><img src="images/resource/enterpriseefficency.jpg" alt="" title=""></a>
                            </figure>
                            <div class="content-box">
                                <div class="title-box">
                                    <h4><a href="enterprise-efficiency-cat.php">Enterprise Efficiency</a></h4>
                                    <!--<div class="sub-text">Artificial Intelligence</div>-->
                                </div>
                                <div class="text-content">
                                    <div class="text">Tech driven efficient Real Estate construction and saleSmart ERP.</div>
                                    <div class="link-box"><a href="enterprise-efficiency-cat.php">View More <span class="arrow fa fa-arrow-right"></span></a></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Case Block-->
                    <div class="case-block col-lg-6 col-md-12 col-sm-12">
                        <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <figure class="image-box">
                                <a href="customer-experience-cat.php"><img src="images/resource/customerexp.jpg" alt="" title=""></a>
                            </figure>
                            <div class="content-box">
                                <div class="title-box">
                                    <h4><a href="customer-experience-cat.php">Customer Experience</a></h4>
                                    <!--<div class="sub-text">IT Networking</div>-->
                                </div>
                                <div class="text-content">
                                    <div class="text">Enhanced consumer experience of Real Estate purchase.</div>
                                    <div class="link-box"><a href="customer-experience-cat.php">View More <span class="arrow fa fa-arrow-right"></span></a></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Case Block-->
                    <div class="case-block col-lg-6 col-md-12 col-sm-12">
                        <div class="inner-box wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                            <figure class="image-box">
                                <a href="connected-living-cat.php"><img src="images/resource/connectedliving.jpg" alt="" title=""></a>
                            </figure>
                            <div class="content-box">
                                <div class="title-box">
                                    <h4><a href="connected-living-cat.php">Connected Living</a></h4>
                                    <!--<div class="sub-text">Artificial Intelligence</div>-->
                                </div>
                                <div class="text-content">
                                    <div class="text">End-to-End living experience at life and work.</div>
                                    <div class="link-box"><a href="connected-living-cat.php">View More <span class="arrow fa fa-arrow-right"></span></a></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </section>

<?php
include "footer.php";
?>