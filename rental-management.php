<?php
include "header.php";
?>
 <!-- Inner Banner Section -->
 <section class="inner-banner alternate">
        <div class="image-layer" style="background-image: url(images/background/banner-bg-1.jpg);"></div>
        <div class="auto-container">
            <div class="inner">
                <div class="title-box">
                    <h1>Rental Management</h1>
                </div>
            </div>
        </div>
    </section>
    <!--End Banner Section -->

    <div class="sidebar-page-container services-page">
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Content Side-->

                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                    <div class="service-details product-details">
                        <h3 class="h-text"><strong>Rental Management</strong></h3>
                        <p>Our rental management platform, The Housemonk, is an all in one rental service platform where Homeowners/Landlords can list their properties for rent and the tenants can view the listings, book and rent properties most suitable to their needs. </p>
                        <div class="row">
                            <!-- DEMO 1 Item-->
                            <div class="col-lg-12 mb-12 mb-lg-12 img-div">
                                <div class="hover hover-1 text-white rounded"><img src="images/rental.jpg" alt="">
                                <div class="hover-overlay"></div>
                                <div class="hover-1-content px-5 py-4">
                                    <h3 class="hover-1-title text-uppercase font-weight-bold mb-0">TheHouseMonk</h3>
                                    <p class="hover-1-description font-weight-light mb-0">THM is a Rental management software that helps property managers and landlords monetize, maintain, and manage their rental portfolio through its technology platform. It has two modules RE:Core which helps drive profitability, manage rental portfolio efficiently and RE:Xp which helps build a rental property portal for tenants to find, book , live in properties and experience all the services within a rented premise.</p>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>

                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
                    <aside class="sidebar services-sidebar">
                        <!--Services Widget-->
                        <div class="sidebar-widget services-widget">
                            <div class="widget-inner">
                                <ul>
                                    <li><a href="amenities-management">Amenities Management</a></li>
                                    <li class="active"><a href="rental-management">Rental Management</a></li>
                                    <li><a href="co-living">Co-Living</a></li>
                                    <li><a href="co-working">Co-Working</a></li>
                                    <li><a href="smart-building-workplace-tech">Smart Buildings and Workplace Tech</a></li>
                                    <li><a href="property-management">Property Management</a></li>
                                </ul>
                            </div>
                        </div>
                        <!--Info Widget-->
                        <div class="sidebar-widget info-widget">
                        <div class="widget-inner">
                                <div class="image"><img src="images/resource/side-contact.jpg" alt=""></div>
                                <div class="lower">
                                    <div class="subtitle">Got any Questions? <br>Call us Today!</div>
                                    <div class="icon-box"><span class="flaticon-telephone"></span></div>
                                    <div class="phone"><a href="tel:+91 22 3000 1700 / 2778 1271">+91 22 3000 1700 / 2778 1271</a></div>
                                    <div class="email"><a href="mailto:corporate@aurumproptech.in">corporate@aurumproptech.in</a></div>
                                </div>
                            </div>
                        </div>
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
<?php
include "footer.php";
?>