<?php
include "header.php";
?>
<style>
    .main-timeline{
        font-family: 'Poppins', sans-serif;
        padding: 15px 0;
        position: relative;
    }
    .main-timeline:after{
        content: '';
        display: block;
        clear: both;
    }
    .main-timeline:before{
        content: "";
        background: #dcdde1;
        width: 10px;
        height: 100%;
        border-radius: 50px;
        transform: translateX(-50%);
        position: absolute;
        top: 0;
        left: 50%;
    }
    .main-timeline .timeline{
        width: 50.2%;
        padding: 0 0 0 50px;
        margin: 0 0 35px;
        float: right;
        position: relative; 
    }
    .main-timeline .timeline:before{
        content:"";
        width: 35px;
        height: 35px;
        border: 6px solid #3b214e;
        border-radius: 50%;
        transform: translateY(-50%);
        position: absolute;
        top: 50%;
        left: -14px;
    }
    .main-timeline .timeline-content{
        color: #555;
    background: rgba(255,255,255,0.1);
    box-shadow: 0px 0px 25px 0px rgb(0 0 0 / 7%);
    /* background-color: #fff; */
    min-height: 120px;
    padding: 15px 15px 15px 100px;
    /* box-shadow: 0 0 15px -3px rgb(0 0 0 / 20%); */
    display: block;
    position: relative;
    border-radius: 10px;
    border: 2px solid rgba( 255, 255, 255,0.6);
    }
    .main-timeline .timeline-content:hover{ text-decoration: none; }
    .main-timeline .timeline-content:before,
    .main-timeline .timeline-content:after{
        content: '';
        background-color: #3b214e;
        height: 100%;
        width: 125px;
        position: absolute;
        left: 0;
        top: 0;
    }
    .main-timeline .timeline-content:after{
        height: 40px;
        width: 40px;
        transform: translateY(-50%) rotate(45deg);
        top: 50%;
        left: -13px;
    }
    .main-timeline .timeline-year{
        color: #01b9e6;
        background: #fff;
        font-size: 21px;
        font-weight: 600;
        text-align: center;
        line-height: 32px;
padding-top:15px;
        width: 142px;
    height: 120px;               border: 10px solid #dcdde1;
        box-shadow: 0 0 15px -3px rgba(0,0,0,0.2);
        transform: translateY(-50%);
        position: absolute;
        left: -237px;
        top: 50%;
    }
    .main-timeline .timeline-icon{
        color: #fff;
        font-size: 50px;
        transform: translateY(-50%);
        position: absolute;
        top: 50%;
        left: 0;
        z-index: 1;
    }
    .main-timeline .title{
        color: #01b9e6;
        font-size: 22px;
        font-weight: 700;
        text-transform: capitalize;
        margin: 0 0 3px;
    }
    .main-timeline .description{
        font-size: 13px;
        letter-spacing: 1px;
        margin: 0;
    }
    .main-timeline .timeline:nth-child(even){
        padding: 0 50px 0 0;
        float: left;
    }
    .main-timeline .timeline:nth-child(even):before{
        left: auto;
        right: -16px;
    }
    .main-timeline .timeline:nth-child(even) .timeline-content{ padding: 15px 100px 15px 15px; }
    .main-timeline .timeline:nth-child(even) .timeline-content:before{
        left: auto;
        right: 0;
    }
    .main-timeline .timeline:nth-child(even) .timeline-content:after{
        left: auto;
        right: -13px;
    }
    .main-timeline .timeline:nth-child(even) .timeline-year{
        left: auto;
        right: -235px;
    }
    .main-timeline .timeline:nth-child(even) .timeline-icon{
        left: auto;
        right: 0;
    }
    .main-timeline .timeline:nth-child(4n+2):before{ border-color: #0c284d; }
    .main-timeline .timeline:nth-child(4n+2) .timeline-content:before,
    .main-timeline .timeline:nth-child(4n+2) .timeline-content:after{
        background-color: #c6d6ed;
    }
    .main-timeline .timeline:nth-child(4n+2) .timeline-year,
    .main-timeline .timeline:nth-child(4n+2) .title{
        color: #F4801E;
    }
    .main-timeline .timeline:nth-child(4n+3):before{ border-color: #d3a232; }
    .main-timeline .timeline:nth-child(4n+3) .timeline-content:before,
    .main-timeline .timeline:nth-child(4n+3) .timeline-content:after{
        background-color: #fff;
    }

  .main-timeline .timeline:nth-child(4n+9) .timeline-content:before,
    .main-timeline .timeline:nth-child(4n+9) .timeline-content:after{
        background-color: #dba1b6 !important;
    }

 .main-timeline .timeline:nth-child(4n+10) .timeline-content:before,
    .main-timeline .timeline:nth-child(4n+10) .timeline-content:after{
        background-color: #624e4e !important;
    }


    .main-timeline .timeline:nth-child(4n+3) .timeline-year,
    .main-timeline .timeline:nth-child(4n+3) .title{
        color: #d3a232;
    }

.main-timeline .timeline:nth-child(4n+5) .timeline-content:before, .main-timeline .timeline:nth-child(4n+5) .timeline-content:after {
    background-color: #fff;
}

.main-timeline .timeline:nth-child(4n+5):before {
    border-color: #d3a232;
}

.main-timeline .timeline:nth-child(4n+6) .timeline-content:before, .main-timeline .timeline:nth-child(4n+6) .timeline-content:after
{
 background-color: #fff;

}

.main-timeline .timeline:nth-child(4n+10) .timeline-content:before, .main-timeline .timeline:nth-child(4n+6) .timeline-content:after
{


}

.main-timeline .timeline:nth-child(4n+6):before {
    border-color: #d3a232;
}
.main-timeline .timeline:nth-child(4n+8):before {
    border-color: #588d30 !important;
}
.main-timeline .timeline:nth-child(4n+9):before {
    border-color: #7a2142 !important;
}

.main-timeline .timeline:nth-child(4n+10):before {
    border-color: #6c5c21 !important;
}



    .main-timeline .timeline:nth-child(4n+4):before{ border-color: #27ace2; }
    .main-timeline .timeline:nth-child(4n+4) .timeline-content:before,
    .main-timeline .timeline:nth-child(4n+4) .timeline-content:after{
        background-color: #fff;
    }
    .main-timeline .timeline:nth-child(4n+4) .timeline-year,
    .main-timeline .timeline:nth-child(4n+4) .title{
        color: #8B7ACB;
    }
    @media screen and (max-width:767px){
        .main-timeline:before{
            transform: translateX(-50%);
            left: 17px;
        } 
        .main-timeline .timeline,
        .main-timeline .timeline:nth-child(even){
            width: 100%;
            padding: 125px 0 0 65px;
        } 
        .main-timeline .timeline:before,
        .main-timeline .timeline:nth-child(even):before{
            left: 0;
            top: calc(50% + 63px);
        } 
        .main-timeline .timeline-content,
        .main-timeline .timeline:nth-child(even) .timeline-content{
            padding: 15px 15px 15px 100px;
        } 
        .main-timeline .timeline-content:before,
        .main-timeline .timeline:nth-child(even) .timeline-content:before{
            right: auto;
            left: 0;
        }
        .main-timeline .timeline-content:after,
        .main-timeline .timeline:nth-child(even) .timeline-content:after{
            right: auto;
            left: -13px;
        } 
        .main-timeline .timeline-year,
        .main-timeline .timeline:nth-child(even) .timeline-year{
            transform: translateY(0);
            right: auto;
            left: 0;
            top: -125px;
        } 
        .main-timeline .timeline-icon,
        .main-timeline .timeline:nth-child(even) .timeline-icon{
            right: auto;
            left: 15px;
        } 
    }
    @media screen and (max-width:576px){
        .main-timeline .timeline:before,
        .main-timeline .timeline:nth-child(even):before{
            transform: translateY(0);
            top: 148px;
        }
        .main-timeline .timeline-content,
        .main-timeline .timeline:nth-child(even) .timeline-content{
            padding: 100px 15px 15px;
        }
        .main-timeline .timeline-content:before,
        .main-timeline .timeline:nth-child(even) .timeline-content:before{
            width: 100%;
            height: 80px;
        }
        .main-timeline .timeline-content:after,
        .main-timeline .timeline:nth-child(even) .timeline-content:after{
            transform: translateX(-50%) translateY(0) rotate(45deg);
            top: 20px;
            left: 7px;
        } 
        .main-timeline .timeline-icon,
        .main-timeline .timeline:nth-child(even) .timeline-icon{
            transform: translateX(-50%) translateY(0);
            left: 50%;
            top: 6px;
        } 
    }
</style>
<!-- Inner Banner Section -->
<section class="inner-banner alternate">
        <div class="banner-curve"></div>
		<div class="auto-container">
            <div class="inner">
                <div class="theme-icon"></div>
    			<div class="title-box">
                    <h1>Timeline</h1>
                    
                </div>
            </div>
		</div>
    </section><!--End Banner Section -->

<!--Team Section-->
    
<section style="padding-top: 70px;    background-image: url(images/background.png);
    background-position: top right;
 
    /* background-color: #120034; */
    background-repeat: no-repeat;
   background-size:100% 1900px;    opacity: 1;
    /* background: linear-gradient(175deg, #e8ecff 0%, #e6e4f9 40%, #e5d7f6 100%) !important; */
    transition: background 0.3s, border-radius 0.3s, opacity 0.3s;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="main-timeline">
                    <div class="timeline">
                        <a href="https://www.majesco.com/" target="_blank" class="timeline-content">
                            <div class="timeline-year" style="color:#3b214e;background: rgba(255,255,255,0.1);
    box-shadow: 0px 0px 25px 0px rgb(0 0 0 / 7%);border:8px solid #3b214e;">June<br> 2015</div>
                            <div class="timeline-icon"><img src="images/majesco1_r (1).jpg" style="width: 125px;"></div>
                            <h3 class="title" style="margin-left:35px;color:#3b214e;">Majesco</h3>
                            <p class="description" style="margin-left:35px;color:#000;">
                                Majesco Demerger from Mastek.
                            </p>
                        </a>
                    </div>
                    <div class="timeline">
                        <a href="https://www.thomabravo.com/" target="_blank" class="timeline-content">
                            <div class="timeline-year" style="color:#0c284d;background: rgba(255,255,255,0.1);
    box-shadow: 0px 0px 25px 0px rgb(0 0 0 / 7%);border:8px solid #0c284d;">August 2020</div>    
                            <div class="timeline-icon"><img src="images/Thoma_Bravo_Logo_6.2020.svg.jpg" style="width: 125px;height:21px"></div>
                            <h3 class="title" style="color:#0c284d;">Thomabravo</h3>
                            <p class="description" style="margin-right:30px;">
                               Majesco insuretech business sold to Thoma Bravo for USD 594 million
                            </p>
                        </a>
                    </div>

			<div class="timeline">
                        <a href="https://www.aurumventures.in/" target="_blank" class="timeline-content">
                            <div class="timeline-year" style="color:#d3a232;background: rgba(255,255,255,0.1);
    box-shadow: 0px 0px 25px 0px rgb(0 0 0 / 7%);border:8px solid #d3a232;margin-left: -68px;width: 206px;">March-June <br>2021</div>
                            <div class="timeline-icon"><img src="images/logo (2).png" style="width: 125px;"></div>
                            <h3 class="title" style="margin-left:35px;color:#d3a232;">Aurum Ventures</h3>
                            <p class="description" style="margin-left:35px;color:#000;">
                                Aurum Ventures acquires 35% including promoter shareholding in majesco.
                            </p>
                        </a>
                    </div>

		<div class="timeline">
                        <a href="https://www.sell.do/" target="_blank" class="timeline-content">
                            <div class="timeline-year" style="color:#27ace2;background: rgba(255,255,255,0.1);
    box-shadow: 0px 0px 25px 0px rgb(0 0 0 / 7%);border:8px solid #27ace2">July<br> 2021</div>
                            <div class="timeline-icon"><img src="images/sell.png" style="width: 125px;"></div>
                            <h3 class="title" style="color:#27ace2;">Sell.do</h3>
                            <p class="description" style="color:#000;">
                                Approves acquisition of majority stake in CRM company sell.do                            </p>
                        </a>
                    </div>

<div class="timeline">
                        <a href="https://aurumproptech.in/" target="_blank" class="timeline-content">
                            <div class="timeline-year" style="color:#d3a232;background: rgba(255,255,255,0.1);
    box-shadow: 0px 0px 25px 0px rgb(0 0 0 / 7%);border:8px solid #d3a232">July<br> 2021</div>
                            <div class="timeline-icon"><img src="images/Horizontal_AurumPropTech%20(1).png" style="width: 125px;"></div>
                            <h3 class="title" style="margin-left:35px;color:#d3a232;">Aurum PropTech</h3>
                            <p class="description" style="margin-left:35px;color:#000;">
                               Announces PropTech as its new line of business.Identifies 4 Key areas for its integrated PropTech Ecosystem - Customer Experience, Enterprise Efficiency, Connected Living, Invest & Finance
                            </p>
                        </a>
                    </div>
<div class="timeline">
                        <a href="https://aurumproptech.in/" target="_blank" class="timeline-content">
                            <div class="timeline-year" style="color:#d3a232;background: rgba(255,255,255,0.1);
    box-shadow: 0px 0px 25px 0px rgb(0 0 0 / 7%);border:8px solid #d3a232">August 2021</div>
                            <div class="timeline-icon"><img src="images/Horizontal_AurumPropTech%20(1).png" style="width: 125px;"></div>
                            <h3 class="title" style="color:#d3a232;">Aurum PropTech</h3>
                            <p class="description" style="color:#000;">
                                Launches state of the art integrated FulFillment Center                            </p>
                        </a>
                    </div>
<div class="timeline">
                        <a href="https://aurumproptech.in/" target="_blank" class="timeline-content">
                            <div class="timeline-year" style="color:#d3a232;background: rgba(255,255,255,0.1);
    box-shadow: 0px 0px 25px 0px rgb(0 0 0 / 7%);border:8px solid #d3a232">October 2021</div>
                            <div class="timeline-icon"><img src="images/Horizontal_AurumPropTech%20(1).png" style="width: 125px;"></div>
                            <h3 class="title" style="margin-left:35px;color:#d3a232;">Aurum PropTech</h3>
                            <p class="description" style="margin-left:35px;color:#000;">
                               Majesco changes its name to Aurum PropTech Ltd.
                            </p>
                        </a>
                    </div>
<div class="timeline">
                        <a href="http://www.integrowamc.com/" target="_blank" class="timeline-content">
                            <div class="timeline-year" style="color:#588d30;background: rgba(255,255,255,0.1);
    box-shadow: 0px 0px 25px 0px rgb(0 0 0 / 7%);border:8px solid #588d30">October 2021</div>
                            <div class="timeline-icon"><img src="images/clients/6.png" style="width: 125px;"></div>
                            <h3 class="title" style="color:#588d30;">Integrow AMC</h3>
                            <p class="description" style="color:#000;">
                               Approves acquisition of 49% stake in Asset Management Company Integrow                          </p>
                        </a>
                    </div>

<div class="timeline">
                        <a href="https://thehousemonk.com/" target="_blank" class="timeline-content">
                            <div class="timeline-year" style="color:#7a2142;background: rgba(255,255,255,0.1);
    box-shadow: 0px 0px 25px 0px rgb(0 0 0 / 7%);border:8px solid #7a2142">December 2021</div>
                            <div class="timeline-icon"><img src="images/THM-Logo_Black-1-1 (1).png" style="width: 125px;"></div>
                            <h3 class="title" style="margin-left:35px;color:#7a2142;">TheHouseMonk</h3>
                            <p class="description" style="margin-left:35px;color:#000;">
                              Approves acquisition of majority stake in Rental Management Software Platform TheHouseMonk                            </p>
                        </a>
                    </div>


<div class="timeline">
                        <a href="https://grexter.in/" target="_blank" class="timeline-content">
                            <div class="timeline-year" style="color:#6c5c21;background: rgba(255,255,255,0.1);
    box-shadow: 0px 0px 25px 0px rgb(0 0 0 / 7%);border:8px solid #6c5c21">February 2022</div>
                            <div class="timeline-icon"><img src="images/logo-white.png" style="width: 125px;"></div>
                            <h3 class="title" style="color:#6c5c21;">Grexter Living</h3>
                            <p class="description" style="color:#000;">
                               Approves acquisition of 53% stake in <br>Grexter Living                          </p>
                        </a>
                    </div>




                </div>
            </div>
        </div>
    </div>
</section>

<?php
include "footer.php";
?>