<?php
include "header.php";
?>
 <!-- Inner Banner Section -->
 <section class="inner-banner alternate">
        <div class="image-layer" style="background-image: url(images/background/banner-bg-1.jpg);"></div>
        <div class="auto-container">
            <div class="inner">
                <div class="title-box">
                    <h1>Transactions</h1>
                </div>
            </div>
        </div>
    </section>
    <!--End Banner Section -->

    <div class="sidebar-page-container services-page">
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                    <div class="service-details product-details">
                        <h3 class="h-text"><strong>Transactions</strong></h3>
                        <p>Our Beyond Walls and Aurum Listing platforms allow channel partners access to information about the projects. Beyond walls creates channel partner centric sales model thus empowering the channel partner network. Aurum Listing is a premium listing service where real estate experts list handpicked properties and the fulfillment center provides real time expert advice to enable the buyers to make the best choice.  </p>
                        <div class="row">
                            <!-- DEMO 1 Item-->
                            <div class="col-lg-12 mb-12 mb-lg-12 img-div">
                                <div class="hover hover-1 text-white rounded"><img src="images/transaction (2).jpg" alt="">
                                <div class="hover-overlay"></div>
                                <div class="hover-1-content px-5 py-4">
                                    <h3 class="hover-1-title text-uppercase font-weight-bold mb-0">Beyond Walls by Aurum PropTech</h3>
                                    <p class="hover-1-description font-weight-light mb-0">Beyond walls is a broker aggregation tech platform aimed at empowering the Channel Partner Network in India while creating a channel partner centric sales model. It helps Real Estate developers launch their project on the platform while enabling Channel partners get digital collaterals and information about the projects.</p>
                                </div>
                                </div>
                            </div>

                            <!-- DEMO 1 Item-->
                            <div class="col-lg-12 mb-12 mb-lg-12 img-div">
                                <div class="hover hover-1 text-white rounded"><img src="images/listing.jpg" alt="">
                                <div class="hover-overlay"></div>
                                <div class="hover-1-content px-5 py-4">
                                    <h3 class="hover-1-title text-uppercase font-weight-bold mb-0">Aurum Listing</h3>
                                    <p class="hover-1-description font-weight-light mb-0">A Premium Listing service that lists properties handpicked and vetted by Real Estate Experts. The Listing service is connected to the fulfillment center thus enabling buyers with real time expert advise on the properties.</p>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>

                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
                    <aside class="sidebar services-sidebar">
                        <!--Services Widget-->
                        <div class="sidebar-widget services-widget">
                            <div class="widget-inner">
                                <ul>
                                    <li><a href="construction-marketplace.php">Construction Marketplace</a></li>
									<li><a href="real-time-document-manager.php">Real Time Document Manager</a></li>
									<li><a href="customer-relationship-management.php">Customer Relationship Management</a></li>
									<li><a href="fullfillment-center.php">Fulfillment Centre</a></li>
									<li class="active"><a href="transaction.php">Transactions</a></li>
                                </ul>
                            </div>
                        </div>
                        <!--Info Widget-->
                        <div class="sidebar-widget info-widget">
                            <div class="widget-inner">
                                <div class="image"><img src="images/resource/side-contact.jpg" alt=""></div>
                                <div class="lower">
                                    <div class="subtitle">Got any Questions? <br>Call us Today!</div>
                                    <div class="icon-box"><span class="flaticon-telephone"></span></div>
                                    <div class="phone"><a href="tel:+91 22 3000 1700 / 2778 1271">+91 22 3000 1700 / 2778 1271</a></div>
                                    <div class="email"><a href="mailto:corporate@aurumproptech.in">corporate@aurumproptech.in</a></div>
                                </div>
                            </div>
                        </div>
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
<?php
include "footer.php";
?>